<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('createNm')->comment('제작번호');
            $table->string('odNo')->comment('주문번호');
            $table->string('odSno')->comment('상품주문번호');
            $table->string('odId')->comment('주문자아이디');
            $table->string('odName')->comment('주문자명');
            $table->string('goodsType')->comment('상품구분');
            $table->string('goodsNm')->comment('상품명');
            $table->string('goodsCd')->comment('상품코드');
            $table->json('sceneInfoUser')->comment('장면정보');
            $table->string('moviePath')->comment('영상경로');
            $table->integer('payment')->comment('결제여부(0:미결제, 1:결제완료)');
            $table->integer('modifyCount')->comment('수정가능횟수(0:가능, 1:불가능)');
            $table->integer('state')->comment('상태(0:제작대기, 1:제작요청, 2:제작중, 3:제작완료, 4:자동삭제)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
