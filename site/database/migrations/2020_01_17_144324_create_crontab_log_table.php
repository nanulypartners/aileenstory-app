<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrontabLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crontab_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type')->comment('타입(0:자동삭제, 1:자동렌더링)');
            $table->json('result')->comment('결과');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
