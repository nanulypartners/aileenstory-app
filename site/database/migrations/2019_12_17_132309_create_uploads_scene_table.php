<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsSceneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads_scene', function (Blueprint $table) {
            $table->increments('id');
            $table->string('createNm')->comment('제작번호');
            $table->integer('scene')->comment('장면번호');
            $table->string('basic_image')->comment('원본이미지');
            $table->string('cut_image')->comment('편집이미지');
            $table->string('synthesis_image')->comment('합성이미지');
            $table->string('basic_movie')->comment('원본영상');
            $table->string('cut_movie')->comment('편집영상');
            $table->json('cut_info')->comment('편집정보');
            $table->integer('filesize')->comment('파일사이즈');
            $table->integer('filetype')->comment('파일타입');
            $table->json('text')->comment('문구');
            $table->integer('complete')->comment('완료여부');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
