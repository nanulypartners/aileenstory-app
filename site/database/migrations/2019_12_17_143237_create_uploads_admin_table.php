<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads_admin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no')->unique()->comment('번호');
            $table->string('goodsType')->comment('상품구분');
            $table->string('goodsNm')->comment('상품명');
            $table->string('goodsCd')->comment('상품코드');
            $table->integer('useYn')->comment('노출여부(0:비노출, 1:노출)');
            $table->string('templateId')->comment('템플릿 아이디');
            $table->string('templateName')->comment('템플릿 이름');
            $table->string('templatePath')->comment('템플릿 경로');
            $table->json('sceneInfo')->comment('장면정보');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
