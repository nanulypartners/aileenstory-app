<?php

namespace Statamic\Addons\Uploads;

use Statamic\Extend\Controller;

use Statamic\UpladsAdmin;
use Statamic\Upload;
use Statamic\UploadsScene;

use Illuminate\Http\Request;
use Statamic\Addons\IumSender\IumSenderController;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class UploadsController extends Controller
{
    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */
    private $request, $domain_info, $folder;

    public function __construct(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: x-csrf-token");
        $this->request = $request;
        $this->domain_info = "http://aileenstory.com/";
        $this->folder = $request->server('DOCUMENT_ROOT') . '/storage/autorender/uploads/';
    }

    public function getDays()
    {
        $createNo = $this->request->segment(4);
        $userUploads = Upload::where('createNm', $createNo)->first();
        $dt = $userUploads->updated_at->addMonth(1);

        if ($dt > Carbon::now()) {
            echo "파일 보관 기간 <span style='color:red'>" . $dt->diffInDays(Carbon::now()) . "일</span> 이후 삭제됩니다.";
        } else {
            echo "파일 보관 기간이 지났습니다.";
        }

        exit;
    }


    public function postMp3()
    {
        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];
        $sceneNo = 999;

        $this->checkState($createNo);

        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->first();

        //사용자 장면 정보
        $userScene = UploadsScene::where('createNm', $createNo)->where('scene', $sceneNo)->first();

        if (empty($userScene)) {
            $scene = new UploadsScene;
        } else {
            $scene = $userScene;
        }

        $folder = $this->folder . $createNo . '/';
        if (!file_exists($folder)) {
            mkdir($folder, 0755, true);
        }

        if ($this->request->file('mp3')) {

            $tmp_file = $this->request->file('mp3')->getPathname();
            $originalName = $this->request->file('mp3')->getClientOriginalName();

            $ffprobe = \FFMpeg\FFProbe::create();

            $duration = $ffprobe
                ->format($tmp_file) // extracts file informations
                ->get('duration');             // returns the duration property

            $file_name = $sceneNo . '.mp3';
            $dest_file = $folder . $file_name;

            $mp3time = $goodsInfo['bgmTime'];

            if ($mp3time > $duration) {
                $difftime = floor($mp3time / $duration);
                exec('ffmpeg -stream_loop ' . $difftime . ' -i "' . $tmp_file . '" -c copy ' . $dest_file);
            } else {
                $ffmpeg = \FFMpeg\FFMpeg::create();
                $video = $ffmpeg->open($tmp_file);
                $format = new \FFMpeg\Format\Audio\Mp3();
                //$video->filters()->clip(\FFMpeg\Coordinate\TimeCode::fromSeconds(0), \FFMpeg\Coordinate\TimeCode::fromSeconds(30));
                $video->save($format, $dest_file);
            }

            $scene->basic_movie = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;
            $scene->cut_movie = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;
            $scene->text = $originalName;
            $scene->filesize = $this->request->file('mp3')->getClientSize();
            $scene->filetype = $this->request->file('mp3')->getClientMimeType();

        } else if ($this->request['type'] == "del") {

            if (file_exists($folder . $sceneNo . '.mp3')) {
                unlink($folder . $sceneNo . '.mp3');
            }

            $scene->basic_movie = '';
            $scene->cut_movie = '';
            $scene->filesize = '';
            $scene->filetype = '';
            $scene->text = '';
        }

        $scene->createNm = $createNo;
        $scene->scene = $sceneNo;
        $scene->complete = 1;
        $scene->save();

        $result['state'] = 'ok';

        if (!empty($createNo)) {
            if ($createNo != "") {
                $userFilesAll = DB::table('uploads')->where('createNm', $createNo);
                $userFilesAll->update(['updated_at' => date('Y-m-d H:i:s')]);
            }
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;

    }


    public function checkState($createNo, $sceneNo = '')
    {
        //사용자 업로드 정보
        $userUploads = Upload::where('createNm', $createNo)->first();
        $result['sceneNo'] = $sceneNo;

        if (!empty($userUploads->state)) {
            if ($userUploads->state == 3 && $userUploads->modifyCount == 1) {
                $result['state'] = 'error';
                $result['msg'] = '이미 제작이 완료된 무비입니다.';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit;
            } else if ($userUploads->state != 0 && $userUploads->state != 3) {
                $result['state'] = "error";
                $result['msg'] = "신청이 완료된 무비는 수정이 불가능 합니다.";
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit;
            }
        }

    }

    public function postSceneDel()
    {
        $createNo = $this->request['createNo'];
        $sceneNo = $this->request['sceneNo'];

        $this->checkState($createNo, $sceneNo);

        //장면정보 불러오기
        $userScene = UploadsScene::where('createNm', $createNo)->where('scene', $sceneNo)->first();

        $dir = '/home/nanu/apps/statamic/storage/autorender/uploads/' . $createNo . '/';

        //제작상태 업데이트
        $userUploads = Upload::where('createNm', $createNo)->first();
        if ($userUploads->state == 3) {
            $userUploads->state = 0;
            $userUploads->update();

            //에일린서버로 전송
            $iumSender = new IumSenderController('autoRenderStart');
            $sendResult = $iumSender->send($createNo);

            if ($sendResult != 'ok') {
                $result['state'] = "error";
                $result['msg'] = "서버 전송에 실패했습니다. 다시 시도해 주세요.";
            } else {
                $result['state'] = "success";
                $result['msg'] = "삭제되었습니다.";
            }
        } else {
            $result['state'] = "success";
            $result['msg'] = "삭제되었습니다.";
        }

        //파일삭제
        if (file_exists($dir . $sceneNo . '.jpg')) {
            unlink($dir . $sceneNo . '.jpg');
        }

        if (file_exists($dir . $sceneNo . '_basic.jpg')) {
            unlink($dir . $sceneNo . '_basic.jpg');
        }

        if (file_exists($dir . $sceneNo . '_basic_thumb.jpg')) {
            unlink($dir . $sceneNo . '_basic_thumb.jpg');
        }

        if (file_exists($dir . $sceneNo . '.mp4')) {
            unlink($dir . $sceneNo . '.mp4');
        }

        if (file_exists($dir . $sceneNo . '_synthesis.jpg')) {
            unlink($dir . $sceneNo . '_synthesis.jpg');
        }

        if (file_exists($dir . $sceneNo . '_synthesis_thumb.jpg')) {
            unlink($dir . $sceneNo . '_synthesis_thumb.jpg');
        }

        //DB삭제
        //$userScene->delete();
        $userScene->basic_image = '';
        $userScene->cut_image = '';
        $userScene->synthesis_image = '';
        $userScene->basic_movie = '';
        $userScene->cut_movie = '';
        $userScene->cut_info = '';
        $userScene->filesize = '';
        $userScene->filetype = '';
        $userScene->complete = 0;
        $userScene->update();

        return json_encode($result, JSON_UNESCAPED_UNICODE);

    }

    public function postSceneChange()
    {

        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];
        $first = $this->request['first'];
        $second = $this->request['second'];
        $firstText = $this->request['firstText'];
        $secondText = $this->request['secondText'];

        $this->checkState($createNo);

        $firstScene = UploadsScene::where('createNm', $createNo)->where('scene', $first)->first();
        $secondScene = UploadsScene::where('createNm', $createNo)->where('scene', $second)->first();

        $folder = $this->folder . $createNo . '/';

        $firstFile = "";
        if (file_exists($folder . $first . '_basic.jpg')) {
            copy($folder . $first . '_basic.jpg', $folder . 'first.jpg');
            $firstFile = new \Symfony\Component\HttpFoundation\File\UploadedFile($folder . 'first.jpg', $first . '_basic.jpg', '0', filesize($folder . 'first.jpg'));
        }

        $secondFile = "";
        if (file_exists($folder . $second . '_basic.jpg')) {
            copy($folder . $second . '_basic.jpg', $folder . 'second.jpg');
            $secondFile = new \Symfony\Component\HttpFoundation\File\UploadedFile($folder . 'second.jpg', $second . '_basic.jpg', '0', filesize($folder . 'second.jpg'));
        }

        $this->request['createNo'] = $createNo;

        if ($firstFile != "") {
            if (!empty($secondScene['text'])) {
                $this->request['text'] = json_decode($secondScene['text'], true);
            }
            $result['files'][] = $this->Uploads($goodsNo, $createNo, $firstScene['type'], '', $firstFile, $second);
        } else {
            //두번째 삭제
            $this->request['sceneNo'] = $second;
            $this->postSceneDel();
        }

        if ($secondFile != "") {
            if (!empty($firstScene['text'])) {
                $this->request['text'] = json_decode($firstScene['text'], true);
            }
            $result['files'][] = $this->Uploads($goodsNo, $createNo, $secondScene['type'], '', $secondFile, $first);
        } else {
            //첫번째 삭제
            $this->request['sceneNo'] = $first;
            $this->postSceneDel();
        }

        //파일삭제
        if (file_exists($folder . 'first.jpg')) {
            unlink($folder . 'first.jpg');
        }
        if (file_exists($folder . 'second.jpg')) {
            unlink($folder . 'second.jpg');
        }

        $result['state'] = "success";
        return json_encode($result, JSON_UNESCAPED_UNICODE);

    }

    public function postUploads()
    {
        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];
        $type = $this->request['type'];
        $tab_name = $this->request['tab_name'];
        $files = $this->request->file('file');

        $this->checkState($createNo);

        $result = [];
        $result['state'] = "Not Files";
        foreach ($files as $key => $file) {
            $result['files'][] = $this->Uploads($goodsNo, $createNo, $type, $tab_name, $file);
            $result['state'] = "success";
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);

    }

    public function postBasic()
    {
        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];
        $type = $this->request['type'];
        $sceneNo = $this->request['sceneNo'];
        $file = $this->request->file('file');

        $this->checkState($createNo, $sceneNo);

        $result = [];
        $result['state'] = "Not Files";

        if (!empty($file)) {
            $result['state'] = "success";
            $result['files'] = $this->Uploads($goodsNo, $createNo, $type, '', $file, $sceneNo);
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    private function Uploads($goodsNo, $createNo, $type, $tab_name = '', $file, $sceneNo = '')
    {

        $folder = $this->folder . $createNo . '/';
        if (!file_exists($folder)) {
            mkdir($folder, 0755, true);
        }

        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->first();

        if ($sceneNo == '') {
            //관리자 등록 정보
            $sceneAdmin = collect(json_decode(stripslashes($goodsInfo['sceneInfo']), true))->groupBy('type');

            $sceneNoArr = [];
            foreach ($sceneAdmin as $key => $value) {
                foreach ($value as $k => $val) {
                    if ($val['type'] == $type && $val['tab_name'] == $tab_name) {
                        $sceneNoArr[] = $val['seq'];
                    }
                }
            }

            //업로드 되어있지 않은 장면 번호 불러오기
            $sceneNo = UploadsScene::select('scene')->whereIn('scene', $sceneNoArr)->where('createNm', $createNo)->where('filesize', '!=', '0')->get();

            $sceneNoInArr = [];
            foreach ($sceneNo as $scene) {
                $sceneNoInArr[] = $scene->scene;
            }

            $sceneNoArr = collect($sceneNoArr);
            $diff = $sceneNoArr->diff($sceneNoInArr);
            $sceneNo = $diff->first();
        } else {
            $sceneAdmin = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$sceneNo];
            $tab_name = $sceneAdmin['tab_name'];
        }

        if (!empty($sceneNo)) {

            $sceneAdmin = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$sceneNo];
            if (!empty($sceneAdmin['text_sample']) && $sceneAdmin['text_use']=="yes") {
                $adminText = json_encode($sceneAdmin['text_sample'], JSON_UNESCAPED_UNICODE);
            }

            if ($type == 'photo') {

                $tmp_file = $file->getPathname();

                //이미지를 jpg로 변환
                // read page 1
                $img = new \imagick($tmp_file);
                $this->autorotate($img);

                $img->resizeImage(1920, 1080, \Imagick::COMPOSITE_ATOP, 1, TRUE);

                // convert to jpg
                $img->setCompression(\Imagick::COMPRESSION_JPEG);
                $img->setCompressionQuality(100);
                $img->setImageFormat('jpeg');

                //write image on server
                $file_name = $sceneNo . '_basic.jpg';
                $dest_file = $folder . $file_name;
                $file_name_thumb = $sceneNo . '_basic_thumb.jpg';
                $dest_file_thumb = $folder . $file_name_thumb;

                if (file_exists($dest_file_thumb)) {
                    unlink($dest_file_thumb);
                }

                $img->writeImage($dest_file);

                $backImage = new \Imagick();
                $backImage->newImage(1920, 1080, '#000000');
                $backImage->compositeImage($img, \Imagick::COMPOSITE_DEFAULT, (((($backImage->getImageWidth()) - ($img->getImageWidth()))) / 2), (((($backImage->getImageHeight()) - ($img->getImageHeight()))) / 2));

                $backImage->cropThumbnailImage($sceneAdmin['image_width'], $sceneAdmin['image_height']);
                $backImage->writeImage($folder . $sceneNo . ".jpg");

                $img->clear();
                $img->destroy();
                $backImage->clear();
                $backImage->destroy();

                //이미지를 jpg로 변환

                $new_basic_image = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;
                $new_cutImage = '/storage/autorender/uploads/' . $createNo . '/' . $sceneNo . '.jpg';
                $new_filesize = $file->getClientSize();
                $new_filetype = $file->getClientMimeType();

                $image_merge_result = $this->image_merge($sceneAdmin, $createNo, $sceneNo)['synthesis'];

            } else if ($type == 'video') {

                $tmp_file = $file->getPathname();

                $file_name = $sceneNo . '_basic.mp4';
                $dest_file = $folder . $file_name;

                $file_name2 = $sceneNo . '_synthesis.jpg';
                $dest_file2 = $folder . $file_name2;

                $ffprobe = \FFMpeg\FFProbe::create();

                $bitRate = $ffprobe
                        ->streams($tmp_file)
                        ->videos()
                        ->first()
                        ->get('bit_rate') / 1024;

                if ($bitRate > 20000) {
                    $bitRate = $bitRate / 2;
                } else if ($bitRate > 4000) {
                    $bitRate = 4000;
                } else {
                    $bitRate = 2000;
                }

                $duration = $ffprobe
                    ->format($tmp_file) // extracts file informations
                    ->get('duration');             // returns the duration property

                $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->first();
                $sceneNoAdmin = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$sceneNo];

                if ($duration < $sceneNoAdmin['video_time']) {
                    $result['subState'] = "error_time";
                    $result['sceneNo'] = $sceneNo;
                    $result['msg'] = "영상의 재생시간이 필요한 재생시간 보다 부족한 파일이 있습니다.\n{$sceneNoAdmin['video_time']}초 이상의 영상을 등록해 주세요.";
                    return $result;
                } else {

                    $ffmpeg = \FFMpeg\FFMpeg::create();
                    $video = $ffmpeg->open($tmp_file);

                    $format = new \FFMpeg\Format\Video\X264();
                    $format
                        ->setKiloBitrate($bitRate)
                        ->setAudioCodec("libmp3lame");

                    $video->save($format, $dest_file);

                    //추가 원본저장후 리사이징
                    $ffmpeg = \FFMpeg\FFMpeg::create();
                    $video = $ffmpeg->open($dest_file);

                    $re_file_name = $sceneNo . '_basic_.mp4';
                    $re_dest_file = $folder . $re_file_name;

                    $video_width = 1920;
                    $video_height = 1080;

                    $video
                        ->filters()
                        ->resize(new \FFMpeg\Coordinate\Dimension($video_width, $video_height), 'inset')
                        ->synchronize();

                    $format = new \FFMpeg\Format\Video\X264();
                    $format
                        ->setKiloBitrate($bitRate)
                        ->setAudioCodec("libmp3lame");

                    $video->save($format, $re_dest_file);
                    rename($re_dest_file, $dest_file);
                    //추가 원본저장후 리사이징

                    $new_basic_movie = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;
                    $new_filesize = $file->getClientSize();
                    $new_filetype = $file->getClientMimeType();

                    //cut파일 삭제
                    if (file_exists($folder . $sceneNo . '.mp4')) {
                        unlink($folder . $sceneNo . '.mp4');
                    }

                    if (file_exists($folder . $sceneNo . '_synthesis.jpg')) {
                        unlink($folder . $sceneNo . '_synthesis.jpg');
                    }

                    if (file_exists($folder . $sceneNo . '_synthesis_thumb.jpg')) {
                        unlink($folder . $sceneNo . '_synthesis_thumb.jpg');
                    }

                    //원본업로드 미리보기
                    $ffmpeg = \FFMpeg\FFMpeg::create();
                    $video = $ffmpeg->open($dest_file);
                    $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(0))->save($dest_file2);

                    //리사이즈
                    $resize = new \Imagick($dest_file2);
                    $resize->resizeImage($sceneNoAdmin['video_width'], $sceneNoAdmin['video_height'], \Imagick::COMPOSITE_ATOP, 1, TRUE);

                    $new_basic_movie_synthesis = '/storage/autorender/uploads/' . $createNo . '/' . $file_name2;

                    $resize->clear();
                    $resize->destroy();
                }

            }

            /*********** DB 저장부분 및 리턴 ********/

            $uploads = UploadsScene::where('createNm', $createNo)->first();

            if (empty($uploads)) {
                $iumSender = new IumSenderController('userInsert');
                $iumSender->send($createNo);
            }

            //사용자 장면 정보
            $userScene = UploadsScene::where('createNm', $createNo)->where('scene', $sceneNo)->first();

            if (empty($userScene)) {
                $scene = new UploadsScene;
            } else {
                $scene = $userScene;
            }

            if (!empty($new_basic_image)) {
                $scene->basic_image = $new_basic_image;
                $scene->cut_image = $new_cutImage;
                $scene->synthesis_image = $image_merge_result;
                $scene->complete = 1;
            }

            if (!empty($new_basic_movie)) {
                $scene->basic_movie = $new_basic_movie;
                $scene->basic_image = $new_basic_movie_synthesis;
                $scene->complete = 0;
            }

            if (!empty($new_filesize)) {
                $scene->filesize = $new_filesize;
            }

            if (!empty($new_filetype)) {
                $scene->filetype = $new_filetype;
            }

            if (!empty($adminText)) {
                $scene->text = $adminText;
            }

            if (!empty($this->request['text'])) {
                $scene->text = json_encode($this->request['text'],JSON_UNESCAPED_UNICODE);
            }

            $scene->type = $type;
            $scene->tab = $tab_name;
            $scene->createNm = $createNo;
            $scene->scene = $sceneNo;
            $scene->cut_info = '';
            $scene->save();

            if (!empty($image_merge_result)) {
                $result['synthesis_image'] = $new_basic_image . '?' . time();
            }

            if (!empty($new_basic_movie)) {
                $result['synthesis_image'] = $new_basic_movie_synthesis . '?' . time();
            }

            $result['subState'] = "success";
            $result['sceneNo'] = $sceneNo;
            $result['msg'] = "저장이 완료되었습니다.";

        } else {
            $result['subState'] = "error";
            $result['msg'] = "더 이상 저장할 수 없습니다.";
        }

        return $result;

    }

    public function postText()
    {
        $createNo = $this->request['createNo'];
        $sceneNo = $this->request['sceneNo'];
        $no = $this->request['no'];
        $text_user = $this->request['text_user'];

        $this->checkState($createNo, $sceneNo);

        $userScene = UploadsScene::where('createNm', $createNo)->where('scene', $sceneNo)->first();

        if (!empty($userScene['text'])) {
            $userText = json_decode($userScene['text'], true);
        } else {
            $userText = [];
        }

        $userText[$no] = $text_user;

        if (empty($userScene)) {
            $userScene = new UploadsScene;
            $userScene->createNm = $createNo;
            $userScene->scene = $sceneNo;
            $userScene->type = 'text';
            $userScene->complete = 1;
            $userScene->text = json_encode((object)$userText, JSON_UNESCAPED_UNICODE);
            $userScene->save();
        } else {
            $userScene->text = json_encode((object)$userText, JSON_UNESCAPED_UNICODE);
            $userScene->update();
        }

        $result['state'] = "success";
        $result['msg'] = "텍스트 수정이 완료되었습니다.";

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function postScene()
    {
        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];
        $sceneNo = $this->request['sceneNo'];

        //관리자 등록 정보
        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->first();
        $sceneAdmin = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$sceneNo];

        $this->checkState($createNo, $sceneNo);

        $cut_info = 0;

        if (!empty($this->request['cut_info'])) {
            $cut_info = json_encode($this->request['cut_info'], JSON_NUMERIC_CHECK);
        }

        if (!empty($createNo)) {
            if ($createNo != "") {
                $userFilesAll = DB::table('uploads')->where('createNm', $createNo);
                $userFilesAll->update(['updated_at' => date('Y-m-d H:i:s')]);
            }
        }

        $folder = $this->folder . $createNo . '/';
        if (!file_exists($folder)) {
            mkdir($folder, 0755, true);
        }

        if ($this->request->file('cut_image')) {
            $tmp_file = $this->request->file('cut_image')->getPathname();

            //이미지를 jpg로 변환
            $img = new \imagick($tmp_file);
            //$img->resizeImage( 1920, 1920, \Imagick::COMPOSITE_ATOP , 1, TRUE );
            $img->resizeImage($sceneAdmin['image_width'], $sceneAdmin['image_height'], \Imagick::COMPOSITE_ATOP, 1, TRUE);
            //$img->brightnessContrastImage($this->request['cut_info']['bright'],0);
            $img->setCompression(\Imagick::COMPRESSION_JPEG);
            $img->setCompressionQuality(100);
            $img->setImageFormat('jpeg');

            //write image on server
            $file_name = $sceneNo . '.jpg';
            $dest_file = $folder . $file_name;

            $img->writeImage($dest_file);
            $img->clear();
            $img->destroy();
            //이미지를 jpg로 변환

            $new_cut_image = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;
        }

        if ($sceneAdmin['type'] == "video") {

            $file_name = $sceneNo . '.mp4';
            $dest_file = $folder . $file_name;
            $firstTime = $this->request['cut_info']['timeline'] / 100;
            $lastTime = $sceneAdmin['video_time'];

            $file_name2 = $sceneNo . '_synthesis.jpg';
            $dest_file2 = $folder . $file_name2;

            $ffmpeg = \FFMpeg\FFMpeg::create();
            $video = $ffmpeg->open($folder . $sceneNo . '_basic.mp4');

            $ffprobeBit = \FFMpeg\FFProbe::create();

            $bitRate = $ffprobeBit
                    ->streams($folder . $sceneNo . '_basic.mp4')
                    ->videos()
                    ->first()
                    ->get('bit_rate') / 1024;

            if ($bitRate > 3000) {
                $bitRate = 4000;
            } else {
                $bitRate = 2000;
            }

            $format = new \FFMpeg\Format\Video\X264();
            $format
                ->setAudioCodec("libmp3lame");

            $video->clip(\FFMpeg\Coordinate\TimeCode::fromSeconds($firstTime), \FFMpeg\Coordinate\TimeCode::fromSeconds($lastTime))->save($format, $dest_file);

            $new_cut_movie = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;

            $sceneAdmin = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$sceneNo];
            $video_width = $last_width = $sceneAdmin['video_width'];
            $video_height = $last_height = $sceneAdmin['video_height'];

            $file_name = $sceneNo . '_.mp4';
            $dest_file3 = $folder . $file_name;

            //비디오 회전
            $ffmpeg = \FFMpeg\FFMpeg::create();
            $video = $ffmpeg->open($folder . $sceneNo . '.mp4');

            if ($this->request['cut_info']['rotate'] == 90) {
                $video->filters()
                    ->rotate(\FFMpeg\Filters\Video\RotateFilter::ROTATE_90)
                    ->resize(new \FFMpeg\Coordinate\Dimension($video_width, $video_height), 'inset');
            } else if ($this->request['cut_info']['rotate'] == 180) {
                $video->filters()
                    ->rotate(\FFMpeg\Filters\Video\RotateFilter::ROTATE_180)
                    ->resize(new \FFMpeg\Coordinate\Dimension($video_width, $video_height), 'inset');
            } else if ($this->request['cut_info']['rotate'] == 270) {
                $video->filters()
                    ->rotate(\FFMpeg\Filters\Video\RotateFilter::ROTATE_270)
                    ->resize(new \FFMpeg\Coordinate\Dimension($video_width, $video_height), 'inset');
            } else {
                $video->filters()
                    ->resize(new \FFMpeg\Coordinate\Dimension($video_width, $video_height), 'inset');
            }

            $format
                ->setKiloBitrate($bitRate)
                ->setAudioCodec("libmp3lame");

            $video->save($format, $dest_file3);

            rename($dest_file3, $dest_file);
            //비디오 회전


            //리사이징
            $video = $ffmpeg->open($folder . $sceneNo . '.mp4');
            $video->filters()->resize(new \FFMpeg\Coordinate\Dimension($video_width, $video_height), 'width');
            $video->save($format, $dest_file3);
            rename($dest_file3, $dest_file);
            //리사이징


            //비디오 블러
            $ffprobe = \FFMpeg\FFProbe::create();
            $dimension = $ffprobe
                ->streams($folder . $sceneNo . '.mp4') // extracts streams informations
                ->videos()                      // filters video streams
                ->first()                       // returns the first video stream
                ->getDimensions();
            $duration = $ffprobe
                ->streams($folder . $sceneNo . '.mp4')
                ->videos()
                ->first()
                ->get('duration');

            $last_width = (int)$last_width;
            $last_height = (int)$last_height;

            if ($dimension->getHeight() >= $dimension->getWidth()) {
                $file_name = $sceneNo . '_.mp4';
                $dest_file4 = $folder . $file_name;
                shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=black [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$sceneAdmin['video_height']}/{$sceneAdmin['video_width']}' -t {$duration} -vb 2000K {$dest_file4}");
                rename($dest_file4, $dest_file);
                /*
                if ($this->request['cut_info']['background']==2) {
                    $file_name = $sceneNo.'_.mp4';
                    $dest_file4 = $folder.$file_name;
                    shell_exec("ffmpeg -i {$dest_file} -lavfi '[0:v]scale=ih*{$sceneAdmin['video_width']}/{$sceneAdmin['video_height']}:-1,boxblur=luma_radius=min(h\,w)/20:luma_power=1:chroma_radius=min(cw\,ch)/20:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$sceneAdmin['video_height']}/{$sceneAdmin['video_width']}' -vb 2000K {$dest_file4}");
                    rename($dest_file4, $dest_file);
                } else if ($this->request['cut_info']['background']==1) {
                    $file_name = $sceneNo.'_.mp4';
                    $dest_file4 = $folder.$file_name;
                    shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=white [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$sceneAdmin['video_height']}/{$sceneAdmin['video_width']}' -t {$duration} -vb 2000K {$dest_file4}");
                    rename($dest_file4, $dest_file);
                } else if ($this->request['cut_info']['background']==0) {
                    $file_name = $sceneNo.'_.mp4';
                    $dest_file4 = $folder.$file_name;
                    shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=black [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$sceneAdmin['video_height']}/{$sceneAdmin['video_width']}' -t {$duration} -vb 2000K {$dest_file4}");
                    rename($dest_file4, $dest_file);
                }
                */
            } else {
                /*
                if (!empty($this->request['cut_info']['background'])) {
                    if ($this->request['cut_info']['background']==2) {
                        $file_name = $sceneNo.'_.mp4';
                        $dest_file4 = $folder.$file_name;
                        shell_exec("ffmpeg -i {$dest_file} -lavfi '[0:v]scale={$data['assets'][0]['width']}:{$data['assets'][0]['height']},boxblur=luma_radius=min(h\,w)/20:luma_power=1:chroma_radius=min(cw\,ch)/20:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=w=ih*{$data['assets'][0]['width']}/{$data['assets'][0]['height']}' -vb 2000K {$dest_file4}");
                        rename($dest_file4, $dest_file);
                    } else if ($this->request['cut_info']['background']==1) {
                        $file_name = $sceneNo.'_.mp4';
                        $dest_file4 = $folder.$file_name;
                        shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=white [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$data['assets'][0]['height']}/{$data['assets'][0]['width']}' -t {$duration} -vb 2000K {$dest_file4}");
                        rename($dest_file4, $dest_file);
                    } else if ($this->request['cut_info']['background']==0) {
                        $file_name = $sceneNo.'_.mp4';
                        $dest_file4 = $folder.$file_name;
                        shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=black [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$data['assets'][0]['height']}/{$data['assets'][0]['width']}' -t {$duration} -vb 2000K {$dest_file4}");
                        rename($dest_file4, $dest_file);
                    }
                } else {
                    $file_name = $sceneNo.'_.mp4';
                    $dest_file4 = $folder.$file_name;
                    shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=black [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$data['assets'][0]['height']}/{$data['assets'][0]['width']}' -t {$duration} -vb 2000K {$dest_file4}");
                    rename($dest_file4, $dest_file);
                }
                */
                $file_name = $sceneNo . '_.mp4';
                $dest_file4 = $folder . $file_name;
                shell_exec("ffmpeg -i {$dest_file} -lavfi 'color=size={$last_width}x{$last_height}:color=black [bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*{$sceneAdmin['video_height']}/{$sceneAdmin['video_width']}' -t {$duration} -vb 2000K {$dest_file4}");
                rename($dest_file4, $dest_file);
            }
            //비디오 블러

            $ffmpeg = \FFMpeg\FFMpeg::create();
            $video = $ffmpeg->open($folder . $sceneNo . '.mp4');
            $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(0))->save($dest_file2);

            //리사이즈
            $ffprobe = \FFMpeg\FFProbe::create();
            $dimension = $ffprobe
                ->streams($folder . $sceneNo . '.mp4') // extracts streams informations
                ->videos()                      // filters video streams
                ->first()                       // returns the first video stream
                ->getDimensions();

            $resize = new \Imagick($dest_file2);

            $getImg = file_get_contents($this->domain_info . $sceneAdmin['dbPathMerge']);

            if (!empty($getImg)) {
                $newImage = new \Imagick();
                $newImage->readImageBlob($getImg);
                $newImage->resizeImage(1920, 1080, \Imagick::COMPOSITE_ATOP, 1, TRUE);
            } else {
                $newImage = new \Imagick();
                $newImage->newImage(1920, 1080, new \ImagickPixel('transparent'));
            }


            if ($sceneAdmin['video_x']) {
                $startx = $sceneAdmin['video_x'];
            } else {
                $startx = (1920 - $resize->getImageGeometry()['width']) / 2;
            }

            if ($sceneAdmin['video_x']) {
                $starty = $sceneAdmin['video_y'];
            } else {
                $starty = (1080 - $resize->getImageGeometry()['height']) / 2;
            }

            $newImage->compositeImage($resize, \imagick::COMPOSITE_DEFAULT, $startx, $starty);
            $newImage->writeImage($dest_file2);

            $new_synthesis_image = '/storage/autorender/uploads/' . $createNo . '/' . $file_name2;
            $synthesis_image = $new_synthesis_image;

            $resize->clear();
            $resize->destroy();

        }

        if (empty($synthesis_image)) {
            $synthesis_image = '';
        }

        if (empty($sceneAdmin['preview'])) {
            $getImg = file_get_contents($this->domain_info . $sceneAdmin['dbPathMerge']);
            $img1 = new \Imagick();
            $img1->readImageBlob($getImg);

            $file_name = $sceneNo . '_synthesis.jpg';
            $dest_file = $folder . $file_name;
            $img1->resizeImage(650, 366, \Imagick::COMPOSITE_ATOP, 1, TRUE);
            $img1->writeImage($dest_file);
            $new_synthesis_image = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;
            $synthesis_image = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;

            $file_name = $sceneNo . '_synthesis_thumb.jpg';
            $dest_file = $folder . $file_name;
            $img1->resizeImage(276, 155, \Imagick::COMPOSITE_ATOP, 1, TRUE);
            $img1->writeImage($dest_file);
            $synthesis_thumb = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;

        } else if (!empty($new_cut_image) || $sceneAdmin['type'] == "video" || $this->request['text']) {
            $image_merge_result = $this->image_merge($sceneAdmin, $createNo, $sceneNo, $synthesis_image);
            $new_synthesis_image = $image_merge_result['synthesis'];
            $synthesis_image = $new_synthesis_image;
            $synthesis_thumb = $image_merge_result['synthesis_thumb'];
        }

        //사용자 장면 정보
        $userScene = UploadsScene::where('createNm', $createNo)->where('scene', $sceneNo)->first();

        if (empty($userScene)) {
            $scene = new UploadsScene;
        } else {
            $scene = $userScene;
        }


        if (!empty($new_basic_image)) {
            $scene->basic_image = $new_basic_image;
        }
        if (!empty($new_cut_image)) {
            $scene->cut_image = $new_cut_image;
        }
        if (!empty($new_basic_movie)) {
            $scene->basic_movie = $new_basic_movie;
        }
        if (!empty($new_filesize)) {
            $scene->filesize = $new_filesize;
        }
        if (!empty($new_filetype)) {
            $scene->filetype = $new_filetype;
        }
        if (!empty($new_cut_movie)) {
            $scene->cut_movie = $new_cut_movie;
        }
        if (!empty($new_synthesis_image)) {
            $scene->synthesis_image = $new_synthesis_image;
        }

        if (!empty($this->request['text'])) {
            $scene->text = json_encode($this->request['text'],JSON_UNESCAPED_UNICODE);
            $result['text'] = json_encode($this->request['text'],JSON_UNESCAPED_UNICODE);
        }
        
        $scene->createNm = $createNo;
        $scene->scene = $sceneNo;
        $scene->cut_info = $cut_info;
        $scene->complete = 1;
        $scene->save();

        $result['state'] = "ok";
        $result['complete'] = 1;
        $result['sceneNo'] = $sceneNo;

        if ($synthesis_image) {
            $result['synthesis_image'] = $synthesis_image . '?' . time();
        }
        if ($synthesis_thumb) {
            $result['synthesis_thumb'] = $synthesis_thumb . '?' . time();
        }
        if ($scene->basic_movie) {
            $result['basic_movie'] = $scene->basic_movie . '?' . time();
        }
        if ($scene->cut_info) {
            $result['cut_info'] = $scene->cut_info;
        }

        $userSceneCnt = UploadsScene::where('createNm', $createNo)->where('complete', '1')->count();
        $result['complete_cnt'] = $userSceneCnt;
        return json_encode($result, JSON_UNESCAPED_UNICODE);

    }

    public function postMovie()
    {
        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];

        $this->checkState($createNo);

        //관리자 등록정보 불러오기
        $adminUploads = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->first();
        $sceneInfo = json_decode(stripslashes($adminUploads['sceneInfo']), true);

        //사용자 업로드 정보 불러오기
        $userUploads = Upload::where('createNm', $createNo)->first();
        $userSceneCnt = UploadsScene::where('createNm', $createNo)->where('complete', '1')->count();
        $userScene = UploadsScene::where('createNm', $createNo)->where('complete', '1')->orderBy('scene', 'asc')->get();
        if (empty($userUploads)) {
            $result['state'] = "error";
            $result['msg'] = "잘못된 제작번호 입니다.";
        } else if ($userUploads->state == 3 && $userUploads->modifyCount == 1) {
            $result['state'] = "error";
            $result['msg'] = "이미 제작이 완료된 제작번호입니다.";
        } else if ($userUploads->state != 0 && $userUploads->state != 3) {
            $result['state'] = "error";
            $result['msg'] = "이미 제작 신청이 완료된 제작번호입니다.";
        } else if (count($sceneInfo) > $userSceneCnt) {
            $result['state'] = 'error';
            $result['msg'] = (count($sceneInfo) - $userSceneCnt) . "개의 장면이 저장되지 않았습니다.\n모든 장면을 저장하신 후 제작요청 해주세요.";
        } else {

            if ($userUploads['modifyCount'] == 1) {
                $result['state'] = "error";
                $result['msg'] = "이미 제작이 완료된 제작번호입니다.";
            } else {

                $json = array();

                $json['templateId'] = $adminUploads['templateId'];
                $json['templateName'] = $adminUploads['templateName'];
                $json['templatePath'] = $adminUploads['templatePath'];

                foreach ($userScene as $key => $value) {
                    $scene[$key + 1] = $value;
		}

		foreach ($sceneInfo as $key => $value) {
			
		    $json['scene'][$key]['type'] = $value['type'];

                    if (!empty($value['layer_name'])) {
                        $json['scene'][$key]['layername'] = $value['layer_name'];
                    }

                    if ($value['type'] == 'none') {
                        $json['scene'][$key]['file_path'] = '';
                    } else if ($value['type'] == 'photo') {
                        $json['scene'][$key]['file_path'] = $scene[$key]['cut_image'];
                    } else if ($value['type'] == 'video') {
                        $json['scene'][$key]['file_path'] = $scene[$key]['cut_movie'];
                        $json['scene'][$key]['cut_info'] = $scene[$key]['cut_info'];
                    } else if ($value['type'] == 'mp3') {
                        $json['scene'][$key]['file_path'] = $scene[$key]['cut_movie'];
                    }

                    if (!empty($value['text_layer_name']) && $value['text_use'] == "yes") {

                        $text = json_decode($scene[$key]['text'], true);

                        foreach ($value['text_layer_name'] as $text_key => $text_value) {
                            $json['scene'][$key]['text'][$text_key]['layername'] = $text_value;
                            $json['scene'][$key]['text'][$text_key]['text'] = !empty($text[$text_key])?$text[$text_key]:"";
                        }
                    }

		}

                //배경음 저장
                $userSceneMp3 = UploadsScene::where('createNm', $createNo)->where('complete', '1')->where('scene', '999')->first();
                if (!empty($userSceneMp3)) {
                    if (!empty($userSceneMp3->cut_movie)) {
                        $key++;
                        $json['scene'][$key]['type'] = 'mp3';
                        $json['scene'][$key]['file_path'] = $userSceneMp3->cut_movie;
                        $json['scene'][$key]['layername'] = $adminUploads->bgmLayerName;
                    }
                }

                $json = (object)$json;

                $userUploads->sceneInfoUser = json_encode($json, JSON_UNESCAPED_UNICODE);
                $userUploads->state = 1;

                if (!empty($this->request['sendPhone'])) {
                    $userUploads->odPhone = $this->request['sendPhone'];
                }

                $userUploads->save();


                //에일린서버로 전송
                $iumSender = new IumSenderController('autoRenderStart');
                $sendResult = $iumSender->send($createNo);

                if ($sendResult != 'ok') {
                    $result['state'] = "error";
                    $result['msg'] = "서버 전송에 실패했습니다. 다시 시도해 주세요.";
                } else {
                    $result['state'] = 'ok';
                    $result['msg'] = '제작 요청이 완료되었습니다.';
                }
            }


        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;

    }


    private function image_merge($data, $createNo, $sceneNo, $synthesis_image = '')
    {

        $getImg = file_get_contents($this->domain_info . $data['dbPathMerge']);

        if ($synthesis_image) {
            $img1 = new \Imagick($this->request->server('DOCUMENT_ROOT') . $synthesis_image);
        } else {
            $img1 = new \Imagick();
            $img1->readImageBlob($getImg);

            if ($getImg) {
                $back = new \Imagick();
                $back->readImageBlob($getImg);
                $back->thumbnailImage(1920, 1080, null);
            }

        }
        $img1->thumbnailImage(1920, 1080, null);
        $folder = $this->folder . $createNo . '/';

        $width = $data['image_width'] ? $data['image_width'] : $data['video_width'];
        $height = $data['image_height'] ? $data['image_height'] : $data['video_height'];
        $x = $data['image_x'] != "" ? $data['image_x'] : $data['video_x'];
        $y = $data['image_y'] != "" ? $data['image_y'] : $data['video_y'];

        //이미지 합성
        if (file_exists($folder . $sceneNo . '.jpg')) {

            $img2 = new \Imagick($folder . $sceneNo . '.jpg');
            $img2->thumbnailImage($width, $height, null);

            //이미지 왜곡정보
            if ($data['start_1'] != '' || $data['start_2'] != '') {
                $img2->setImageVirtualPixelMethod(\Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
                $img2->setImageMatte(true);

                $controlPoints = array($data['start_1'], $data['start_2'], //top left
                    $data['end_1'], $data['end_2'],

                    $data['start_3'], $data['start_4'], //top right
                    $data['end_3'], $data['end_4'],

                    $data['start_5'], $data['start_6'], //bottom right
                    $data['end_5'], $data['end_6'],

                    $data['start_7'], $data['start_8'], //bottum left
                    $data['end_7'], $data['end_8']);

                $img2->distortImage(\Imagick::DISTORTION_PERSPECTIVE, $controlPoints, true);
            }

            //이미지 회전정보
            // if($data['assets'][0]['stillcut_rotation']){
            //     $img2->rotateImage('#00000000', $data['assets'][0]['stillcut_rotation']);
            // }

            $img1->compositeImage($img2, \imagick::COMPOSITE_DEFAULT, $x, $y);

            if ((($x != 0 || $y != 0) && !empty($back)) || ($data['start_1'] != '' || $data['start_2'] != '') || $data['start_7'] != '') {
                $img1->compositeImage($back, \imagick::COMPOSITE_DEFAULT, 0, 0);
            }
        }

        //텍스트합성
        if (!empty($data['text_sample']) && $data['text_use'] == "yes") {
            foreach ($data['text_sample'] as $key => $value) {
                $usertext = !empty($this->request['text'][$key]) ? $this->request['text'][$key] : '';
                $draw = new \ImagickDraw();

                $text_x = $data['text_x'][$key];
                $text_y = $data['text_y'][$key];
                $text_align = $data['text_align'][$key];

                if ($text_align == "left") {
                    $draw->setTextAlignment(\Imagick::ALIGN_LEFT);
                } else if ($text_align == "right") {
                    $draw->setTextAlignment(\Imagick::ALIGN_RIGHT);
                } else if ($text_align == "center") {
                    $draw->setTextAlignment(\Imagick::ALIGN_CENTER);
                }

                $font = $this->request->server('DOCUMENT_ROOT') . '/storage/fonts/' . $data['text_family'][$key];

                if (!file_exists($font) || $data['text_family'][$key] == '') {
                    $font = $this->request->server('DOCUMENT_ROOT') . '/storage/fonts/NanumGothic.ttf';
                }

                $angle = 0;
                if (!empty($data['text_rotate'][$key])) {
                    $angle = $data['text_rotate'][$key];
                }

                $draw->setFont($font);
                $draw->setFontSize($data['text_size'][$key]);
                $draw->setFillColor("#" . "{$data['text_color'][$key]}");
                $img1->annotateImage($draw, $text_x, $text_y, $angle, $usertext);
            }
        }

        $file_name = $sceneNo . '_synthesis.jpg';
        $dest_file = $folder . $file_name;
        $img1->resizeImage(650, 366, \Imagick::COMPOSITE_ATOP, 1, TRUE);
        $img1->writeImage($dest_file);

        $result['synthesis'] = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;

        $file_name = $sceneNo . '_synthesis_thumb.jpg';
        $dest_file = $folder . $file_name;
        $img1->resizeImage(276, 155, \Imagick::COMPOSITE_ATOP, 1, TRUE);
        $img1->writeImage($dest_file);

        $result['synthesis_thumb'] = '/storage/autorender/uploads/' . $createNo . '/' . $file_name;

        $img1->clear();
        $img1->destroy();
        return $result;
    }

    private function text_merge($data, $createNo, $sceneNo)
    {
        //텍스트합성
        if (!empty($data['text_sample']) && $data['text_use'] == "yes") {
            foreach ($data['text_sample'] as $key => $value) {
                $usertext = $this->request['text'][$key] ? $this->request['text'][$key] : '';
                $draw = new \ImagickDraw();

                $text_x = $data['text_x'][$key];
                $text_y = $data['text_y'][$key];
                $text_align = $data['text_align'][$key];

                if ($text_align == "left") {
                    $draw->setTextAlignment(\Imagick::ALIGN_LEFT);
                } else if ($text_align == "right") {
                    $draw->setTextAlignment(\Imagick::ALIGN_RIGHT);
                } else if ($text_align == "center") {
                    $draw->setTextAlignment(\Imagick::ALIGN_CENTER);
                }

                $font = $this->request->server('DOCUMENT_ROOT') . '/storage/fonts/' . $data['text_family'][$key];

                if (!file_exists($font) || $data['text_family'][$key] == '') {
                    $font = $this->request->server('DOCUMENT_ROOT') . '/storage/fonts/NanumGothic.ttf';
                }

                $angle = 0;
                if (!empty($data['text_rotate'][$key])) {
                    $angle = $data['text_rotate'][$key];
                }

                $draw->setFont($font);
                $draw->setFontSize($data['text_size'][$key]);
                $draw->setFillColor("#" . "{$data['text_color'][$key]}");
                $img1->annotateImage($draw, $text_x, $text_y, $angle, $usertext);
            }
        }

        return $result;

    }


    public function autorotate(\Imagick $image)
    {
        switch ($image->getImageOrientation()) {
            case \Imagick::ORIENTATION_TOPLEFT:
                break;
            case \Imagick::ORIENTATION_TOPRIGHT:
                $image->flopImage();
                break;
            case \Imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateImage("#000", 180);
                break;
            case \Imagick::ORIENTATION_BOTTOMLEFT:
                $image->flopImage();
                $image->rotateImage("#000", 180);
                break;
            case \Imagick::ORIENTATION_LEFTTOP:
                $image->flopImage();
                $image->rotateImage("#000", -90);
                break;
            case \Imagick::ORIENTATION_RIGHTTOP:
                $image->rotateImage("#000", 90);
                break;
            case \Imagick::ORIENTATION_RIGHTBOTTOM:
                $image->flopImage();
                $image->rotateImage("#000", 90);
                break;
            case \Imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateImage("#000", -90);
                break;
            default: // Invalid orientation
                break;
        }
        $image->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
        return $image;
    }
}
