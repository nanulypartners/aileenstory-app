<?php

namespace Statamic\Addons\IumSender;

use Statamic\Extend\Controller;
use Statamic\Upload;

class IumSenderController extends Controller
{
    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */

    private $domain_info;

    public function __construct($type)
    {
        $this->domain_info = "http://aileenstory.com/";
        $this->type = $type;
    }
    

    public function send($data)
    {

        $arrData = array();
        $arrData['nanuly_type'] = $this->type;
        $arrData['nanuly'] = "sksnfl2020@";

        if ($this->type=="userInsert" || $this->type=="autoRenderStart") {
            $uploads = Upload::where('createNm', $data)->first();
            $arrData['uploads'] = $uploads;
        } else if ($this->type=="autoDel" || $this->type=="autoRenderEnd") {
            foreach ($data as $value) {
                $uploads = Upload::where('createNm', $value)->first();
                $uploads->oldRenderType = $uploads->renderType;
                $uploads->renderType = 0;
                $arrData['uploads'][] = $uploads;
                $uploads->save();
            }
        } else if ($this->type=="delReset") {
            foreach ($data as $value) {
                $uploads = Upload::where('createNm', $value)->first();
                $uploads->state = 0;
                $arrData['uploads'][] = $uploads;
                $uploads->save();
            }
        }

        $url = $this->domain_info."autorender/autorender_api.php";
        $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                  'Content-Type: application/json',
                                  'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);
        $result = 'ok';
        if ($output['state']!="success") {
            $result = 'error';
        }

        return $result;
        
    }
}
