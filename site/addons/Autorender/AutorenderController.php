<?php

namespace Statamic\Addons\Autorender;

use Statamic\Extend\Controller;

use Statamic\UpladsAdmin;
use Statamic\Upload;
use Statamic\UploadsScene;
use Statamic\order_number;

use Illuminate\Http\Request;
use Statamic\Addons\IumSender\IumSenderController;

use Statamic\CrontabLog;

class AutorenderController extends Controller
{
    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */

    private $request, $domain_info;

    public function __construct(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: x-csrf-token");
        $this->request = $request;
        $this->domain_info_img = "http://aileenstory.com/";
        $this->domain_info = "aileenstory.com";
        $this->domain_info2 = "aileenstory.com";
    }

    public function getLog()
    {
        $page = $this->request->segment(4);
        $limit = 20;
        $offset = $page;
        $logs = CrontabLog::orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
        $result = array();
        foreach ($logs as $key=>$log) {
            $result['log'][$key] = $log;
        }
        echo json_encode((object)$result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function getPreview()
    {
        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);
        $scene = $this->request->segment(6);

        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->first();
        $data = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$scene];
        $userUploads = UploadsScene::where('createNm', $createNo)->where('scene', $scene)->first();

        if (!empty($userUploads)) {
            $result['image'] = $userUploads['synthesis_image']?$userUploads['synthesis_image']."?".time():"https://app.aileenstory.com/api/image.php?path={$data['dbPath']}";
        } else {
            $result['image'] = "https://app.aileenstory.com/api/image.php?path={$data['dbPath']}";
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);

    }

    public function getScene()
    {
        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);
        $scene = $this->request->segment(6);

        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->first();
        $data = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$scene];

        $imageInfo = array();
        $videoInfo = array();

        if ($data['type']=="photo") {
            $imageInfo['width'] = $data['image_width'];
            $imageInfo['height'] = $data['image_height'];
        } else if ($data['type']=="video") {
            $videoInfo['width'] = $data['video_width'];
            $videoInfo['height'] = $data['video_height'];
            $videoInfo['video_time'] = $data['video_time'];
        }

        $imageInfo = json_encode((object)$imageInfo, JSON_UNESCAPED_UNICODE);
        $videoInfo = json_encode((object)$videoInfo, JSON_UNESCAPED_UNICODE);

        $sceneInfo = array('goodsNo'=>$goodsNo, 'createNo'=>$createNo, 'sceneNo'=>$scene);

        //사용자 업로드 장면정보 불러오기
        $userUploads = UploadsScene::where('createNm', $createNo)->where('scene', $scene)->first();
        if (!empty($userUploads['text'])) {
            $userUploads['text'] = json_decode($userUploads['text'], true);
        }

        return $this->view('scene', ['data'=>$data, 'imageInfo'=>$imageInfo, 'videoInfo'=>$videoInfo, 'sceneInfo'=>$sceneInfo, 'userUploads'=>$userUploads]);
    }

    public function getMp3()
    {
        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);
        $scene = 999;

        $userUploads = UploadsScene::where('createNm', $createNo)->where('scene', $scene)->first();

        $result = array();

        if (!empty($userUploads)) {
            $result['data'] = $userUploads;
        } else {
            $result['data'] = 'none';
        }
        
        $result['state'] = 'ok';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }


    public function getFirst()
    {
        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);

        //업로드 내역 있으면 불러오기
        $uploadsScene = UploadsScene::where('createNm', $createNo)->where('filesize', '!=', '0')->orderBy('scene')->get();
        $userUploads = array();

        foreach ($uploadsScene as $scene) {
            if($scene->synthesis_image || $scene->basic_image){
                $userUploads['scene'][$scene->scene]['user_upload_image'] = $scene->synthesis_image?$scene->basic_image:$scene->basic_image;
                $userUploads['scene'][$scene->scene]['user_upload_image_thumb'] = $this->thumbnail($userUploads['scene'][$scene->scene]['user_upload_image'], 276, 155);
            }
            $userUploads['scene'][$scene->scene]['complete'] = $scene->complete;
            $userUploads['scene'][$scene->scene]['type'] = $scene->type;
            $userUploads['scene'][$scene->scene]['tab'] = $scene->tab;
            $userUploads['scene'][$scene->scene]['sceneNo'] = $scene->scene;
        }

        $userSceneGroup = [];
        if (!empty($userUploads['scene'])) {
            $userSceneGroupArr = collect($userUploads['scene'])->groupBy('type');
            foreach ($userSceneGroupArr as $key=>$value) {
                foreach ($value as $k=>$val) {
                    if ($val['type']!='') {
                        $userSceneGroup[$val['type']][$val['tab']][] = $val;
                    }
                }
            }
        }

        return json_encode($userSceneGroup, JSON_UNESCAPED_UNICODE);
    }

    public function getSecond()
    {
        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);

        //상품정보 불러오기
        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('type', 0)->first();
        $sceneInfo = json_decode(stripslashes($goodsInfo->sceneInfo), true);
        $sceneInfo = $this->record_sort($sceneInfo, 'seq');

        //업로드 내역 있으면 불러오기
        $uploadsScene = UploadsScene::where('createNm', $createNo)->get();
        $userUploads = [];

        foreach ($uploadsScene as $scene) {
            if($scene->synthesis_image || $scene->basic_image){
                $userUploads[$scene->scene]['user_upload_image'] = $scene->synthesis_image?$scene->synthesis_image:$scene->basic_image;
                $userUploads[$scene->scene]['user_upload_image_thumb'] = $this->thumbnail($userUploads[$scene->scene]['user_upload_image'], 276, 155);
            }
            $userUploads[$scene->scene]['text'] = $scene->text;
            $userUploads[$scene->scene]['complete'] = $scene->complete;
            $userUploads[$scene->scene]['type'] = $scene->type;
            $userUploads[$scene->scene]['tab'] = $scene->tab;
            $userUploads[$scene->scene]['sceneNo'] = $scene->scene;
        }

        $sceneArr = [];
        foreach ($sceneInfo as $scene) {
            if (!empty($userUploads[$scene['seq']])) {
                $sceneArr[$scene['seq']]['complete'] = $userUploads[$scene['seq']]['complete'];
                if (!empty($userUploads[$scene['seq']]['user_upload_image'])) {
                    $sceneArr[$scene['seq']]['image'] = $userUploads[$scene['seq']]['user_upload_image']."?".time();
                    $sceneArr[$scene['seq']]['image_thumb'] = $userUploads[$scene['seq']]['user_upload_image_thumb']."?".time();
                } else {
                    //$sceneArr[$scene['seq']]['image'] = 'http://aileenstory.com/'.$scene['dbPath']."?".time();
                    //$sceneArr[$scene['seq']]['image_thumb'] = 'http://aileenstory.com/'.$scene['dbPath']."?".time();
                    $sceneArr[$scene['seq']]['image']  = "https://app.aileenstory.com/api/image.php?path={$scene['dbPath']}";
                    $sceneArr[$scene['seq']]['image_thumb']  = "https://app.aileenstory.com/api/image.php?path={$scene['dbPath']}";
                }

                if (!empty($userUploads[$scene['seq']]['text'])) {
                    $sceneArr[$scene['seq']]['text']  = json_decode($userUploads[$scene['seq']]['text'], true);
                } else if (!empty($scene['text_sample'])) {
                    $sceneArr[$scene['seq']]['text']  = $scene['text_sample'];
                }

            } else {
                //$sceneArr[$scene['seq']]['image'] = 'http://aileenstory.com/'.$scene['dbPath']."?".time();
                //$sceneArr[$scene['seq']]['image_thumb'] = 'http://aileenstory.com/'.$scene['dbPath']."?".time();
                $sceneArr[$scene['seq']]['image']  = "https://app.aileenstory.com/api/image.php?path={$scene['dbPath']}";
                $sceneArr[$scene['seq']]['image_thumb']  = "https://app.aileenstory.com/api/image.php?path={$scene['dbPath']}";
                $sceneArr[$scene['seq']]['complete'] = 0;
                if (!empty($scene['text_sample'])) {
                    $sceneArr[$scene['seq']]['text']  = $scene['text_sample'];
                }
            }
        }

        return json_encode($sceneArr, JSON_UNESCAPED_UNICODE);
    }
    

    public function getThird()
    {
        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);

        //상품정보 불러오기
        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('type', 0)->first();
        $sceneInfo = json_decode(stripslashes($goodsInfo->sceneInfo), true);
        $sceneInfo = $this->record_sort($sceneInfo, 'seq');

        //업로드 내역 있으면 불러오기
        $uploadsScene = UploadsScene::where('createNm', $createNo)->get();
        $userUploads = array();


        foreach ($uploadsScene as $scene) {
            if (!empty($scene->text)) {
                $texts = json_decode($scene->text);
                if (!empty($texts)) {
                    foreach ($texts as $key=>$text) {
                        $userUploads[$scene->scene][$key] = [
                            'no'=>$key,
                            'scene'=>$scene->scene,
                            'text'=>$text,
                        ];
                    }
                }
            }
        }

        $sceneArr = [];
        foreach ($sceneInfo as $scene) {
            if (!empty($scene['text_layer_name'])) {
                foreach ($scene['text_layer_name'] as $key=>$text) {

                    $userText = "";
                    $mandatory = "N";
                    if (!empty($scene['mandatory'][$key])) {
                        if ($scene['mandatory'][$key] == "Y") {
                            $mandatory = "Y";
                        }
                    }
                    /*
                                        if (!empty($scene['mandatory'][$key])) {
                                            if ($scene['mandatory'][$key]=="Y") {
                                                $mandatory = "Y";
                                                $text_user_yn = isset($userUploads[$scene['seq']][$key]['text'])?'Y':'N';
                                                $userText = isset($userUploads[$scene['seq']][$key]['text'])?$userUploads[$scene['seq']][$key]['text']:'';
                                            } else {
                                                $mandatory = "N";
                                                $text_user_yn = "Y";
                                                $text_user_yn_mandatory = isset($userUploads[$scene['seq']][$key]['text'])?'Y':'N';
                                                if (empty($userUploads[$scene['seq']][$key]['text'])) {
                                                    if (empty($userUploads[$scene['seq']])) {
                                                        $this->textSave($createNo, $scene['seq'], $key, $scene['text_sample'][$key]);
                                                        $userText = $scene['text_sample'][$key];
                                                    } else {
                                                        if (empty($userUploads[$scene['seq']][$key])) {
                                                            $this->textSave($createNo, $scene['seq'], $key, $scene['text_sample'][$key]);
                                                            $userText = $scene['text_sample'][$key];
                                                        } else {
                                                            if ($userUploads[$scene['seq']][$key]['text']) {
                                                                $this->textSave($createNo, $scene['seq'], $key, $scene['text_sample'][$key]);
                                                                $userText = $scene['text_sample'][$key];
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $userText = $userUploads[$scene['seq']][$key]['text'];
                                                }
                                            }
                                        } else {
                                            $mandatory = "N";
                                            $text_user_yn = "Y";
                                            $text_user_yn_mandatory = isset($userUploads[$scene['seq']][$key]['text'])?'Y':'N';
                                            if (empty($userUploads[$scene['seq']][$key]['text'])) {
                                                if (empty($userUploads[$scene['seq']])) {
                                                    $this->textSave($createNo, $scene['seq'], $key, $scene['text_sample'][$key]);
                                                    $userText = $scene['text_sample'][$key];
                                                } else {
                                                    if (empty($userUploads[$scene['seq']][$key])) {
                                                        $this->textSave($createNo, $scene['seq'], $key, $scene['text_sample'][$key]);
                                                        $userText = $scene['text_sample'][$key];
                                                    } else {
                                                        if ($userUploads[$scene['seq']][$key]['text']) {
                                                            $this->textSave($createNo, $scene['seq'], $key, $scene['text_sample'][$key]);
                                                            $userText = $scene['text_sample'][$key];
                                                        }
                                                    }
                                                }
                                            } else {
                                                $userText = $userUploads[$scene['seq']][$key]['text'];
                                            }
                                        }
                    */


                    $sceneArr[] = [
                        "scene"=>$scene['seq'],
                        "no"=>$key,
                        "text_no"=>$scene['text_no'][$key],
                        "image"=>"http://aileenstory.com/".$scene['dbPath'],
                        "text_sample"=>$scene['text_sample'][$key],
                        "text_type"=>$scene['text_type'][$key],
                        "text_user"=>isset($userUploads[$scene['seq']][$key]['text'])?$userUploads[$scene['seq']][$key]['text']:'',
                        "text_user_yn"=>isset($userUploads[$scene['seq']][$key]['text'])?'Y':'N',
                        //"text_user_yn"=>$text_user_yn,
                        "mandatory"=>$mandatory,
                        //"text_user_yn_mandatory"=>$text_user_yn_mandatory,
                    ];
                }
            }
        }

        return json_encode($sceneArr, JSON_UNESCAPED_UNICODE);
    }

    public function postBasicInfo($goodsNo, $createNo)
    {
        $result = array();

        $uploads = Upload::where('createNm', $createNo)->first();

        if (!empty($uploads)) {
            $basicInfo = json_encode((object)$this->request['introInputValue'], JSON_UNESCAPED_UNICODE);
            $uploads->basicInfo = $basicInfo;
            $uploads->save();

            $iumSender = new IumSenderController('userInsert');
            $sendResult = $iumSender->send($createNo);

            if ($sendResult!='ok') {
                $result['state'] = "error";
                $result['msg'] = "서버 전송에 실패했습니다. 다시 시도해 주세요.";
            } else {
                $result['state'] = "success";                
            }

        } else {
            $result['state'] = "error";
            $result['msg'] = "존재하지 않는 제작번호 입니다.";            
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function getUploads()
    {
        if(!$this->request->segment(4) || !$this->request->segment(5)){
            abort(404);
        }

        $goodsNo = $this->request->segment(4);
        $createNo = $this->request->segment(5);
        
        //리퍼러체크후 세션에 제작번호 저장
        if (strpos($this->request->headers->get('referer'), $this->domain_info) !== false || strpos($this->request->headers->get('referer'), $this->domain_info2) !== false) {
            $this->request->session()->put('createNo', $createNo);    
        }

        //세션에 제작번호가 동일한지 체크
        if ($this->request->session()->get('createNo')!=$createNo) {
            abort(404);
        }

        //제작번호 불러오기
        $uploads = Upload::where('createNm', $createNo)->first();
        if (empty($uploads)) {
            abort(404);
        }

        //상품정보 불러오기
        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 0)->where('goodsCd', $uploads->goodsCd)->where('type', 0)->first();
        if (empty($goodsInfo)) {
            abort(404);
        }

        //접근할 수 있는 상태인지 상태값 체크
        if ($uploads->state!=0 && $uploads->state!=3) {
            //수정불가 상태
            echo "<script>alert('수정이 불가능한 상태 입니다.');self.close();</script>";
            exit;
        }

        if ($uploads->modifyCount==2) {
            //수정가능 횟수 초과
            echo "<script>alert('수정가능 횟수가 초과되었습니다.');self.close();</script>";
            exit;
        }

        //업로드 내역 있으면 불러오기
        $uploadsScene = UploadsScene::where('createNm', $createNo)->get();
        $userUploads = array();


        foreach ($uploadsScene as $scene) {
            if($scene->synthesis_image || $scene->cut_image){
                $userUploads['scene'][$scene->scene]['user_upload_image'] = $scene->synthesis_image?$scene->synthesis_image:$scene->cut_image;
                $userUploads['scene'][$scene->scene]['user_upload_image_thumb'] = $this->thumbnail($userUploads['scene'][$scene->scene]['user_upload_image'], 276, 155);
            }
            $userUploads['scene'][$scene->scene]['complete'] = $scene->complete;
            $userUploads['scene'][$scene->scene]['type'] = $scene->type;
            $userUploads['scene'][$scene->scene]['tab'] = $scene->tab;
            $userUploads['scene'][$scene->scene]['sceneNo'] = $scene->scene;
        }

        $_token = csrf_token();

        $basicInfo = json_decode($goodsInfo->basicInfo, true);
        $sceneInfo = json_decode(stripslashes($goodsInfo->sceneInfo), true);
        $sceneInfo = $this->record_sort($sceneInfo, 'seq');


        $sceneGroupArr = collect($sceneInfo)->groupBy('type');
        $sceneGroup = [];
        $videoCnt = 0;
        $photoCnt = 0;
        $sceneText = [];
        foreach ($sceneGroupArr as $key=>$value) {
            foreach ($value as $k=>$val) {
                if ($val['type']!='none') {
                    $sceneGroup[$val['type']][$val['tab_name']][] = $val;
                    if ($val['type']=='photo') {
                        $photoCnt++;
                    } else if ($val['type']=='video') {
                        $videoCnt++;
                    }
                }
                if (!empty($val['text_layer_name'])) {
                    foreach ($val['text_layer_name'] as $textKey=>$text) {
                        $sceneText[] = [
                            "scene"=>$val['seq'],
                            "text_max_length"=>$val['text_max_length'][$textKey],
                            "text_type"=>$val['text_type'][$textKey],
                            "text_sample"=>$val['text_sample'][$textKey]
                        ];
                    }
                }
            }
        }

        $data = [
            'goodsInfo'=>$goodsInfo,
            'sceneInfo'=>$sceneInfo,
            'sceneGroup' =>$sceneGroup,
            'photoCnt' => $photoCnt,
            'videoCnt' => $videoCnt,
            'sceneText'=>$sceneText,

            'uploads'=>$uploads,
            'uploadsScene'=>$userUploads,

            '_token'=>$_token,
            'domain_info'=>$this->domain_info_img,
            'goodsNo'=>$goodsNo,
            'createNo'=>$createNo
        ];

    	return $this->view('uploads', $data);
    }

    public function thumbnail($img, $width, $height)
    {

        $filename = str_replace('.jpg', '', $img);
        $filename = $filename.'_thumb.jpg';

        if (!file_exists($this->request->server('DOCUMENT_ROOT').$filename)) {
            $resize = new \Imagick($this->request->server('DOCUMENT_ROOT').$img);
            $resize->resizeImage( 130.5, 73.41, \Imagick::COMPOSITE_ATOP , 1, TRUE );
            $resize->writeImage($this->request->server('DOCUMENT_ROOT').$filename);
        }

        return $filename;

    }


    public function record_sort($records, $field, $reverse=false)
    {
       $hash = array();

       foreach($records as $record)
       {
           $hash[$record[$field]] = $record;
       }

       ($reverse)? krsort($hash) : ksort($hash);

       $records = array();

       $i=1;
       foreach($hash as $record)
       {
           $records [$i]= $record;
           $i++;
       }

       return $records;
    }

}
