<!--<footer id="footer">-->
<!--    <div class="standard_container">-->
<!--        <button class="next next_move">장면편집으로 넘어가기</button>-->
<!--    </div>-->
<!--</footer>-->


<div class="modal file_loading_modal">
    <div class="modal_content">
        <div class="bouncingLoader">
            <div></div>
        </div>
        <p class="txt">사진을 배치하는 중입니다 잠시만 기다려주세요.</p>
    </div>
    <div class="back"></div>
</div>

<div class="modal alert_modal">
    <div class="modal_content">
        <h4>알림</h4>
        <div class="txt">
            사진은 최대 <span class="color"><span class="count"></span>장까지 첨부 가능합니다.</span>
        </div>
        <button class="close_btn">
            확인
        </button>
    </div>
    <div class="back"></div>
</div>

<div class="move_modal max_box">
    <div class="content">
        사진을 이동할 장면을 선택하세요
    </div>
</div>


<div class="modal music_modal">
    <div class="modal_content">
        <h4>알림</h4>
        <div class="text_box">
            <p>변경음악 길이에 맞춰 영상 진행속도를 조절하여</p>
            <p>음악 길이에 맞춰 제작됩니다.</p>
        </div>
        <button class="close_btn">
            확인
        </button>
    </div>
    <div class="back"></div>
</div>

<div class="modal create_modal">
    <div class="modal_content">
        <h4>알림</h4>
        <div class="txt">비어있는 장면을 모두 채워주세요.</div>
        <button class="close_btn">
            확인
        </button>
    </div>
    <div class="back"></div>
</div>

<div class="modal request_modal">
    <div class="modal_content">
        <h4>제작요청</h4>
        <div class="txt">
            <p>
                시안 영상 제작은 <span class="color">20분~30분 소요됩니다.</span>
            </p>
            <p>
                제작 완료 후 카톡or문자로 안내드리고 있습니다.
            </p>
            <p>
                영상 확인은 마이페이지 <span class="color">업로드 및 영상확인에서 가능합니다.</span>
            </p>
        </div>
        <button class="close_btn">
            확인
        </button>
    </div>
    <div class="back"></div>
</div>
