<section class="guide_content guide_page1">
    <button class="guide_close">
        <span class="line1"></span>
        <span class="line2"></span>
    </button>
    <div class="guide_line border">
        <p>정보를 모두 입력해주셔야</p>
        <p>다음단계로 넘어가실 수 있습니다.</p>
    </div>
    <button class="guide_close2">
        닫기
    </button>
    <div class="guide_back">

    </div>
</section>

<section class="guide_content guide_page2">
    <button class="guide_close">
        <span class="line1"></span>
        <span class="line2"></span>
    </button>
    <div class="guide_line">
        <div class="left content">
            <div class="top border">
                <div class="text_box">
                    <p>화면을 클릭하여 편집을 진행해주세요</p>
                </div>
            </div>
            <div class="btm border">
                <p>미리보거나 편집할 장면을 선택해주세요</p>
            </div>
        </div>
        <div class="right content">
            <div class="top border">
                <div class="text_box">
                    <p>사진을 <span>주황색 선에 맞춰 편집</span>해주세요</p>
                    <p>화면에 나타나는 공간입니다.</p>
                </div>
            </div>
            <div class="mid border">
                <p>편집 툴로 사진을 편집해주세요.</p>
            </div>
            <div class="btm border">
                <p>글귀를 편집해주세요.</p>
                <p>최대한 예시글과 글자수를 맞추시는 걸 추천드립니다.</p>
            </div>
            <div class="btn border">
                <p>저장을 꼭 눌러주셔야 <span>화면저장</span>이 됩니다</p>
            </div>
        </div>
    </div>
    <button class="guide_close2">
        닫기
    </button>
    <div class="guide_back">

    </div>
</section>