<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

@include('Autorender::head')

<body class="section-uploads">

    @include('Autorender::header')

    @yield('content')

    @include('Autorender::foot')

    
    <script src="/assets/js/babel.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="/assets/js/swiper.min.js"></script>
    <script type="text/babel" src="/assets/js/custom.js?v=1.0.0"></script>

    @if($test=="test")
    <script type="text/babel" src="/assets/js/phototest2.js?v=1.0.1"></script>
    @else
    <script type="text/babel" src="/assets/js/phototest.js?v=1.0.1"></script>
    @endif

    <script type="text/babel" src="/assets/js/video.js?v=1.0.0"></script>
    <script type="text/babel" src="/assets/js/text.js?v=1.0.0"></script>
    <script type="text/babel" src="/assets/js/mp3.js?v=1.0.0"></script>
    <script type="text/babel" src="/assets/js/autorender.js?v=1.0.0"></script>
    <script type="text/babel" src="/assets/js/guidance.js?v=1.0.0"></script>

</body>
</html>
