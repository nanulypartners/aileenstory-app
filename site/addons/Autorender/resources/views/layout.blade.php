<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

@include('Autorender::head')

<body class="section-uploads">

    @include('Autorender::header')

    @yield('content')

	@include('Autorender::foot')

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js" integrity="sha512-0bEtK0USNd96MnO4XhH8jhv3nyRF0eK87pJke6pkYf3cM0uDIhNJy9ltuzqgypoIFXw3JSuiy04tVk4AjpZdZw==" crossorigin="anonymous"></script>
    <script src="/assets/js/function.js?v=1.0.1"></script>
    <script src="/assets/js/autorender.js?v=1.0.1"></script>
    <script src="/assets/js/photo.js?v=1.0.2"></script>
    <script src="/assets/js/video.js?v=1.0.2"></script>
    <script src="/assets/js/text.js?v=1.0.2"></script>

    <!--    <script src="/assets/js/babel.min.js"></script>-->
<!--    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>-->
<!--    <script src="/assets/js/swiper.min.js"></script>-->
<!--    <script type="text/babel" src="/assets/js/custom.js?v=1.0.1"></script>-->
<!--    <script type="text/babel" src="/assets/js/video.js?v=1.0.0"></script>-->
<!--    <script type="text/babel" src="/assets/js/text.js?v=1.0.0"></script>-->
<!--    <script type="text/babel" src="/assets/js/mp3.js?v=1.0.0"></script>-->
<!--    <script type="text/babel" src="/assets/js/autorender.js?v=1.0.0"></script>-->
<!--    <script type="text/babel" src="/assets/js/guidance.js?v=1.0.0"></script>-->

</body>
</html>
