@extends('Autorender::layout')

@section('content')

<input type="hidden" name="_token" id="_token" value="{{$_token}}">
<input type="hidden" id="bgm" value="{{$goodsInfo->bgmLayerName or ''}}">
<input type="hidden" id="goodsNo" name="goodsNo" value="{{$goodsNo}}">
<input type="hidden" id="createNo" name="createNo" value="{{$createNo}}">


<div id="app">
    <header id="header">
        <div class="top">
            <div class="max_box">
                <img src="/assets/img/logo.png" alt="">
            </div>

        </div>
        <div class="btm">
            <div class="max_box">
                <h1>사진업로드</h1>
                <div class="btn_box">
                    <button class="prev_btn">
                        <img src="/assets/img/prev_icon.png" alt="">
                        <span>이전장면</span>
                    </button>
                    <button class="next_btn">
                        <span>장면편집</span>
                        <img src="/assets/img/next_icon.png" alt="">
                    </button>
                </div>
            </div>
        </div>
    </header>
    <section id="intro_content">
        <div id="content" class="max_box">
            <div class="left">
                <div class="sample_video_box">
                    <iframe src="{{$goodsInfo->sampleVideoUrl}}" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                </div>
                <div class="sample_text_box">
                    <div class="title">슬라이드</div>
                    <div class="text_box">
                        <span class="txt">사진 장수</span>
                        <span class="yellow">{{ $photoCnt }}장</span>
                    </div>
                    <div class="text_box">
                        <span class="txt">재생 시간</span>
                        <span class="yellow">{{ floor($goodsInfo->playTime/60) }}분 {{ ($goodsInfo->playTime % 60)<10?'0'.($goodsInfo->playTime % 60):$goodsInfo->playTime % 60 }}초</span>
                    </div>
                    <div class="text_box">
                        <span class="txt">배경 음악</span>
                        <span class="yellow">{{!empty($goodsInfo->bgmName)?$goodsInfo->bgmName:"없음"}}</span>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="photo_upload">

                    <ul class="tab_box">
                        @foreach($sceneGroup as $key=>$groups)
                        @foreach($groups as $k=>$group)
                        <li>
<!--                            @if(array_key_first($groups)==$k)-->
<!--                            <button class="active">-->
<!--                                @else-->
<!--                                <button>-->
<!--                                    @endif-->
<!--                                    {{$k}}-->
<!--                                </button>-->
                            <button>{{$k}}</button>
                        </li>
                        @endforeach
                        @endforeach
                    </ul>

                    @foreach($sceneGroup as $key=>$groups)
                    @foreach($groups as $k=>$group)
                    <div class="upload_list_box">
                        <div class="upload_list_header">
                            @if($key=='video')
                            <p>영상을
                                @elseif($key=='photo')
                            <p>사진을
                                @endif
                                <span><em class="count"></em>장</span> 더 첨부해주세요
                            </p>
                            <input type="hidden" value="{{ count($sceneGroup[$key][$k]) }}" class="total">
                        </div>
                        <form enctype="multipart/form-data" onsubmit="return false">
                            @if($key=='video')
                            <ul class="upload_list_content" data-type="video" data-tab_name="{{ $group[0]['tab_name'] }}">
                            @elseif($key=='photo')
                            <ul class="upload_list_content" data-type="photo" data-tab_name="{{ $group[0]['tab_name'] }}">
                            @endif
                                <li class="upload_btn" data-n="{{$k}}">
                                    <span class="line1"></span>
                                    <span class="line2"></span>
                                </li>
                            </ul>
                            @if($key=='video')
                            <input type="file" multiple="multiple" accept="video/*" class="upload_input screen_out"/>
                            @elseif($key=='photo')
                            <input type="file" multiple="multiple" accept="image/*" class="upload_input screen_out"/>
                            @endif
                        </form>
                    </div>
                    @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section id="scene_content" class="max_box">
        <div class="free_view">
            <div class="free_view_box">
                <img src="" alt="">
            </div>
        </div>
        <div class="left">
            <ul class="item_list_box">
                @if($goodsInfo->bgmChange=="Y")
                <li class="item_list music_box">
                    <div class="view_img">
                        <p class="music_title">노래 제목</p>
                    </div>
                    <div class="text_box">
                        <p>노래변경을 원하시는 경우</p>
                        <p>음악파일을 첨부해주세요</p>
                    </div>
                    <div class="input_box">
                        <i class="fas fa-music"></i>
                        <label for="music">음원 업로드</label>
                        <input type="file" id="music" accept="audio/*" >
                    </div>
                </li>
                @endif

                @foreach($sceneInfo as $key=>$scene)

                    @if (!empty($uploadsScene['scene'][$key]['complete']))
                        @if ($uploadsScene['scene'][$key]['complete']==1)
                        @else
                        @endif
                    @else
                    @endif
                    @if($scene['type']=='photo' || $scene['type']=='video')
                    <li class="item_list img_box file_box">
                    @else
                    <li class="item_list img_box text_box">
                    @endif
                        <div class="view_img">
                            <img src="" alt="">
                        </div>
                        <div class="center">
                            <p></p>
                        </div>
                        <div class="bottom">
                            <span>{{$scene['subject']}}</span>
                            @if($scene['type']=='photo')
                            <button>
                                순서변경
                                <img src="/assets/img/list_rotate_icon.png" alt="">
                            </button>
                            @elseif($scene['type']=='video')
                            <button class="active">
                            </button>
                            @endif
                        </div>
                        <div class="file_input_btn">
                            @if($scene['type']=='photo')
                            <input type="file" class="screen_out file_input" accept="image/*" multiple="multiple" />
                            @elseif($scene['type']=='video')
                            <input type="file" class="screen_out file_input" accept="video/*" multiple="multiple" />
                            @endif
                            <input type="hidden" name="sceneNo" class="sceneNo" value="{{$scene['seq']}}">
                            <input type="hidden" class="type" value="{{$scene['type']}}">
                        </div>
                    </li>

                @endforeach


            </ul>
        </div>

        <div class="right">
            <div class="user_controller">
                <div class="user_controller_box">

                </div>
            </div>
            <div class="btn_box">
                <button class="prev">
                    <img src="/assets/img/view_prev.png" alt="">
                    이전장면
                </button>
                <button class="save">
                    편집저장
                </button>
                <button class="next">
                    다음장면
                    <img src="/assets/img/view_next.png" alt="">
                </button>
            </div>
        </div>

    </section>
</div>
@endsection
