<input type="hidden" name="imageInfo" id="imageInfo" value="{{$imageInfo}}">
<input type="hidden" name="videoInfo" id="videoInfo" value="{{$videoInfo}}">
<input type="hidden" name="basicImage" id="basicImage" value="{{ isset($userUploads['basic_image'])?$userUploads['basic_image'].'?'.time():'' }}">
<input type="hidden" name="cutInfo" id="cutInfo" value="{{isset($userUploads['cut_info'])?$userUploads['cut_info']:''}}">

<form id="FormData">
    <input type="hidden" name="goodsNo" value="{{$sceneInfo['goodsNo']}}">
    <input type="hidden" name="createNo" value="{{$sceneInfo['createNo']}}">
    <input type="hidden" name="sceneNo" id="canvas_sceneNo" value="{{$sceneInfo['sceneNo']}}">


    <input type="hidden" name="imageUserInfo" id="imageUserInfo" value="">

<!--    <div class="help_msg pc">{{$data['help'] or ''}}</div>-->

    <div class="help_text_box">
        {{$data['help'] or ''}}
    </div>

@if($data['type']=='photo')
<div class="photo_content">
    <div class="controller_box">
        <div class="controll_ok photo_ok">
            <div class="icon_box">
                <button type="button" class="change" >
                    <img src="/assets/img/photo_upload_icon.png" alt="">
                </button>
                <button type="button" class="reset">
                    <img src="/assets/img/photo_icon_p1.png" alt="" class="icon">
                    <span class="text">리셋</span>
                </button>
                <button type="button"  class="rotate">
                    <img src="/assets/img/photo_icon_p4.png" alt="" class="icon">
                    <span class="text">회전</span>
                </button>
                <button type="button"  class="align">
                    <img src="/assets/img/photo_icon_p8.png" alt="" class="icon">
                    <span class="text">정렬</span>
                </button>
                <button type="button" class="mirror_open">
                    <img src="/assets/img/photo_icon_p9.png" alt="" class="icon">
                    <span class="text">여백</span>
                </button>
            </div>
            <div class="icon_box line">
                <button type="button"  class="bright_up">
                    <img src="/assets/img/photo_icon_brightness_up.png" alt="" class="icon">
                    <span class="text">밝게</span>
                </button>
                <button type="button"  class="bright_down">
                    <img src="/assets/img/photo_icon_brightness_down.png" alt="" class="icon">
                    <span class="text">어둡게</span>
                </button>
            </div>
            <div class="icon_box">
                <button type="button"  class="size_down">
                    <img src="/assets/img/photo_icon_p2.png" alt="" class="icon">
                    <span class="text">축소</span>
                </button>
                <button type="button"  class="size_up">
                    <img src="/assets/img/photo_icon_p3.png" alt="" class="icon">
                    <span class="text">확대</span>
                </button>
            </div>
<!---->
<!--            <button type="button"  class="size_down">-->
<!--                <span class="icon"></span>-->
<!--                <span class="text">축소</span>-->
<!--            </button>-->
<!--            <button type="button"  class="size_up">-->
<!--                <span class="icon"></span>-->
<!--                <span class="text">확대</span>-->
<!--            </button>-->
<!---->
<!--            <button type="button"  class="height_align">-->
<!--                <span class="icon"></span>-->
<!--                <span class="text">수직정렬</span>-->
<!--            </button>-->
        </div>
        <div class="mirror_box">
            <button type="button" class="mirror_white">
                <span class="icon"></span>
                <span class="text">흰색여백</span>
            </button>
            <button type="button" class="mirror_black">
                <span class="icon"></span>
                <span class="text">검정여백</span>
            </button>
            <button type="button" class="mirror_color">
                <span class="icon"></span>
                <span class="text">블러여백</span>
            </button>
        </div>
    </div>
    <div class="canvas_box">
        <canvas id="canvas"></canvas>
        <span class="point_line"></span>
    </div>
</div>
@elseif($data['type']=='video')
<div class="video_content">

    <div class="controller_box">
        <div class="controll_ok video_ok">
            <div class="icon_box">
                <button type="button"  class="change">
                    <img src="/assets/img/video_icon_p7.png" alt="" class="icon">
                    <span class="text">영상업로드</span>
                </button>
                <button type="button" class="cut">
                    <img src="/assets/img/video_icon_p2.png" alt="" class="icon">
                    <span class="text">편집</span>
                </button>
                <button type="button" class="rotate">
                    <img src="/assets/img/video_icon_p4.png" alt="" class="icon">
                    <span class="text">회전</span>
                </button>
                <button type="button" class="play">
                    <img src="/assets/img/video_icon_p6.png" alt="" class="icon">
                    <span class="text">재생</span>
                </button>
            </div>
        </div>
        <div class="cut_box slider_bar">
            <p class="txt">파란색 바를 이동하여 시작지점을 설정해주세요.</p>
            <div class="content">
                <div class="control_box">
                    <span class="number left">0</span>

                    <span class="balloon">20</span>
                    <input type="range" id="timeline" value="0" min="0" max="100">
                    <span class="number right" id="last_time"></span>


                    <div class="bar_box">
                        <span class="number center">4.5-8.5</span>
                    </div>
                </div>
                <button type="button" class="result_btn">확인</button>
            </div>
        </div>
        <div class="volum_box slider_bar">
            <p class="txt">음량을 조절해주세요.</p>
            <div class="content">
                <div class="control_box">
                    <span class="number left">0</span>
                    <input type="range" id="volume" value="0" min="0" max="100">
                    <span class="number right">100</span>
                    <span class="balloon">20</span>
                </div>
                <button type="button" class="result_btn">확인</button>
            </div>
        </div>
        <div class="mirror_box">
            <button type="button" class="mirror_white">
                <span class="icon"></span>
                <span class="text">흰색여백</span>
            </button>
            <button type="button" class="mirror_black">
                <span class="icon"></span>
                <span class="text">검정여백</span>
            </button>
            <button type="button" class="mirror_color">
                <span class="icon"></span>
                <span class="text">블러여백</span>
            </button>
        </div>
        <input type="hidden" id="video_rotate">
        <input type="hidden" id="video_background">

    </div>
    <div class="canvas_box">
        <div id="user_video">
            <video id="my_video" playsinline autoplay>
                <source src="{{!empty($userUploads['basic_movie'])?$userUploads['basic_movie'].'?'.time():''}}" type="">
            </video>
        </div>
<!--        <span class="point_line"></span>-->
    </div>
</div>
@else
<div class="text_content">
    <div class="canvas_box">
        <canvas id="canvas"></canvas>
    </div>
<!--    <div class="controller_box">-->
<!--        <div class="controll_not">-->
<!--            <p>편집하실 사진이 없습니다.</p> -->
<!--        </div>-->
<!--    </div>-->
</div>
@endif


<div class="text_input_box">
	@if($data['text_use']=="yes")
    @foreach($data['text_sample'] as $key=>$text_sample)
    <div class="text_ok">

            @if($data['text_type'][$key]=="text")
                <input class="text_input" type="text" name="text[]" placeholder="{{$text_sample}}" maxlength="{{$data['text_max_length'][$key]}}" value="{{$userUploads['text'][$key] or $text_sample}}">
            @else
                <textarea class="text_input" name="text[]" placeholder="{{$text_sample}}" maxlength="{{$data['text_max_length'][$key]}}">{{$userUploads['text'][$key] or $text_sample}}</textarea>
            @endif

        <button type="button" class="btn_box1">
            <img src="/assets/img/icon3.png" alt="">
        </button>
        <div class="btn_box2">
            <p>
                <span class="input_length">45</span>
                <span class="max_length">
                    /
                    @if(!empty($data['text_max_length'][$key]))
                    {{ $data['text_max_length'][$key] }}
                    @else
                    100
                    @endif
                </span>
            </p>
            <button type="button">
                <img src="/assets/img/icon4.png" alt="">
            </button>
        </div>
    </div>
    @endforeach
    @else
    <div class="text_not">
        @if($data['type']=='photo')
        <p>
            <img src="/assets/img/icon1.png" alt="">
            <span>사진 2장을 첨부하면 이여붙여 1장으로 넣기가 가능합니다.</span>
        </p>
        <p>
            <img src="/assets/img/icon2.png" alt="">
            <span>사진을 드래그하여 움직일 수 있습니다.</span>
        </p>
        @endif
    </div>
    @endif
</div>

<!--<div class="help_msg mobile">{{$data['help'] or ''}}</div>-->

</form>
