<?php

namespace Statamic\Addons\StacheUpdate;

use Statamic\Extend\Controller;
use Statamic\API\Stache;

class StacheUpdateController extends Controller
{
    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */
    public function index()
    {
	//return $this->view('index');
	exit;
    }

    public function getStacheUpdate()
    {
        Stache::update();
        exit;
    }

}
