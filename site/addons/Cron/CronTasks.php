<?php

namespace Statamic\Addons\Cron;

use Statamic\Extend\Tasks;
use Illuminate\Console\Scheduling\Schedule;

use Statamic\Upload;
use Statamic\UploadsScene;
use Statamic\CrontabLog;
use Illuminate\Support\Facades\Storage;
use Statamic\Addons\IumSender\IumSenderController;

class CronTasks extends Tasks
{
    /**
     * Define the task schedule
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    public function schedule(Schedule $schedule)
    {
        //자동삭제크론 시작
        /*
        $schedule->call(function () {

            $result = array();
            $result['type'] = "AutoDel";
            $result['date'] = date("Y-m-d H:i:s");


            //업로드파일 30일이후 삭제
            $delFileCreateNoArr = array();
            $day = date('Y-m-d', strtotime('-1 month'));
            $uploads = Upload::whereDate('created_at', '<=', $day)
                                ->where(function ($query) {
                                        $day = date('Y-m-d', strtotime('-1 month'));
                                        $query->whereDate('updated_at', '<=', $day)
                                              ->orWhere('updated_at', '=', '0000-00-00 00:00:00');
                                })->whereIn('state', [0,3])->get();

            foreach ($uploads as $upload) {

                if($upload->createNm){

                    $dir = '/home/nanu/apps/statamic/storage/autorender/';

                    //업로드 폴더 삭제
                    if (is_dir($dir.'uploads/'.$upload->createNm)) {
                        $delPath = '/storage/autorender/uploads/'.$upload->createNm;
                        Storage::deleteDirectory($delPath);
                    }

                    //업로드 장면 테이블 삭제
                    UploadsScene::where('createNm', $upload->createNm)->delete();

                    //제작번호 배열담기
                    $delFileCreateNoArr[] = $upload->createNm;
                }

            }
            //업로드파일 30일이후 삭제

            if (!empty($delFileCreateNoArr[0])) {
                $delFileCreateNoUnique = array_unique($delFileCreateNoArr);
                
                $result['createNoArr'] = $delFileCreateNoUnique; // 자동삭제 제작번호 리스트
                $iumSender = new IumSenderController('delReset');
                $sendResult = $iumSender->send($delFileCreateNoUnique);
                $result['msg'] = '파일 자동삭제로 상태가 제작대기로 변경 되었습니다.';

                $resultData = json_encode($result, JSON_UNESCAPED_UNICODE);
                $log = new CrontabLog;
                $log->type = 0;
                $log->result = $resultData;
                $log->save();
            }


            //주문 생성일이 2달 지난 주문건 불러오기
            $createNoArr = array();
            $day = date('Y-m-d', strtotime('-100 days'));
            $uploads = Upload::whereDate('created_at', '<=', $day)
                                ->where(function ($query) {
                                        $day = date('Y-m-d', strtotime('-100 days'));
                                        $query->whereDate('updated_at', '<=', $day)
                                              ->orWhere('updated_at', '=', '0000-00-00 00:00:00');
                                })->whereIn('state', [0,3])->get();

            foreach ($uploads as $upload) {

                if($upload->createNm){

                    $dir = '/home/nanu/apps/statamic/storage/autorender/';

                    //업로드 폴더 삭제
                    if (is_dir($dir.'uploads/'.$upload->createNm)) {
                        $delPath = '/storage/autorender/uploads/'.$upload->createNm;
                        Storage::deleteDirectory($delPath);
                    }

                    //무료 시안영상 삭제
                    if (file_exists($dir.'/home/nanu/apps/statamic/storage/autorender/draft/'.$upload->createNm.'.mp4')) {
                        Storage::delete('/storage/autorender/draft/'.$upload->createNm.'.mp4');
                    }

                    //원본 시안영상 삭제
                    if (file_exists($dir.'/home/nanu/apps/statamic/storage/autorender/video/'.$upload->createNm.'.mp4')) {
                        Storage::delete('/storage/autorender/video/'.$upload->createNm.'.mp4');
                    }

                    //업로드 장면 테이블 삭제
                    UploadsScene::where('createNm', $upload->createNm)->delete();

                    //업로드 테이블 상태 업데이트
                    $upload_update = Upload::where('createNm', $upload->createNm)->first();
                    $upload_update->timestamps = false;
                    $upload_update->sceneInfoUser = '';
                    $upload_update->sianPath = '';
                    $upload_update->moviePath = '';
                    $upload_update->state = 4;
                    $upload_update->save();

                    //제작번호 배열담기
                    $createNoArr[] = $upload->createNm;
                }

            }

            //자동삭제 제작번호 리스트 에일린서버로 전송
            if (!empty($createNoArr)) {
                $result['createNoArr'] = $createNoArr; // 자동삭제 제작번호 리스트
                $iumSender = new IumSenderController('autoDel');
                $sendResult = $iumSender->send($createNoArr);

                $result['msg'] = '자동삭제 되었습니다.';

                if ($sendResult!='ok') {
                    $result['msg'] = "자동삭제건 에일린서버 전송에 실패했습니다.";
                }
            } else {
                $result['msg'] = '자동삭제 대상이 존재하지 않습니다.';
            }

            //자동삭제 결과 출력
            $resultData = json_encode($result, JSON_UNESCAPED_UNICODE);
            $log = new CrontabLog;
            $log->type = 0;
            $log->result = $resultData;
            $log->save();

            echo $resultData;

        })->cron('30 4 * * *');
        */
        //자동삭제크론 종료

        //자동 렌더링 크론 시작
        $schedule->call(function () {

            $api = "http://app.aileenstory.com:3050/api/v1/jobs";
            $upload_http = "https://app.aileenstory.com";
            $output_path = "/home/nanu/apps/statamic/storage/autorender/";
            $rs_template = "Draft Settings";

            $rendered = Upload::where('state', '2')->get();

            $result = array();
            $result['data'] = "";
            $result['type'] = "autoRenderEnd";
            $result['date'] = date("Y-m-d H:i:s");

            $renderEndList = array();
            $renderEndListAll = array();

            foreach($rendered as $render) {

                if ($render->renderType == 1) {
                    $folder = "video";
                    $rs_template = "Best Settings";
                } else {
                    $folder = "draft";
                    $rs_template = "Draft Settings";
                }

                if(file_exists($output_path . $folder . '/' . $render->createNm .'.mp4')) {

                    if ($render->renderType == 1) {
                        $render->moviePath = '/storage/autorender/' . $folder .'/' . $render->createNm.'.mp4';
                        $renderEndListAll[] = [ $render->createNm => $render->moviePath ];
                        $this->slack($render->createNm . " Studio Ium (".$rs_template.") render done. " . $upload_http . $render->moviePath);                        
                    } else {
                        $render->sianPath = '/storage/autorender/' . $folder .'/' . $render->createNm.'.mp4';
                        $renderEndListAll[] = [ $render->createNm => $render->sianPath ];
                        $this->slack($render->createNm . " Studio Ium (".$rs_template.") render done. " . $upload_http . $render->sianPath);                        
                    }
                    
                    $render->state = 3;
                    $render->save();

                    $renderEndList[] = $render->createNm;

		} else {
		    $current_time = new \DateTime("now");
		    $render_time = new \DateTime($render->updated_at);
                    $interval = date_diff($current_time, $render_time);

		    if($interval === 1) echo "Warning: (".$interval->format('%h').")".$render->createNm." should have rendered.\n";
		}
            }

            if (!empty($renderEndList)) {
                $result['renderEndListAll'] = $renderEndListAll;
                $result['msg'] = "렌더링이 완료되었습니다.";

                //에일린서버 전송
                $iumSender = new IumSenderController('autoRenderEnd');
                $sendResult = $iumSender->send($renderEndList);
                if ($sendResult!='ok') {
                    $result['error'] = "렌더링완료건 에일린서버 전송에 실패했습니다.";
                }
                //에일린서버 전송

                //자동렌더링 완료 결과 출력
                $resultData = json_encode($result, JSON_UNESCAPED_UNICODE);
                $log = new CrontabLog;
                $log->type = 1;
                $log->result = $resultData;
                $log->save();
                echo $resultData;
            }

            $upload = Upload::where('state', '1')->latest()->first();

            $result = array();
            $result['type'] = "autoRenderStart";
            $result['date'] = date("Y-m-d H:i:s");

            if(empty($upload)) {
                $result['msg'] = "No data to render";
                $resultData = json_encode($result, JSON_UNESCAPED_UNICODE);
            } else {

                if($upload->renderType == 1) {
                    $rs_template = "Best Settings";
                    $folder = "video";
                    $result['renderType'] = "best";
                    $upload->moviePath = '';
                } else {
                    $rs_template = "Draft Settings";
                    $folder = "draft";
                    $result['renderType'] = "draft";
                    $upload->sianPath = '';

                    if ($upload->payment > 0) {
                        $upload->modifyCount = $upload->modifyCount+1;
                    }
                }

                $data = json_decode($upload->sceneInfoUser, true);
                $templates = array();
                $assets = array();

                $templates["oid"] = $upload->createNm;
                $templates["template"] = array();
                $templates["template"]["src"] = "file:///" . str_replace("iumstudio", "data\\iumstudio\\files", $data["templatePath"]);
                $templates["template"]["composition"] = "main";
                $templates["template"]["continueOnMissing"] = true;
                $templates["template"]["settingsTemplate"] = $rs_template;

                if(empty($data['scene'])) $data['scene'] = array();

                foreach($data['scene'] as $scene) {

                    if(!empty($scene['file_path'])) {
                        $scene['file_path'] = str_replace("\\", "", $scene['file_path']);
                        $scene['file_path'] = str_replace("/storage/smartstore/", "", $scene['file_path']);

                        if($scene['type']=='photo') {
                            $assets[] = array('type'=>'image', 'layerName'=>$scene['layername'], 'src'=>$upload_http . $scene['file_path']);
                        } else if($scene['type']=='video') {
                            $assets[] = array('type'=>'video', 'cutInfo'=>$scene['cut_info'], 'layerName'=>$scene['layername'], 'src'=>$upload_http . $scene['file_path']);
                        } else if($scene['type']=='mp3') {
                            $assets[] = array('type'=>'audio', 'layerName'=>$scene['layername'], 'src'=>$upload_http . $scene['file_path']);
                        }
                    }

                    if (!empty($scene['text'])) {
                        foreach($scene['text'] as $text) {
                            $assets[] = array('type' => 'data', 'layerName' => $text['layername'], 'property' => "Source Text", 'value' => $text['text']);
                        }
                    }

                }

		if($folder == "video")  $assets[] = array('type'=>'image', 'layerName'=>'watermark', 'src'=>'https://movie.aileenstory.com/ium-result/null.png');

                $templates["assets"] = $assets;

                $templates['actions']['postrender'][0] = array('module'=>'@nexrender/action-encode', 'preset'=>'mp4', 'output' => $upload->createNm.'.mp4');
                $templates['actions']['postrender'][1] = array('module'=>'@nexrender/action-copy', 'input'=> $upload->createNm.'.mp4', 'output'=> 'E:/Work/data/iumstudio/files/' . $folder . '/' . $upload->createNm.'.mp4');
                $templates['actions']['postrender'][2] = array('module'=>'E:/Work/data/iumstudio/post-render/index.js', 'id'=> $upload->createNm, 'folder'=>$folder);

                $json = json_encode($templates,  JSON_UNESCAPED_SLASHES);

                $handle = curl_init();
                curl_setopt($handle, CURLOPT_URL, $api);
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($handle, CURLOPT_HTTPHEADER, array(
                    'content-type: application/json'
                ));
                curl_setopt($handle, CURLOPT_POSTFIELDS, $json);
                $result_curl = curl_exec($handle);
                curl_close($handle);

		json_decode($result_curl);

                if (json_last_error() === JSON_ERROR_NONE) {
                    $this->slack($upload->createNm . " Studio Ium (".$rs_template.") render started.");
                    var_dump($result_curl);
		} else {
                    $this->slack($upload->createNm . " Studio Ium (".$rs_template.") render failed.\n");
                    var_dump($result_curl);
		}

                if(file_exists($output_path . $folder . '/' . $upload->createNm .'.mp4')) {
                    unlink($output_path . $folder . '/' . $upload->createNm . '.mp4');    
                }

                $upload->state = 2;
                $upload->save();

                $result['data'] = $json;
                $result['msg'] = "렌더링이 시작되었습니다.";

                //에일린서버 전송
                $iumSender = new IumSenderController('autoRenderStart');
                $sendResult = $iumSender->send($upload->createNm);
                if ($sendResult!='ok') {
                    $result['error'] = "렌더링시작건 에일린서버 전송에 실패했습니다.";
                }
                //에일린서버 전송
                $resultData = json_encode($result, JSON_UNESCAPED_UNICODE);
                $log = new CrontabLog;
                $log->type = 1;
                $log->result = $resultData;
                $log->save();

            }

            //자동렌더링 시작 결과 출력
            echo $resultData;

        })->everyMinute()->name('Auto-Render'); //->withoutOverlapping();
        //자동 렌더링 크론 종료

    }


    public function slack($message)
    {
        /*
        $ch = curl_init(
        //"https://hooks.slack.com/services/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"
	"https://hooks.slack.com/services/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"
        );
        $data = array('payload' => json_encode(array('text' => stripslashes($message))));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        */
    }

   public function file_exists($url) 
   {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($responseCode == 200){
            return true;
        } else {
            return false;
        }
    }

}
