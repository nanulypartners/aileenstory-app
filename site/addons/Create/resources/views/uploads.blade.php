{{--@extends('Create::layout')--}}

@section('content')

<input type="hidden" name="_token" id="_token" value="{{$_token}}">
<input type="hidden" id="goodsNo" name="goodsNo" value="{{$goodsInfo->no}}">
<input type="hidden" id="createNo" name="createNo" value="{{$uploads->createNo}}">


<div id="app">
    <header id="header">
        <div class="top">
            <div class="max_box">
                <img src="/assets/img/logo.png" alt="">
            </div>

        </div>
        <div class="btm">
            <div class="max_box">
                <h1>사진업로드</h1>
                <div class="btn_box">
                    <button class="next_btn">
                        <span>제작요청</span>
                    </button>
                </div>
            </div>
        </div>
    </header>
    <section id="intro_content">
        <div id="content" class="max_box">
            <div class="left">
                <div class="sample_video_box">
                    <iframe src="{{$goodsInfo->sampleVideoUrl}}" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                </div>
                <div class="sample_text_box">
                    <div class="title">슬라이드</div>
                    <div class="text_box">
                        <span class="txt">사진 장수</span>
                        <span class="yellow">{{ $photoCnt }}장</span>
                    </div>
                    <div class="text_box">
                        <span class="txt">재생 시간</span>
                        <span class="yellow">{{ floor($goodsInfo->playTime/60) }}분 {{ ($goodsInfo->playTime % 60)<10?'0'.($goodsInfo->playTime % 60):$goodsInfo->playTime % 60 }}초</span>
                    </div>
                    <div class="text_box">
                        <span class="txt">배경 음악</span>
                        <span class="yellow">{{!empty($goodsInfo->bgmName)?$goodsInfo->bgmName:"없음"}} {{$goodsInfo->bgmChange=="N"?"(변경불가)":""}}</span>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="photo_upload">
                    <ul class="tab_box">

                        @if(!empty($basicInfo))
                        <li>
                            <button>기본정보</button>
                        </li>
                        @endif

                        @foreach($sceneGroup as $key=>$groups)
                        <li>
                            <button>
                                @if($key=="photo")
                                사진
                                @elseif($key=="video")
                                영상
                                @elseif($key=="text")
                                자막
                                @endif
                            </button>
                        </li>
                        @endforeach

<!--                        @if($goodsInfo->bgmChange=="Y")-->
<!--                        <li><button>배경음악</button></li>-->
<!--                        @endif-->

                    </ul>

                    @if(!empty($basicInfo))
                    <div class="upload_list_box info_box">
                        <div class="input_box">
                            @foreach($basicInfo as $key=>$val)
                            <input type="text" name="introInputValue[]" placeholder="{{$val}}" value="{{!empty($uploadsBasic[$key])?$uploadsBasic[$key]:''}}">
                            @endforeach
                            <button type="button" class="info_save">기본정보 저장</button>
                        </div>
                    </div>
                    @endif

                    @foreach($sceneGroup as $key=>$groups)
                    <div class="upload_list_box">
                        @foreach($groups as $k=>$group)
                        <div class="list_box">
                            @if($key=="photo" || $key=="video")
                            <div class="upload_list_header">
                                <p>{{$group[0]['tabname']}}을 <span><em class="count">{{ $group[0]['file_count'] }}</em>장</span> 첨부해주세요</p>
                                <input type="hidden" value="{{ $group[0]['file_count'] }}" class="total">
                            </div>
                            @if($key=='video')
                            <ul class="upload_list_content box_{{ $group[0]['seq'] }}" data-type="video">
                            @elseif($key=='photo')
                            <ul class="upload_list_content box_{{ $group[0]['seq'] }}" data-type="photo">
                            @endif
                                @if(!empty($userUploads[$key][$k]))
                                @foreach($userUploads[$key][$k] as $userUpload)
                                <li class="img_container">
                                    <div class="img_box">
                                        <img src="{{ $userUpload['fileThumb'] }}" data-scene="{{ $userUpload['scene'] }}" alt="">
                                        <div class="remove_btn" data-scene="{{ $userUpload['scene'] }}" data-sceneSub="{{ $userUpload['sceneSub'] }}">
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @endif
                                <li class="upload_btn">
                                    <span class="line1"></span>
                                    <span class="line2"></span>
                                </li>
                            </ul>

                            @if($key=='video')
                            <input type="file" multiple="multiple" accept="video/*" data-scene="{{ $group[0]['seq'] }}" class="upload_input screen_out"/>
                            @elseif($key=='photo')
                            <input type="file" multiple="multiple" accept="image/*" data-scene="{{ $group[0]['seq'] }}" class="upload_input screen_out"/>
                            @endif
                            @elseif($key=="text")
                            <div class="upload_list_header">
                                <p>{{$k}} ({{$group[0]['title']}})</p>
                            </div>

                            <div class="input_box text_input_box">

                                @foreach($group[0]['text_type'] as $t=>$text)
                                    @if($text=="text")
                                        <input class="input" type="text" placeholder="{{$group[0]['text_sample'][$t]}}" value="{{$userUploads['text'][$k]['text'][$t] or ''}}">
                                    @elseif($text=="textarea")
                                        <textarea class="input" name="" id="" cols="30" placeholder="{{$group[0]['text_sample'][$t]}}"
                                              rows="10">{{$userUploads['text'][$k]['text'][$t] or ''}}</textarea>
                                    @endif
                                @endforeach
                                <div>
                                    <button class="text_save" data-scene="{{ $group[0]['seq'] }}">자막 저장</button>
                                </div>
                            </div>

                            @endif
                        </div>
                        @endforeach
                    </div>
                    @endforeach

<!--                    @if($goodsInfo->bgmChange=="Y")-->
<!--                    <div class="upload_list_box">-->
<!--                        <input type="file" class="upload_input screen_out"/>-->
<!--                    </div>-->
<!--                    @endif-->

                </div>
            </div>
        </div>

    </section>


</div>
@endsection
