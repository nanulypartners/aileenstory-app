<head>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="HandheldFriendly" content="true" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css" />
    <link rel="stylesheet" href="/assets/css/swiper.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css?v=1.0.0">
    <link rel="stylesheet" href="/assets/css/create.css?v=1.0.0">


</head>
