<?php

namespace Statamic\Addons\Create;

use Statamic\Create;
use Statamic\CreateScene;
use Statamic\Extend\Controller;

use Statamic\UpladsAdmin;

use Illuminate\Http\Request;
use Statamic\Addons\IumSender\IumSenderController;

class CreateController extends Controller
{

    private $request, $domain_info, $domain_info_img, $folder, $path;

    public function __construct(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: x-csrf-token");
        $this->request = $request;
        $this->domain_info_img = "http://aileenstory.com/";
        $this->domain_info = "aileenstory.com";
        $this->folder = $request->server('DOCUMENT_ROOT').'/storage/create/uploads/';
        $this->path = '/home/nanu/apps/statamic/storage/create/uploads/';
        $this->storege_folder = '/storage/create/uploads/';
    }

    public function checkState($createNo, $sceneNo='')
    {
        //사용자 업로드 정보
        $userUploads = Create::where('createNo', $createNo)->first();
        $result['sceneNo'] = $sceneNo;

        if (!empty($userUploads->state)) {
            if($userUploads->state==3){
                $result['state'] = 'error';
                $result['msg'] = '이미 제작이 완료된 무비입니다.';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit;
            }else if($userUploads->state!=0 && $userUploads->state!=3){
                $result['state'] = "error";
                $result['msg'] = "신청이 완료된 무비는 수정이 불가능 합니다.";
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit;
            }
        }

    }

    public function postBasicInfo()
    {

        $createNo = $this->request['createNo'];
        $result = array();
        $this->checkState($createNo);

        $uploads = Create::where('createNo', $createNo)->first();

        if (!empty($uploads)) {
            $basicInfo = json_encode((object)$this->request['introInputValue'], JSON_UNESCAPED_UNICODE);
            $uploads->basicInfo = $basicInfo;
            $uploads->save();
            $result['state'] = "success";
            $result['msg'] = "저장이 완료되었습니다.";
        } else {
            $result['state'] = "error";
            $result['msg'] = "존재하지 않는 제작번호 입니다.";
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function postMovie()
    {
        $goodsType = $this->request['goodsType'];
        $goodsCd = $this->request['goodsCd'];
        $createNo = $this->request['createNo'];

        $this->checkState($createNo);

        //관리자 등록정보
        $adminUploads = UpladsAdmin::where('goodsType', $goodsType)->where('goodsCd', $goodsCd)->where('useYn', 1)->where('type', 1)->first();
        $adminBasic = json_decode($adminUploads['basicInfo'], true);
        $adminScene = json_decode(stripslashes($adminUploads['sceneInfo']), true);

        //사용자 등록정보
        $userUploads = Create::where('createNo', $createNo)->first();
        $userScene = CreateScene::where('createNo', $createNo)->orderBy('scene', 'asc')->get();
        $userUploadsBasic = json_decode($userUploads['basicInfo'], true);

        $sceneArray = [];
        foreach ($userScene as $scene) {
            $sceneArray[$scene['scene']][$scene['sceneSub']] = $scene;
        }

        //기본정보 저장 완료 체크
        foreach ($adminBasic as $key=>$val) {
            if (empty($userUploadsBasic[$key])) {
                $result['state'] = "error";
                $result['msg'] = "기본정보를 모두 입력해 주세요.";
                return json_encode($result, JSON_UNESCAPED_UNICODE);
            }
        }

        //업로드 및 자막 저장 완료 체크
        foreach ($adminScene as $key=>$val) {
            if ($val['type']=="text") {
                if (empty($sceneArray[$key])) {
                    $result['state'] = "error";
                    $result['msg'] = $val['tabname'] . "의 자막을 입력해 주세요.";
                    return json_encode($result, JSON_UNESCAPED_UNICODE);
                }
            } else {
                // $val['file_count'] 파일수 , $val['tabname'] 탭이름
                if ($val['file_count']!=count($sceneArray[$key])) {
                    $result['state'] = "error";
                    $result['msg'] = $val['tabname'] . "의 파일을 모두 업로드 해주세요.";
                    return json_encode($result, JSON_UNESCAPED_UNICODE);
                }
            }
        }

        $userUploads->state = 1;

        //DB저장 및 에일린서버로 전송
        $url = $this->domain_info."autorender/create_api.php";
        $json['data'] = $userUploads;
        $json['nanuly'] = "sksnfl2020@";
        $json['nanuly_type'] = "insert";
        $json_data = json_encode((object)$json, JSON_UNESCAPED_UNICODE);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($json_data)));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output, true);

        $result['state'] = "success";
        $result['msg'] = "제작요청이 완료되었습니다.";

        if ($output['state']!="success") {
            $result['state'] = "error";
            $result['msg'] = "통신오류가 발생했습니다. 다시 시도해 주세요.";
        }

        $userUploads->update();

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }


    public function postMp3()
    {
        $goodsNo = $this->request['goodsNo'];
        $createNo = $this->request['createNo'];
        $sceneNo = 999;

        $this->checkState($createNo);

        $goodsInfo = UpladsAdmin::where('no', $goodsNo)->where('useYn', 1)->where('type', 1)->first();

        //사용자 장면 정보
        $userScene = CreateScene::where('createNo', $createNo)->where('scene', $sceneNo)->first();

        if(empty($userScene)){
            $scene = new CreateScene;
        }else{
            $scene = $userScene;
        }

        $folder = $this->folder.$createNo.'/';
        if (!file_exists($folder)) {
            mkdir($folder, 0755, true);
        }

        if($this->request->file('mp3')){

            $tmp_file = $this->request->file('mp3')->getPathname();
            $originalName = $this->request->file('mp3')->getClientOriginalName();

            $ffprobe = \FFMpeg\FFProbe::create();

            $duration = $ffprobe
                ->format($tmp_file) // extracts file informations
                ->get('duration');             // returns the duration property

            $file_name = $sceneNo.'.mp3';
            $dest_file = $folder.$file_name;

            $mp3time = $goodsInfo['bgmTime'];

            if ($mp3time > $duration) {
                $difftime = floor($mp3time / $duration);
                exec('ffmpeg -stream_loop '.$difftime.' -i "'.$tmp_file.'" -c copy '.$dest_file);
            } else {
                $ffmpeg = \FFMpeg\FFMpeg::create();
                $video = $ffmpeg->open($tmp_file);
                $format = new \FFMpeg\Format\Audio\Mp3();
                //$video->filters()->clip(\FFMpeg\Coordinate\TimeCode::fromSeconds(0), \FFMpeg\Coordinate\TimeCode::fromSeconds(30));
                $video->save($format, $dest_file);
            }

            $scene->basic_movie = '/storage/create/uploads/'.$createNo.'/'.$file_name;
            $scene->cut_movie = '/storage/create/uploads/'.$createNo.'/'.$file_name;
            $scene->text = $originalName;
            $scene->filesize = $this->request->file('mp3')->getClientSize();
            $scene->filetype = $this->request->file('mp3')->getClientMimeType();

        } else if ($this->request['type']=="del") {

            if (file_exists($folder.$sceneNo.'.mp3')) {
                unlink($folder.$sceneNo.'.mp3');
            }

            $scene->basic_movie = '';
            $scene->cut_movie = '';
            $scene->filesize = '';
            $scene->filetype = '';
            $scene->text = '';
        }

        $scene->createNm = $createNo;
        $scene->scene = $sceneNo;
        $scene->complete = 1;
        $scene->save();

        $result['state'] = 'ok';

        if (!empty($createNo)) {
            if ($createNo!="") {
                $userFilesAll = DB::table('uploads')->where('createNo', $createNo);
                $userFilesAll->update(['updated_at'=>date('Y-m-d H:i:s')]);
            }
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;

    }

    public function postSceneDel()
    {
        $createNo = $this->request['createNo'];
        $scene = $this->request['scene'];
        $sceneSub = $this->request['sceneSub'];

        $userScene = CreateScene::where('createNo', $createNo)->where('scene', $scene)->where('sceneSub', $sceneSub)->first();
        $dir = $this->path.$createNo.'/';

        //파일 및 DB 삭제처리
        if (file_exists($dir.$scene.'_'.$sceneSub.'.jpg')) {
            unlink($dir.$scene.'_'.$sceneSub.'.jpg');
        }

        if (file_exists($dir.$scene.'_'.$sceneSub.'.mp4')) {
            unlink($dir.$scene.'_'.$sceneSub.'.mp4');
        }

        if (file_exists($dir.$scene.'_'.$sceneSub.'_thumb.jpg')) {
            unlink($dir.$scene.'_'.$sceneSub.'_thumb.jpg');
        }

        $userScene->delete();

        $result['state'] = "success";
        $result['msg'] = "삭제되었습니다.";

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function postScene()
    {
        $goodsType = $this->request['goodsType'];
        $goodsCd = $this->request['goodsCd'];
        $createNo = $this->request['createNo'];
        $scene = $this->request['scene'];
        $type = $this->request['type'];
        $files = $this->request->file('file');

        if ($type=="text") {
            $result['text'] = $this->Uploads($goodsType, $goodsCd, $createNo, $type, $scene);
            $result['state'] = "success";
        } else {
            $result['state'] = "Not Files";
            foreach ($files as $key=>$file) {
                $result['files'][] = $this->Uploads($goodsType, $goodsCd, $createNo, $type, $scene, $file);
                $result['state'] = "success";
            }
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    private function Uploads($goodsType, $goodsCd, $createNo, $type, $scene, $file='')
    {
        $folder = $this->folder.$createNo.'/';
        if (!file_exists($folder)) {
            mkdir($folder, 0755, true);
        }

        $goodsInfo = UpladsAdmin::where('goodsType', $goodsType)->where('goodsCd', $goodsCd)->where('useYn', 1)->where('type', 1)->first();
        $sceneAdmin = json_decode(stripslashes($goodsInfo['sceneInfo']), true)[$scene];

        if ($type!='text') {
            $max = $sceneAdmin['file_count'];
            $cnt = CreateScene::where('createNo', $createNo)->where('scene', $scene)->count();
            $sceneData = CreateScene::where('createNo', $createNo)->where('scene', $scene)->get();
            $sceneSub = 1;

            if ($cnt > 0) {

                foreach ($sceneData as $key=>$val) {
                    $userSceneArr[] = $val['sceneSub'];
                }

                for ($i=1;$i<=$max;$i++) {
                    $adminSceneArr[] = $i;
                }

                $adminSceneArr = collect($adminSceneArr);
                $diff = $adminSceneArr->diff($userSceneArr);
                $sceneSub = $diff->first();
            }

            $result['sceneSub'] = $sceneSub;

            if ($max > $cnt) {

                $tmp_file = $file->getPathname();

                if ($type=="photo") {

                    $file_name = $scene.'_'.$sceneSub.'.jpg';
                    $file_name2 = $scene.'_'.$sceneSub.'_thumb.jpg';
                    $dest_file = $folder.$file_name;
                    $dest_file2 = $folder.$file_name2;


                    $img = new \imagick($tmp_file);
                    $img->setCompressionQuality(100);
                    $img->setImageFormat('jpeg');
                    $img->writeImage($dest_file);

                    $img->resizeImage( 1920, 1080, \Imagick::COMPOSITE_ATOP , 1, TRUE );
                    $img->writeImage($dest_file2);

                    $img->clear();
                    $img->destroy();

                    $dest_file = $this->storege_folder.$createNo.'/'.$file_name;
                    $dest_file2 = $this->storege_folder.$createNo.'/'.$file_name2;

                } else if ($type=="video") {

                    $file_name = $scene.'_'.$sceneSub.'.mp4';
                    $file_name2 = $scene.'_'.$sceneSub.'_thumb.jpg';
                    $dest_file = $folder.$file_name;
                    $dest_file2 = $folder.$file_name2;

                    $ffprobe = \FFMpeg\FFProbe::create();

                    $bitRate = $ffprobe
                            ->streams($tmp_file)
                            ->videos()
                            ->first()
                            ->get('bit_rate') / 1024;

                    if($bitRate>20000){
                        $bitRate = $bitRate / 2;
                    } else if($bitRate>4000){
                        $bitRate = 4000;
                    }else{
                        $bitRate = 2000;
                    }

                    $ffmpeg = \FFMpeg\FFMpeg::create();
                    $video = $ffmpeg->open($tmp_file);

                    $format = new \FFMpeg\Format\Video\X264();
                    $format
                        ->setKiloBitrate($bitRate)
                        ->setAudioCodec("libmp3lame");

                    $video->save($format, $dest_file);
                    $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(0))->save($dest_file2);

                    $dest_file = $this->storege_folder.$createNo.'/'.$file_name;
                    $dest_file2 = $this->storege_folder.$createNo.'/'.$file_name2;

                }

                $fileName = "";
                $filePath = $dest_file;
                $fileThumb = $dest_file2;
                $filesize = $file->getClientSize();
                $filetype = $file->getClientMimeType();

                $result['goodsNo'] = $goodsCd;
                $result['fileName'] = $fileName;
                $result['fileThumb'] = $fileThumb;
                $result['subState'] = "success";
                $result['sceneNo'] = $scene;
                $result['msg'] = "저장이 완료되었습니다.";
                $text = "";

            } else {
                $result['subState'] = "error";
                $result['msg'] = "더 이상 저장할 수 없습니다.";
            }

        } else {
            //자막저장

            $fileName = "";
            $filePath = "";
            $fileThumb = "";
            $filesize = "";
            $filetype = "";
            $sceneSub = "";
            $text = json_encode($this->request['text'], JSON_UNESCAPED_UNICODE);

            $result['subState'] = "success";
            $result['sceneNo'] = $scene;
            $result['msg'] = "저장이 완료되었습니다.";
        }

        /*********** DB 저장부분 및 리턴 ********/

        if ($result['subState']=="success") {
            $userScene = CreateScene::where('createNo', $createNo)->where('scene', $scene)->where('sceneSub', $sceneSub)->first();
            if (empty($userScene)) {
                $userScene = new CreateScene;
            }

            $userScene->createNo = $createNo;
            $userScene->scene = $scene;
            $userScene->sceneSub = $sceneSub;
            $userScene->type = $type;
            $userScene->fileName = $fileName;
            $userScene->filePath = $filePath;
            $userScene->fileThumb = $fileThumb;
            $userScene->filesize = $filesize;
            $userScene->filetype = $filetype;
            if (!empty($text)) {
                $userScene->text = $text;
            }
            $userScene->save();
        }

        return $result;

    }

    public function getUploads()
    {
        $goodsType = $this->request->segment(4);
        $goodsCd = $this->request->segment(5);
        $odNo = $this->request->segment(6);
        $odSno = $this->request->segment(7);

        //상품정보 불러오기
        $goodsInfo = UpladsAdmin::where('goodsType', $goodsType)->where('useYn', 1)->where('goodsCd', $goodsCd)->where('type', 1)->first();

        if (empty($goodsInfo)) {
            abort(404);
        }

        $goodsInfo->playTime = (int)$goodsInfo->playTime;     

        $basicInfo = json_decode($goodsInfo->basicInfo, true);
        $sceneInfo = json_decode(stripslashes($goodsInfo->sceneInfo), true);
        $sceneInfo = $this->record_sort($sceneInfo, 'seq');

        //업로드 불러오기
        $uploads = Create::where('odNo', $odNo)->where('odSno', $odSno)->first();
        if (empty($uploads)) {
            $uploads = new Create;
            $uploads->odNo = $odNo;
            $uploads->odSno = $odSno;
            $uploads->createNo = $odNo.$odSno;
            $uploads->goodsType = $goodsType;
            $uploads->goodsCd = $goodsCd;
            $uploads->state = 0;
            $uploads->save();
        }

        if (!empty($uploads->basicInfo)) {
            $uploadsBasic = json_decode($uploads->basicInfo, true);
        } else {
            $uploadsBasic = [];
        }

        //업로드 내역 있으면 불러오기
        $uploadsScene = CreateScene::where('createNo', $uploads->createNo)->get();
        $userUploads = array();

        foreach ($uploadsScene as $scene) {
            if($scene->type!="text"){
                $userUploads[$scene->type][$scene->scene][$scene->sceneSub]['filePath'] = $scene->filePath;
                $userUploads[$scene->type][$scene->scene][$scene->sceneSub]['fileThumb'] = $scene->fileThumb;
                $userUploads[$scene->type][$scene->scene][$scene->sceneSub]['scene'] = $scene->scene;
                $userUploads[$scene->type][$scene->scene][$scene->sceneSub]['sceneSub'] = $scene->sceneSub;
            } else if($scene->type=="mp3") {
                $userUploads[$scene->type]['mp3'] = $scene->filePath;
            } else {
                $userUploads[$scene->type][$scene->scene]['text'] = json_decode($scene->text, true);
            }
        }

        $_token = csrf_token();

        $sceneInfo = json_decode(stripslashes($goodsInfo->sceneInfo), true);
        $sceneInfo = $this->record_sort($sceneInfo, 'seq');

        $sceneGroupArr = collect($sceneInfo)->groupBy('type');
        $sceneGroup = [];
        $videoCnt = 0;
        $photoCnt = 0;
        $sceneText = [];
        foreach ($sceneGroupArr as $key=>$value) {
            foreach ($value as $k=>$val) {

                if ($val['type']!='none') {
                    $sceneGroup[$val['type']][$val['seq']][] = $val;
                    if ($val['type']=='photo') {
                        $photoCnt++;
                    } else if ($val['type']=='video') {
                        $videoCnt++;
                    }
                }
                if (!empty($val['text_layer_name'])) {
                    foreach ($val['text_layer_name'] as $textKey=>$text) {
                        $sceneText[] = [
                            "scene"=>$val['seq'],
                            "text_max_length"=>$val['text_max_length'][$textKey],
                            "text_type"=>$val['text_type'][$textKey],
                            "text_sample"=>$val['text_sample'][$textKey]
                        ];
                    }
                }

            }
        }

        $data = [
            'goodsInfo'=>$goodsInfo,
            'createNo'=>$odNo,
            'basicInfo'=>$basicInfo,
            'sceneInfo'=>$sceneInfo,

            'sceneGroup' =>$sceneGroup,
            'photoCnt' => $photoCnt,
            'videoCnt' => $videoCnt,

            'uploads'=>$uploads,
            'uploadsBasic'=>$uploadsBasic,
            'userUploads'=>$userUploads,

            '_token'=>$_token,
            'domain_info'=>$this->domain_info_img
        ];

        return $this->view('uploads', $data);
    }

    public function record_sort($records, $field, $reverse=false)
    {
        $hash = array();

        foreach($records as $record)
        {
            $hash[$record[$field]] = $record;
        }

        ($reverse)? krsort($hash) : ksort($hash);

        $records = array();

        $i=1;
        foreach($hash as $record)
        {
            $records [$i]= $record;
            $i++;
        }

        return $records;
    }
}
