





window.addEventListener("load", function(event) {
    // console.log("All resources finished loading!");

    const odsno = {
        'odsno' : document.querySelector('#orderNo').value,
        'type' : 'check_card'
    }

    const warning_container = document.querySelector('.warning_container');
    const App = document.querySelector('#app');

    fetch('https://app.aileenstory.com/api/mobilecard.php', {
        method: 'POST',
        body: JSON.stringify(odsno)
    }).then(res => {
        res.text().then(function (text) {
            if (text === 'Y') {
                warning_container.style.display = 'none';
                App.style.display = 'block'
                App.style.opacity = '1'
            } else {
                warning_container.style.display = 'flex';
                App.style.display = 'none'
                App.style.opacity = '0'
            }
        });

    }).catch(function(error) {
        console.log(error);
    });
});







// 날짜 관련 스크립트

const date = {
    datepicker: document.querySelector('#datepicker').value,
    hour: document.querySelector('#hour').value,
    minute: document.querySelector('#minute').value,
    apm: document.querySelector('#apm').value
}

const dateTime = date.datepicker;
const newDate = new Date(dateTime.replace(/-/g, "/"))

let Year
let Month
let DD
let Day
let Minutes
let APM;
let Hour
let Minute


Year = newDate.getFullYear();

if (newDate.getMonth()+1 < 10) {
    Month = `0${newDate.getMonth()+1}`
} else {
    Month = `${newDate.getMonth()+1}`
}

if (newDate.getDate() < 10) {
    DD = `0${newDate.getDate()}`
} else {
    DD = `${newDate.getDate()}`
}


if (newDate.getDay() === 0) {
    Day = '일요일'
} else if (newDate.getDay() === 1) {
    Day = '월요일'
} else if (newDate.getDay() === 2) {
    Day = '화요일'
} else if (newDate.getDay() === 3) {
    Day = '수요일'
} else if (newDate.getDay() === 4) {
    Day = '목요일'
} else if (newDate.getDay() === 5) {
    Day = '금요일'
} else if (newDate.getDay() === 6) {
    Day = '토요일'
}

if (newDate.getMinutes() < 10) {
    Minutes = `0${newDate.getMinutes()}`
} else {
    Minutes = `${newDate.getMinutes()}`
}

if (date.apm === 'AM') {
    APM = '오전'
} else {
    APM = '오후'
}

if (Number(date.hour) < 10) {
    Hour = `0${date.hour}`
} else {
    Hour = `${date.hour}`
}

if (Number(date.minute) < 10) {
    Minute = `0${date.minute}`
} else {
    Minute = `${date.minute}`
}

const coverDate = {
    day: document.querySelector('.cover_location_box .day'),
    building: document.querySelector('.cover_location_box .building'),
    createDate() {
        this.day.textContent = `${Year}년 ${Month}월 ${DD}일 ${Day} ${APM} ${Hour}시 ${Minute}분 `;
        this.building.textContent = document.querySelector('#lot_detail').value;
    }
}
coverDate.createDate()







// 연락처 관련 스크립트

const basicState = {
    basicToggle: document.querySelector('#basic_toggle').value,
    basicPlus: document.querySelectorAll('.basic_plus'),
    init() {
        if (this.basicToggle) {
            this.basicPlus.forEach((e) => {
                e.style.display = 'flex';
            })
        } else {
            this.basicPlus.forEach((e) => {
                e.style.display = 'none';
            })
        }
    }
}

basicState.init();











// 지도 관련 스트립트


let lat = document.getElementById('lat').value;
let lng = document.getElementById('lng').value;
let road = document.getElementById('road');
const mapType = document.getElementById('map_type').value;

function kakao_map_init() {
    lat = document.getElementById('lat').value;
    lng = document.getElementById('lng').value;
    const container = document.getElementById('daum_map_box');
    const options = {
        center: new kakao.maps.LatLng(lat, lng),
        level: 3
    };
    const daum_map_box = new kakao.maps.Map(container, options);
    // 마커가 표시될 위치입니다
    const markerPosition = new kakao.maps.LatLng(lat, lng);
    // 마커를 생성합니다
    const marker = new kakao.maps.Marker({
        position: markerPosition
    });
    // 마커가 지도 위에 표시되도록 설정합니다
    marker.setMap(daum_map_box);
    // daum_map_box.setZoomable();
    daum_map_box.setDraggable();

    //주소-좌표 변환 객체를 생성
    const geocoder = new daum.maps.services.Geocoder();
}

kakao_map_init();

let naverMap = null;

function initMap() {
    naverMap = new naver.maps.Map('naver_map', {
        center: new naver.maps.LatLng(lat, lng),
        zoom: 17,
        draggable: false,
        pinchZoom: false,
        scrollWheel: false,
        keyboardShortcuts: false,
        disableDoubleTapZoom: true,
        disableDoubleClickZoom: true,
        disableTwoFingerTapZoom: true
    });

    new naver.maps.Marker({
        position: new naver.maps.LatLng(lat, lng),
        map: naverMap
    });

}

// 웹 플랫폼 도메인 등 초기화한 앱의 설정이 그대로 적용됩니다.
// 초기화한 앱에 현재 도메인이 등록되지 않은 경우 에러가 발생합니다.
const kakaoBtn = document.querySelector('.icon_kakao');
kakaoBtn.addEventListener('click', function(e){
    e.preventDefault();
    navi()
})

Kakao.init('c089c8172def97eb00c07217cae17495');
function navi() {
    Kakao.Navi.start({
        name: road.value,
        x: Number(lng),
        y: Number(lat),
        coordType: 'wgs84',
    })
}

const tmapBtn = document.querySelector('.tmap_btn');

tmapBtn.addEventListener('click', (e) => {
    e.preventDefault();
    location.href = `https://apis.openapi.sk.com/tmap/app/routes?appKey=l7xx502bfd4bde304358b12ba177f51f8675&name=${road.value}&lon=${lng}&lat=${lat}`
});

const daumMapBox = document.getElementById('daum_map_box');
const naverMapBox = document.getElementById('naver_map');

if (mapType === 'daum') {
    daumMapBox.style.display = 'block';
    naverMapBox.style.display = 'none';
} else {
    daumMapBox.style.display = 'none';
    naverMapBox.style.display = 'block';
}

const locationToggle = document.querySelector('#location_toggle').value;
const locationTextareaBox = document.querySelector('.location_text_box .textarea_box');

if (locationToggle) {
    locationTextareaBox.style.display = 'block';
} else {
    locationTextareaBox.style.display = 'none';
}




// 옵션 관련 스크립트

const option = {
    naviOption: document.querySelector('#navi_option'),
    thankOption: document.querySelector('#thank_option'),
    navIcon : document.querySelectorAll('#location .nav_icon'),
    chatBox: document.querySelector('#chat'),
    naviOnOff() {
        if(this.naviOption.value) {
            Array.from(this.navIcon).forEach(v => {
                v.style.display = 'block'
            })
            // this.locationNaviBox.style.display = 'flex'
        } else {
            // this.locationNaviBox.style.display = 'none'
            Array.from(this.navIcon).forEach(v => {
                v.style.display = 'none'
            })
        }
    },
    thankOnOff() {
        if(this.thankOption.value) {
            this.chatBox.style.display = 'block'
        } else {
            this.chatBox.style.display = 'none'
        }
    }
}
option.naviOnOff();
// option.thankOnOff();


