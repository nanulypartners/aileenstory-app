


window.addEventListener("load", function(event) {
    // console.log("All resources finished loading!");

    const odsno = {
        'odsno' : document.querySelector('#orderNo').value,
        'type' : 'check_card'
    }

    const warning_container = document.querySelector('.warning_container');
    const App = document.querySelector('#app');

    fetch('https://app.aileenstory.com/api/mobilecard.php', {
        method: 'POST',
        body: JSON.stringify(odsno)
    }).then(res => {
        res.text().then(function (text) {
            if (text === 'Y') {
                warning_container.style.display = 'none';
                App.style.display = 'block'
                App.style.opacity = '1'
            } else {
                warning_container.style.display = 'flex';
                App.style.display = 'none'
                App.style.opacity = '0'
            }
        });

    }).catch(function(error) {
        console.log(error);
    });
});





// 날짜 관련 스크립트

const date = {
    datepicker: document.querySelector('#datepicker').value,
    hour: document.querySelector('#hour').value,
    minute: document.querySelector('#minute').value,
    apm: document.querySelector('#apm').value
}

const dateTime = date.datepicker;
const newDate = new Date(dateTime.replace(/-/g, "/"))

let Year
let Month
let DD
let Day
let Minutes
let APM;
let Hour
let Minute


Year = newDate.getFullYear();

if (newDate.getMonth()+1 < 10) {
    Month = `0${newDate.getMonth()+1}`
} else {
    Month = `${newDate.getMonth()+1}`
}

if (newDate.getDate() < 10) {
    DD = `0${newDate.getDate()}`
} else {
    DD = `${newDate.getDate()}`
}


if (newDate.getDay() === 0) {
    Day = 'SUN'
} else if (newDate.getDay() === 1) {
    Day = 'MON'
} else if (newDate.getDay() === 2) {
    Day = 'TUE'
} else if (newDate.getDay() === 3) {
    Day = 'WED'
} else if (newDate.getDay() === 4) {
    Day = 'THU'
} else if (newDate.getDay() === 5) {
    Day = 'FRI'
} else if (newDate.getDay() === 6) {
    Day = 'SAT'
}

if (newDate.getMinutes() < 10) {
    Minutes = `0${newDate.getMinutes()}`
} else {
    Minutes = `${newDate.getMinutes()}`
}

if (date.apm === 'AM') {
    APM = 'AM'
} else {
    APM = 'PM'
}

if (Number(date.hour) < 10) {
    Hour = `0${date.hour}`
} else {
    Hour = `${date.hour}`
}

if (Number(date.minute) < 10) {
    Minute = `0${date.minute}`
} else {
    Minute = `${date.minute}`
}

const coverDate = {
    day: document.querySelector('.cover_location_box .day'),
    building: document.querySelector('.cover_location_box .building'),
    YMD_Y: document.querySelector('.YMD_bod .year'),
    YMD_M: document.querySelector('.YMD_bod .month'),
    YMD_D: document.querySelector('.YMD_bod .day'),
    createDate() {
        this.YMD_Y.textContent = `${Year.toString().substring(2,4)}`;
        this.YMD_M.textContent = `${Month}`;
        this.YMD_D.textContent = `${DD}`;

        this.day.textContent = `${Year}. ${Month}. ${DD} ${Day} ${Hour}:${Minute} ${APM}`;
        this.building.textContent = document.querySelector('#lot_detail').value;
    },


}
coverDate.createDate()




// 연락처 관련 스크립트

const basicState = {
    basicToggle: document.querySelector('#basic_toggle').value,
    basicPlus: document.querySelectorAll('.basic_plus'),
    init() {
        if (this.basicToggle) {
            this.basicPlus.forEach((e) => {
                const name = e.querySelector('.name').textContent;
                if (name.length > 2) {
                    e.style.display = 'flex';
                } else {
                    e.style.display = 'none';
                }
            })
        } else {
            this.basicPlus.forEach((e) => {
                e.style.display = 'none';
            })
        }
    }
}

basicState.init();

class locationModal{
    constructor(btn) {
        this.btn = btn;
        this.modal = document.querySelector('.hosting_modal');
        this.closeBtn = document.querySelector('.hosting_modal .close_btn');
        this.headerGroom = document.querySelector('.hosting_modal .icon_box .groom');
        this.headerBride = document.querySelector('.hosting_modal .icon_box .bride');
        this.listGroom = document.querySelector('.hosting_modal .list_box .groom');
        this.listBride = document.querySelector('.hosting_modal .list_box .bride');
    }

    on() {
        this.btn.addEventListener('click', (e) => {
            this.modal.style.display = 'block';
            const target = e.currentTarget;
            if (target.classList.contains('groom_btn')) {
                this.headerGroom.style.display = 'flex'
                this.headerBride.style.display = 'none'
                this.listGroom.style.display = 'block'
                this.listBride.style.display = 'none'
            } else {
                this.headerGroom.style.display = 'none'
                this.headerBride.style.display = 'flex'
                this.listGroom.style.display = 'none'
                this.listBride.style.display = 'block'
            }
        });
    }

    off() {
        this.closeBtn.addEventListener('click', () => this.modal.style.display = 'none');
    }
}

const locationGroom = new locationModal(
    document.querySelector('.hosting_tell_btn_box .groom_btn'),
)
locationGroom.on();
locationGroom.off();

const locationBride = new locationModal(
    document.querySelector('.hosting_tell_btn_box .bride_btn'),
)
locationBride.on();
locationBride.off();







// 갤러리 스크립트


var initPhotoSwipeFromDOM = function(gallerySelector) {
    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size = [],
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element
            let image = linkEl.children[0]

            size[0] = image.naturalWidth;
            size[1] = image.naturalHeight;
            // size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML;
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            }

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) {
                continue;
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }
        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
            params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if(pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('.my-gallery');

const imagesBoxFigures = document.querySelectorAll('.images_box figure');
const galleryBox = document.querySelector('#gallery');

if (imagesBoxFigures.length > 0) {
    galleryBox.style.display = 'block';
} else {
    galleryBox.style.display = 'none';
}





// 비디오 관련 스크립트

const video = {
    check: document.querySelector('#video_check').value,
    videoBox : document.querySelector('#video'),

    init() {
        if (this.check) {
            this.videoBox.style.display = 'block'
        } else {
            this.videoBox.style.display = 'none'
        }
    }
}

video.init();







// 지도 관련 스트립트


let lat = document.getElementById('lat').value;
let lng = document.getElementById('lng').value;
let road = document.getElementById('road');
const mapType = document.getElementById('map_type').value;

function kakao_map_init() {
    lat = document.getElementById('lat').value;
    lng = document.getElementById('lng').value;
    const container = document.getElementById('daum_map_box');
    const options = {
        center: new kakao.maps.LatLng(lat, lng),
        level: 3
    };
    const daum_map_box = new kakao.maps.Map(container, options);
    // 마커가 표시될 위치입니다
    const markerPosition = new kakao.maps.LatLng(lat, lng);
    // 마커를 생성합니다
    const marker = new kakao.maps.Marker({
        position: markerPosition
    });
    // 마커가 지도 위에 표시되도록 설정합니다
    marker.setMap(daum_map_box);
    // daum_map_box.setZoomable();
    daum_map_box.setDraggable();

    //주소-좌표 변환 객체를 생성
    const geocoder = new daum.maps.services.Geocoder();
}

kakao_map_init();

let naverMap = null;

function initMap() {
    naverMap = new naver.maps.Map('naver_map', {
        center: new naver.maps.LatLng(lat, lng),
        zoom: 17,
        draggable: false,
        pinchZoom: false,
        scrollWheel: false,
        keyboardShortcuts: false,
        disableDoubleTapZoom: true,
        disableDoubleClickZoom: true,
        disableTwoFingerTapZoom: true
    });

    new naver.maps.Marker({
        position: new naver.maps.LatLng(lat, lng),
        map: naverMap
    });

}

// 웹 플랫폼 도메인 등 초기화한 앱의 설정이 그대로 적용됩니다.
// 초기화한 앱에 현재 도메인이 등록되지 않은 경우 에러가 발생합니다.
const kakaoBtn = document.querySelector('.icon_kakao');
kakaoBtn.addEventListener('click', function(e){
    e.preventDefault();
    navi()
})

Kakao.init('c089c8172def97eb00c07217cae17495');
function navi() {
    Kakao.Navi.start({
        name: road.value,
        x: Number(lng),
        y: Number(lat),
        coordType: 'wgs84',
    })
}


const tmapBtn = document.querySelector('.tmap_btn');

tmapBtn.addEventListener('click', (e) => {
    e.preventDefault();
    location.href = `https://apis.openapi.sk.com/tmap/app/routes?appKey=l7xx502bfd4bde304358b12ba177f51f8675&name=${road.value}&lon=${lng}&lat=${lat}`
});

const daumMapBox = document.getElementById('daum_map_box');
const naverMapBox = document.getElementById('naver_map');

if (mapType === 'daum') {
    daumMapBox.style.display = 'block';
    naverMapBox.style.display = 'none';
} else {
    daumMapBox.style.display = 'none';
    naverMapBox.style.display = 'block';
}

const locationToggle = document.querySelector('#location_toggle').value;
const locationTextareaBox = document.querySelector('.location_text_box .textarea_box');

if (locationToggle) {
    locationTextareaBox.style.display = 'block';
} else {
    locationTextareaBox.style.display = 'none';
}




// 옵션 관련 스크립트

const option = {
    naviOption: document.querySelector('#navi_option'),
    thankOption: document.querySelector('#thank_option'),
    iconTmap : document.querySelectorAll('#location .icon_tmap'),
    chatBox: document.querySelector('#chat'),
    naviOnOff() {
        if(this.naviOption) {
            Array.from(this.iconTmap).forEach(v => {
                v.style.display = 'block'
            })
            // this.locationNaviBox.style.display = 'flex'
        } else {
            // this.locationNaviBox.style.display = 'none'
            Array.from(this.iconTmap).forEach(v => {
                v.style.display = 'none'
            })
        }
    },
    thankOnOff() {
        if(this.naviOption) {
            this.chatBox.style.display = 'block'
        } else {
            this.chatBox.style.display = 'none'
        }
    }
}
option.naviOnOff();
option.thankOnOff();

// 채팅 관련 스크립트

const chat = {
    createModalOpenBtn: document.querySelector('.chat_modal_open_btn button'),
    createModal: document.querySelector('.chat_create_modal'),
    modifyModal: document.querySelector('.chat_modify_modal'),
    deleteModal: document.querySelector('.chat_delete_modal'),
    createModalCloseBtn: document.querySelector('.chat_create_modal .close_btn'),
    modifyModalCloseBtn: document.querySelector('.chat_modify_modal .close_btn'),
    deleteModalCloseBtn: document.querySelector('.chat_delete_modal .close_btn'),
    url: 'https://app.aileenstory.com/api/mobilecard.php',
    modifyChatNum: document.querySelector('#modifiy_chat_num'),
    chatModifyBtn: document.querySelector('#chat_modify_btn'),
    deleteChatNum: document.querySelector('#delete_chat_num'),
    chatDeleteBtn: document.querySelector('#chat_delete_btn'),
    createModalOpen() {
        this.createModalOpenBtn.addEventListener('click', () => {
            this.createModal.style.display = 'block';
        })
    },
    createModalClose() {
        this.createModalCloseBtn.addEventListener('click', () => {
            this.createModal.style.display = 'none';
        })
    },
    modifyModalClose() {
        this.modifyModalCloseBtn.addEventListener('click', () => {
            this.modifyModal.style.display = 'none';
        })
    },
    deleteModalClose() {
        this.deleteModalCloseBtn.addEventListener('click', () => {
            this.deleteModal.style.display = 'none';
        })
    },
    loadChat() {
        const formData = new FormData();
        formData.append('card[card_title]', document.querySelector('#card_title').value);
        formData.append('type', 'get_chat');
        fetch(this.url, {
            method: 'POST',
            headers: {},
            body: formData
        }).then(res => {
            res.json().then(item => {
                // console.log(item)
                const chatItemBoxTbody = document.querySelector('.chat_item_box tbody');

                for (let prop in item) {
                    guestTag(item[prop], callback => {
                        // console.log(callback)
                        chatItemBoxTbody.appendChild(callback)
                    })
                }
                $('#example').DataTable({

                    pageLength: 3,
                    // 표시 건수기능 숨기기
                    lengthChange: false,

                    searching: false,
                    // 정렬 기능 숨기기
                    // 정보 표시 숨기기
                    info: false,

                    // 검색 기능 숨기기
                    searching: false,

                    order : [[0, 'desc']]
                });
                // item.forEach((v) => console.log(v))




            });

        }).catch(function(error) {
            console.log(error);
        });
    }
}

chat.createModalOpen();
chat.createModalClose();
chat.modifyModalClose();
chat.deleteModalClose();
chat.loadChat();

function goData() {
    const chat_form_name = document.querySelector('#chat_form_name').value;
    const chat_form_password = document.querySelector('#chat_form_password').value;
    const chat_form_text = document.querySelector('#chat_form_text').value;
    let chat_time = document.querySelector('#chat_time');

    const today = new Date();
    chat_time.value = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();

    if (chat_form_name === '') {
        alert('성함을 입력해주세요.');
        return false;
    } else if (chat_form_password === '') {
        alert('비밀번호를 입력해주세요.');
        return false;
    } else if (chat_form_text === '') {
        alert('짧은 메세지를 입력해주세요.');
        return false;
    }

    // const url = "https://video.studioium.com/api/mobilecard.php";
    const $url = 'https://app.aileenstory.com/api/mobilecard.php';
    const chat_data = document.querySelector('#chat_data');
    const formData = new FormData(chat_data);


    fetch($url, {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        res.json().then(item => {
            const table = $('#example').DataTable();
            const Obj = Object.keys(item);
            // console.log(item)
            // console.log(item[Obj[Obj.length -1]])
            // console.log(Obj)
            // console.log(Obj[Obj.length -1])
            guestTag(item[Obj[Obj.length -1]], callback => {
                // console.log(callback)
                table.row.add(callback).draw();

            })
            document.querySelector('#chat_form_name').value = ''
            document.querySelector('#chat_form_password').value = ''
            document.querySelector('#chat_form_text').value = ''
            document.querySelector('#chat_time').value = ''
            chat.createModal.style.display = 'none'


        });


    }).catch(function(error) {
        console.log(error);
    });
}

function guestTag(item, callback) {
    // console.log('item =>',item)
    const guest_tr = document.createElement('tr');
    const guest_td = document.createElement('td');
    const guest_div = document.createElement('div');
    const guest_name = document.createElement('span');
    const guest_date = document.createElement('span');
    const guest_modify_btn = document.createElement('button');
    const guest_delete_btn = document.createElement('button');
    const guest_text = document.createElement('p');
    const guest_num = document.createElement('input');

    guest_name.textContent = item.name;
    guest_date.textContent = item.time;
    guest_modify_btn.textContent = '수정';
    guest_delete_btn.textContent = '삭제';
    guest_text.textContent = item.comment;
    guest_num.value = item.num;
    guest_num.type = 'hidden';

    guest_tr.appendChild(guest_td);
    guest_td.appendChild(guest_div);
    guest_div.appendChild(guest_name)
    guest_div.appendChild(guest_date)
    guest_div.appendChild(guest_modify_btn)
    guest_div.appendChild(guest_delete_btn)
    guest_div.appendChild(guest_text)
    guest_div.appendChild(guest_num)

    guest_tr.id = `chat_${item.num}`;
    guest_div.classList.add('guest_tag')
    guest_name.classList.add('name')
    guest_date.classList.add('date')
    guest_modify_btn.classList.add('modify_btn')
    guest_delete_btn.classList.add('delete_btn')
    guest_text.classList.add('guest_text')
    guest_num.classList.add('guest_num')

    guest_modify_btn.addEventListener('click', () => {
        chat.modifyChatNum.value = guest_num.value;
        modifyChatModalOpen()
    })

    guest_delete_btn.addEventListener('click', () => {
        chat.deleteChatNum.value = guest_num.value;
        deleteChatModalOpen()
    })


    callback(guest_tr)
}

function modifyChatModalOpen() {
    const modifyChatName = document.querySelector('#chat_modifiy_name');
    const modifyChatContent = document.querySelector('#chat_modifiy_text');

    // const selectChat = document.querySelectorAll('#example tbody tr')[chat.modifyChatNum.value - 1]
    const selectChat = document.querySelector(`#chat_${chat.modifyChatNum.value}`)
    console.log(selectChat)
    modifyChatName.value = selectChat.querySelector('.name').textContent;
    modifyChatContent.value = selectChat.querySelector('.guest_text').textContent;
    chat.modifyModal.style.display = 'block';
}


chat.chatModifyBtn.addEventListener('click', () => {
    const formData = new FormData();
    const modifyChatName = document.querySelector('#chat_modifiy_name');
    const modifyChatPassword = document.querySelector('#chat_modifiy_password');
    const modifyChatContent = document.querySelector('#chat_modifiy_text');
    const selectChat = document.querySelector(`#chat_${chat.modifyChatNum.value}`)

    formData.append('card[card_title]', document.querySelector('#card_title').value);
    formData.append('type', 'edit_chat');
    formData.append('name', modifyChatName.value);
    formData.append('comment', modifyChatContent.value);
    formData.append('password', modifyChatPassword.value);
    formData.append('num', chat.modifyChatNum.value);

    fetch(chat.url, {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        res.json().then(item => {
            // console.log(item)
            const target = document.querySelector(`#chat_${item.num}`)
            target.querySelector('.name').textContent = item.name;
            target.querySelector('.guest_text').textContent = item.comment;
            chat.modifyModal.style.display = 'none';
        }).catch(e => {
            alert('비밀번호가 다릅니다.')
            // chat.modifyModal.style.display = 'none';
        })
    }).catch(function(error) {
        console.log(error);
    });

})


function deleteChatModalOpen() {



    chat.deleteModal.style.display = 'block';
}


chat.chatDeleteBtn.addEventListener('click', () => {
    const formData = new FormData();
    const delete_chat_password = document.querySelector('#delete_chat_password').value;
    formData.append('card[card_title]', document.querySelector('#card_title').value);
    formData.append('type', 'del_chat');
    formData.append('password', delete_chat_password);
    formData.append('num', chat.deleteChatNum.value);

    fetch(chat.url, {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        res.json().then(item => {
            // console.log(item)
            const target = document.querySelector(`#chat_${item.num}`)
            target.remove();
            const table = $('#example').DataTable();
            table.row(target).remove().draw();
            chat.deleteModal.style.display = 'none';
        }).catch((e) => {
            alert('비밀번호가 틀립니다.')
            console.log(e)
        })
    }).catch(function(error) {
        console.log(error);
    });
})




//
// chat.createModalOpenBtn.addEventListener('click', () => {
//    chat
// });















