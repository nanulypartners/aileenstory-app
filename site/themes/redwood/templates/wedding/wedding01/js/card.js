


class locationModal{
    constructor(btn) {
        this.btn = btn;
        this.modal = document.querySelector('.hosting_modal');
        this.closeBtn = document.querySelector('.hosting_modal .close_btn');
        this.headerGroom = document.querySelector('.hosting_modal .icon_box .groom');
        this.headerBride = document.querySelector('.hosting_modal .icon_box .bride');
        this.listGroom = document.querySelector('.hosting_modal .list_box .groom');
        this.listBride = document.querySelector('.hosting_modal .list_box .bride');
    }

    on() {
        this.btn.addEventListener('click', (e) => {
            console.log(e.currentTarget)
            this.modal.style.display = 'block';
            const target = e.currentTarget;
            if (target.classList.contains('groom_btn')) {
                console.log('aaa')
                this.headerGroom.style.display = 'flex'
                this.headerBride.style.display = 'none'
                this.listGroom.style.display = 'block'
                this.listBride.style.display = 'none'
            } else {
                console.log('bbb')
                this.headerGroom.style.display = 'none'
                this.headerBride.style.display = 'flex'
                this.listGroom.style.display = 'none'
                this.listBride.style.display = 'block'
            }
        });
    }

    off() {
        this.closeBtn.addEventListener('click', () => this.modal.style.display = 'none');
    }
}

const locationGroom = new locationModal(
    document.querySelector('.hosting_tell_btn_box .groom_btn'),
)
locationGroom.on();
locationGroom.off();

const locationBride = new locationModal(
    document.querySelector('.hosting_tell_btn_box .bride_btn'),
)
locationBride.on();
locationBride.off();































var initPhotoSwipeFromDOM = function(gallerySelector) {
    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML;
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            }

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) {
                continue;
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }
        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
            params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if(pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('.my-gallery');









// 지도 관련 스트립트


let lat = document.getElementById('lat').value;
let lng = document.getElementById('lng').value;
let road = document.getElementById('road');

function kakao_map_init() {
    lat = document.getElementById('lat').value;
    lng = document.getElementById('lng').value;
    const container = document.getElementById('daum_map_box');
    const options = {
        center: new kakao.maps.LatLng(lat, lng),
        level: 3
    };
    const daum_map_box = new kakao.maps.Map(container, options);
    // 마커가 표시될 위치입니다
    const markerPosition = new kakao.maps.LatLng(lat, lng);
    // 마커를 생성합니다
    const marker = new kakao.maps.Marker({
        position: markerPosition
    });
    // 마커가 지도 위에 표시되도록 설정합니다
    marker.setMap(daum_map_box);
    // daum_map_box.setZoomable();
    daum_map_box.setDraggable();

    //주소-좌표 변환 객체를 생성
    const geocoder = new daum.maps.services.Geocoder();
}

kakao_map_init();

let naverMap = null;

function initMap() {
    naverMap = new naver.maps.Map('naver_map', {
        center: new naver.maps.LatLng(37.5635793508101, 126.975389363157),
        zoom: 17,
        draggable: false,
        pinchZoom: false,
        scrollWheel: false,
        keyboardShortcuts: false,
        disableDoubleTapZoom: true,
        disableDoubleClickZoom: true,
        disableTwoFingerTapZoom: true
    });

    new naver.maps.Marker({
        position: new naver.maps.LatLng(37.5635793508101, 126.975389363157),
        map: naverMap
    });

}

// 웹 플랫폼 도메인 등 초기화한 앱의 설정이 그대로 적용됩니다.
// 초기화한 앱에 현재 도메인이 등록되지 않은 경우 에러가 발생합니다.
Kakao.init('c089c8172def97eb00c07217cae17495')
function navi() {
    Kakao.Navi.start({
        name: '현대백화점 판교점',
        x: 127.11205203011632,
        y: 37.39279717586919,
        coordType: 'wgs84',
    })
}




// 채팅 관련 스크립트

const chat = {
    createModalOpenBtn: document.querySelector('.chat_modal_open_btn button'),
    createModal: document.querySelector('.chat_create_modal'),
    createModalCloseBtn: document.querySelector('.chat_create_modal .close_btn'),
    createModalOpen() {
        this.createModalOpenBtn.addEventListener('click', () => {
            this.createModal.style.display = 'block';
        })
    },
    createModalClose() {
        this.createModalCloseBtn.addEventListener('click', () => {
            this.createModal.style.display = 'none';
        })
    }
}

chat.createModalOpen();
chat.createModalClose();

function goData() {
    const chat_form_name = document.querySelector('#chat_form_name').value;
    const chat_form_password = document.querySelector('#chat_form_password').value;
    const chat_form_text = document.querySelector('#chat_form_text').value;
    let chat_time = document.querySelector('#chat_time');

    const today = new Date();
    chat_time.value = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();

    if (chat_form_name === '') {
        alert('성함을 입력해주세요.');
        return false;
    } else if (chat_form_password === '') {
        alert('비밀번호를 입력해주세요.');
        return false;
    } else if (chat_form_text === '') {
        alert('짧은 메세지를 입력해주세요.');
        return false;
    }

    // const url = "https://video.studioium.com/api/mobilecard.php";
    const url = "/!/cards/card-chat";
    const chat_data = document.querySelector('#chat_data');
    const formData = new FormData(chat_data);

    $.ajax({
        url: url,
        data: formData,
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function (result) {
            const res = JSON.parse(result);
            // console.log(res)
            const table = $('#example').DataTable();
            const chatTtem = document.querySelectorAll('.chat_item')
            const data = guestTag( chatTtem.length + 1 , chat_form_text, chat_form_name, chat_time.value)
            table.row.add(data).draw();
            document.querySelector('#chat_form_name').value = ''
            document.querySelector('#chat_form_password').value = ''
            document.querySelector('#chat_form_text').value = ''
            document.querySelector('#chat_time').value = ''
        }
    });
}

//
// chat.createModalOpenBtn.addEventListener('click', () => {
//    chat
// });















