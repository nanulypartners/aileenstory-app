

// 유효성 검사 코드


var odsno = {
  'odsno' : document.querySelector('#orderGoodsNo').value,
  'type' : 'check_card'
}


var crad_theme_container = document.querySelector('.crad_theme_container');
var warning_container = document.querySelector('.warning_container');


$.ajax({
  method: 'post',
  url: 'https://video.studioium.com/api/mobilecard.php',
  async: false,
  data: JSON.stringify(odsno),
  success: function (data) {
    // console.log(data)
    if (data === 'Y') {
      crad_theme_container.style.visibility = 'visible'
    } else {
      warning_container.style.display = 'flex';
    }

  }
});

// 커버 이미지 관련된 코드 css 에 object-fit 속성이 있지만 익쓰 때문에 작성함


// var cover_img = document.querySelector('.cover_img');
// var cover_imgW = cover_img.width
// var cover_imgH = cover_img.height


// console.log(cover_imgW , cover_imgH)






var date_input = document.querySelector('#date_input').value;
var time_input = document.querySelector('#time_input').value;
var time_data = time_input.split(' ');
var ho_mi = time_data[0].split(':');

if (time_data[1] === 'PM') {
  time_data[1] = '오후'
} else {
  time_data[1] = '오전'
}



var cover_date = document.querySelector('.cover_date')

if(date_input !== '') {
  var custom_date = date_input.split('-')
  var custom_date2 = custom_date[2].split(' ')
  cover_date.textContent = custom_date[0] + "년 " + custom_date[1] + "월 " + custom_date2[0] + "일 " + custom_date2[1] + " " + time_data[1] + " " + ho_mi[0] + "시 " + ho_mi[1] + "분"; 
}



// cover_date.textContent = custom_date[0] + "~" + custom_date[1] + "~" + custom_date[2] + " " + time_data[1] + " " + ho_mi[0] + "시 " + ho_mi[1] + "분 ";



// gallery 갤러리 이미지 슬라이더 코드

var galleryThumbs = new Swiper('.gallery-thumbs', {
  spaceBetween: 10,
  slidesPerView: 4,
  //loop: true,
  freeMode: true,
  //loopedSlides: 5, //looped slides should be the same
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
});
var galleryTop = new Swiper('.gallery-top', {
  // spaceBetween: 10,
  // autoHeight: true, //enable auto height
  loop:true,
  //loopedSlides: 5, //looped slides should be the same
  navigation: {
    nextEl: '.gallery_slider_right',
    prevEl: '.gallery_slider_left',
  },
  thumbs: {
    swiper: galleryThumbs,
  },
});


var gallery = document.querySelector('#gallery');
var swiperWrapper = document.querySelector('#swiper-wrapper');

if (swiperWrapper.lastElementChild === null) {
  gallery.style.display = 'none';
}






var location_map = document.querySelector('#location');
var lat = document.getElementById('lat').value;
var lng = document.getElementById('lng').value;
var road = document.getElementById('road');


function kakao_map_init() {
  lat = document.getElementById('lat').value;
  lng = document.getElementById('lng').value;
  var container = document.getElementById('daum_map_box');
  var options = {
    center: new kakao.maps.LatLng(lat,lng),
    level: 3
  };
  var daum_map_box = new kakao.maps.Map(container, options);
  // 마커가 표시될 위치입니다 
  var markerPosition  = new kakao.maps.LatLng(lat, lng);   
  // 마커를 생성합니다
  var marker = new kakao.maps.Marker({
      position: markerPosition
  });
  // 마커가 지도 위에 표시되도록 설정합니다
  marker.setMap(daum_map_box);
  // daum_map_box.setZoomable();  
  daum_map_box.setDraggable();  

  //주소-좌표 변환 객체를 생성
  var geocoder = new daum.maps.services.Geocoder(); 
}

kakao_map_init();



// 구글맵

var google_map_box = document.getElementById("google_map_box")

function initMap() {
  lat = document.getElementById('lat').value;
  lng = document.getElementById('lng').value;
  var LatLng = new google.maps.LatLng(lat, lng);
  var mapProp = {
    center: LatLng, // 위치
    zoom:17, // 어느정도까지 세세하게 볼 것인지.
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    scrollwheel : false, //마우스 휠로 확대 축소 사용 여부
  };
  map = new google.maps.Map(google_map_box , mapProp);
  var marker = new google.maps.Marker({
    position: LatLng,
    map: map,
  });
 
};


var road_subway = document.querySelector('.road_subway');
var road_subway_text = document.querySelector('.road_subway p');

var road_bus = document.querySelector('.road_bus');
var road_bus_text = document.querySelector('.road_bus  p');

var road_foot = document.querySelector('.road_foot');
var road_foot_text = document.querySelector('.road_foot  p');

var road_parking = document.querySelector('.road_parking');
var road_parking_text = document.querySelector('.road_parking  p');

if (road_subway_text.textContent === '') {
  road_subway.style.display = 'none';
};

if (road_bus_text.textContent === '') {
  road_bus.style.display = 'none';
};

if (road_foot_text.textContent === '') {
  road_foot.style.display = 'none';
};

if (road_parking_text.textContent === '') {
  road_parking.style.display = 'none';
}




var icon_tel = document.querySelector('#location .icon_tel');
var icon_tel_link = document.querySelector('#location .icon_tel a');
var icon_tel_Ext = icon_tel_link.href.replace('tel:' , '');
if (icon_tel_Ext === '') {
  icon_tel.style.display = 'none';
}


// first_birthday 관련 코드

var first_date_input = document.querySelector('#date_input').value;
var first_date_time = document.querySelector('#time_input').value;


if(first_date_input !== '') {
  var firstDate = first_date_input.split('-');
  var firstDay = firstDate[2].split(' ');

  if (firstDay[1] === '월요일') {
    firstDay[1] = 'MON'
  } else if (firstDay[1] === '화요일') {
    firstDay[1] = 'TUE'
  } else if (firstDay[1] === '수요일') {
    firstDay[1] = 'WED'
  } else if (firstDay[1] === '목요일') {
    firstDay[1] = 'THU'
  } else if (firstDay[1] === '금요일') {
    firstDay[1] = 'FRI'
  } else if (firstDay[1] === '토요일') {
    firstDay[1] = 'SAT'
  } else if (firstDay[1] === '일요일') {
    firstDay[1] = 'SUN'
  }

  var first_date =document.querySelector('.first_date');
  first_date.textContent = firstDate[0] + "." + firstDate[1] + "." + firstDay[0] + " " + firstDay[1] + " " + first_date_time;

}





// 채팅 관련 코드

var chat = document.querySelector('#chat');
var chat_select = document.querySelector('#chat_select').value;




function goData() {
  var chat_form_name = document.querySelector('#chat_form_name').value;  
  var chat_form_password = document.querySelector('#chat_form_password').value;  
  var chat_form_text = document.querySelector('#chat_form_text').value; 
  var chat_time = document.querySelector('#chat_time');

  var today = new Date(); //오늘 날짜// 내 컴퓨터 로컬을 기준으로



  chat_time.value = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();

  

  if (chat_form_name === '') {
    alert('성함을 입력해주세요.');
    return false;
  } else if (chat_form_password === '') {
    alert('비밀번호를 입력해주세요.');
    return false;
  } else if (chat_form_text === '') {
    alert('짧은 메세지를 입력해주세요.');
    return false;
  }

  var url = "https://video.studioium.com/api/mobilecard.php";
  var chat_data = document.querySelector('#chat_data');
  var formData = new FormData(chat_data);

  // console.log(formData)

  $.ajax({
    url : url,
    data : formData,
    type : 'POST',
    enctype : 'multipart/form-data',
    processData : false,
    contentType : false,
    success : function(result) {
      var result1 = JSON.parse(result);
      var table = $('#example').DataTable();
      result1 = $.map(result1, function(value, index) {
          return [value];
      });

      console.log(result1)

      var data1 = guestTag(pad(result1[result1.length-1].num, 3) , chat_form_text ,chat_form_name , chat_time.value);
      table.row.add(data1).draw();      
      document.querySelector('#chat_form_name').value = '' 
      document.querySelector('#chat_form_password').value = '' 
      document.querySelector('#chat_form_text').value = '' 
      document.querySelector('#chat_time').value = '' 
    }
  });

}


var url = "https://video.studioium.com/api/mobilecard.php";
var formData = new FormData();
formData.append('card[card_title]', $('#card_title').val());
formData.append('type', 'get_chat');


$.ajax({
  url : url,
  data : formData,
  type : 'POST',
  enctype : 'multipart/form-data',
  processData : false,
  contentType : false,
  success : function(result) {
    // console.log(result);
    // console.log(JSON.parse(result))

    var result2 = JSON.parse(result);
      var chat_item_box_tbody = document.querySelector('.chat_item_box tbody');

    for( var key in result2 ) {
      var data1 = guestTag(pad(result2[key].num, 3) , result2[key].comment , result2[key].name , result2[key].time )
      chat_item_box_tbody.appendChild(data1);
    }

    $('#example').DataTable({

      pageLength: 3,
       // 표시 건수기능 숨기기
      lengthChange: false,

      searching: false,
      // 정렬 기능 숨기기
      // 정보 표시 숨기기
      info: false,

      // 검색 기능 숨기기
      searching: false,

      order : [[0, 'desc']]
    });


   
  }
});

function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}


function guestTag(num , text , name , date ) {
  var guest_tr = document.createElement('tr');
  var guest_td = document.createElement('td');

  var guest_div = document.createElement('div');
  var guest_text = document.createElement('p');
  var guest_name = document.createElement('span');
  var guest_date = document.createElement('span');
  var guest_num = document.createElement('p');

  guest_tr.appendChild(guest_td);
    guest_td.appendChild(guest_div);
      guest_div.appendChild(guest_num);
      guest_div.appendChild(guest_text);
      guest_div.appendChild(guest_name);
      guest_div.appendChild(guest_date);


  guest_tr.id = "chat_" + num;

  guest_div.classList.add('chat_item')

  guest_num.classList.add('guest_num');
  guest_num.textContent = num

  guest_text.classList.add('text');
  guest_text.textContent = text

  guest_name.classList.add('name');
  guest_name.textContent = name;

  guest_date.classList.add('date');
  guest_date.textContent = date;



  var chat_item_box_tbody = document.querySelector('.chat_item_box tbody');
  //chat_item_box_tbody.appendChild(guest_tr);
  return guest_tr;
};


if (chat_select === '') {
  chat.style.display = 'none';
}


var chat_select_modal = document.querySelector('.chat_select_modal');
var modal_box = document.querySelector('.chat .modal_box');


document.querySelector('body').addEventListener('click', function(event) {

  // console.log(event.target)

  if (event.target.classList.contains( 'text' )) {
    document.querySelector('#delete_chat_num').value = event.target.previousSibling.textContent
    document.querySelector('#modifiy_chat_num').value = event.target.previousSibling.textContent
    modal_box.classList.add('active')
    document.querySelector('#chat_modifiy_name').value = event.target.nextSibling.textContent
    document.querySelector('#chat_modifiy_text').value = event.target.textContent
  }

  if (event.target.classList.contains( 'name' )) {
    document.querySelector('#delete_chat_num').value = event.target.previousSibling.previousSibling.textContent
    document.querySelector('#modifiy_chat_num').value = event.target.previousSibling.previousSibling.textContent
    modal_box.classList.add('active')
    document.querySelector('#chat_modifiy_name').value = event.target.textContent
    document.querySelector('#chat_modifiy_text').value = event.target.previousSibling.textContent
  }

  if (event.target.classList.contains( 'date' )) {
    document.querySelector('#delete_chat_num').value = event.target.previousSibling.previousSibling.previousSibling.textContent
    document.querySelector('#modifiy_chat_num').value = event.target.previousSibling.previousSibling.previousSibling.textContent
    modal_box.classList.add('active')
    document.querySelector('#chat_modifiy_name').value = event.target.previousSibling.textContent
    document.querySelector('#chat_modifiy_text').value = event.target.previousSibling.previousSibling.textContent
  }

  if (event.target.classList.contains( 'chat_item' )) {
    document.querySelector('#delete_chat_num').value = event.target.childNodes[0].textContent
    document.querySelector('#modifiy_chat_num').value = event.target.childNodes[0].textContent
    modal_box.classList.add('active')
    document.querySelector('#chat_modifiy_name').value = event.target.childNodes[2].textContent
    document.querySelector('#chat_modifiy_text').value = event.target.childNodes[1].textContent
  }
  
});






var chat_delete_select = document.querySelector('.chat_delete_select');
var chat_modifiy_select = document.querySelector('.chat_modifiy_select');

var chat_delete_modal_box = document.querySelector('.chat_delete_modal_box');
var chat_modifiy_modal_box = document.querySelector('.chat_modifiy_modal_box');

chat_delete_select.addEventListener('click' , function(){
  chat_delete_modal_box.classList.add('active');
});

chat_modifiy_select.addEventListener('click' , function(){
  chat_modifiy_modal_box.classList.add('active');
});


var chat_delete_btn = document.querySelector('#chat_delete_btn');
chat_delete_btn.addEventListener('click' , function(){
  var formData = new FormData();
  var delete_chat_password = document.querySelector('#delete_chat_password').value;
  var delete_chat_num = document.querySelector('#delete_chat_num').value;
  formData.append('card[card_title]', $('#card_title').val());
  formData.append('type', 'del_chat');
  formData.append('password', delete_chat_password);
  formData.append('num', delete_chat_num);
  $.ajax({
    url : url,
    data : formData,
    type : 'POST',
    enctype : 'multipart/form-data',
    processData : false,
    contentType : false,
    success : function(result) {
      // console.log(result)
      if (result === 'NoPass') {
        alert('비밀번호가 다릅니다.')
      } else {
        var state = JSON.parse(result);
        // console.log(state.num)
        var clear_data = document.querySelector("#chat_" + state.num);
        // clear_data.remove();


        var table = $('#example').DataTable();
        table.row(clear_data).remove().draw(); 
        modalclose()
      }
    }
  });

});

var chat_delete_close = document.querySelector('#chat_delete_close');

chat_delete_close.addEventListener('click' , function(){
  modalclose()
});



var chat_modifiy_close = document.querySelector('#chat_modifiy_close');
chat_modifiy_close.addEventListener('click' , function(){
  modalclose()
});


var chat_modifiy_btn = document.querySelector('#chat_modifiy_btn');
chat_modifiy_btn.addEventListener('click' , function(){
  var formData = new FormData();
  var chat_modifiy_name = document.querySelector('#chat_modifiy_name').value;
  var chat_modifiy_password = document.querySelector('#chat_modifiy_password').value;
  var chat_modifiy_text = document.querySelector('#chat_modifiy_text').value;
  var modifiy_chat_num = document.querySelector('#modifiy_chat_num').value;
  formData.append('card[card_title]', $('#card_title').val());
  formData.append('type', 'edit_chat');
  formData.append('name', chat_modifiy_name);
  formData.append('comment', chat_modifiy_text);
  formData.append('password', chat_modifiy_password);
  formData.append('num', modifiy_chat_num);
  $.ajax({
    url : url,
    data : formData,
    type : 'POST',
    enctype : 'multipart/form-data',
    processData : false,
    contentType : false,
    success : function(result) {
      // console.log(result)
      if (result === 'NoPass') {
        alert('비밀번호가 다릅니다.')
      } else {
        var state = JSON.parse(result);
        console.log(state)
        var clear_data = document.querySelector("#chat_" + state.num);
        console.log(clear_data.firstChild.firstChild.childNodes)
        clear_data.firstChild.firstChild.childNodes[1].textContent = state.comment;
        clear_data.firstChild.firstChild.childNodes[2].textContent = state.name;  
        modalclose()   
      }
    }
  });
});



var modal_bg = document.querySelector('.chat .modal_bg');
modal_bg.addEventListener('click' , function(){
  modalclose()
});


function modalclose() {
  document.querySelector('#delete_chat_password').value = '';
  document.querySelector('#chat_modifiy_password').value = '';
  modal_box.classList.remove('active');
  chat_delete_modal_box.classList.remove('active');
  chat_modifiy_modal_box.classList.remove('active');
}





// 카카오 공유 및 문자 보내기 관련 코드

var sns_box = document.querySelector('.footer .sns_box');

var kakao_select = document.querySelector('#kakao_select').value;
var mail_select = document.querySelector('#mail_select').value;

var sns_kakao = document.querySelector('.sns_kakao');
var sns_mail = document.querySelector('.sns_mail');

if (kakao_select === '') {
  sns_kakao.style.display = 'none';
  sns_mail.style.marginLeft = 'auto'
};

if (mail_select === '') {
  sns_mail.style.display = 'none';
};

if (mail_select === '' && kakao_select === '') {
  sns_box.style.display = 'none';
}