<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public $timestamps = true;
}
