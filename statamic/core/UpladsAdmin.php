<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class UpladsAdmin extends Model
{
    public $timestamps = true;
    protected $table = 'uploads_admin';
}
