<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class Create extends Model
{
    public $timestamps = true;
    protected $table = 'create_uploads';
}
