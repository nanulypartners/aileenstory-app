<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class UploadsScene extends Model
{
    public $timestamps = true;
    protected $table = 'uploads_scene';
}
