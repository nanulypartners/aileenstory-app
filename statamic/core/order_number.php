<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class order_number extends Model
{
    public $timestamps = true;
    protected $table = 'order_number';
}
 