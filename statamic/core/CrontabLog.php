<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class CrontabLog extends Model
{
    public $timestamps = true;
    protected $table = 'crontab_log';
}
