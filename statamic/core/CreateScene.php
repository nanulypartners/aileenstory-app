<?php

namespace Statamic;

use Illuminate\Database\Eloquent\Model;

class CreateScene extends Model
{
    public $timestamps = true;
    protected $table = 'create_uploads_scene';
}
