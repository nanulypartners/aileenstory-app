<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: x-csrf-token");
include_once('./db_config.php');

$data = file_get_contents('php://input');
$data = json_decode($data);
$result = array();

if ($data->nanuly!="sksnfl2020@" || !$data->nanuly_type) {
    $result['state'] = "error";
    $result['msg'] = "잘못된 접근입니다.";
} else {

    if ($data->nanuly_type=="AdminRender") {

        $sql = "UPDATE uploads SET
                                renderType='{$data->data->type}',
                                state='1'
                                WHERE createNm='{$data->data->createNm}'
                                ";

        $mysqli->query($sql);
        $result['state'] = "success";

    } else if ($data->nanuly_type=="AdminModifyCount") {

        $sql = "UPDATE uploads SET
                                modifyCount='{$data->data->cnt}'
                                WHERE createNm='{$data->data->createNm}'
                                ";

        $mysqli->query($sql);
        $result['state'] = "success";

    } else if ($data->nanuly_type=="UserCodeInsert") {

        $sql = "UPDATE uploads SET
                                odNo='{$data->odNo}',
                                odSno='{$data->odSno}',
                                payment='{$data->payment}',
                                renderType='{$data->renderType}',
                                modifyCount='{$data->modifyCount}',
                                state='{$data->state}',
                                moviePath='{$data->moviePath}'
                                WHERE createNm='{$data->createNm}'
                                ";

        $mysqli->query($sql);
        $result['state'] = "success";

    } else if ($data->nanuly_type=="UserCreateNmInsert") {

        $sql = "INSERT INTO uploads SET
                                    createNm='{$data->createNm}',
                                    odNo='',
                                    odSNo='',
                                    odId='{$data->odId}',
                                    odName='{$data->odName}',
                                    goodsType='{$data->goodsType}',
                                    goodsNm='{$data->goodsNm}',
                                    goodsCd='{$data->goodsCd}',
                                    basicInfo='',
                                    sceneInfoUser='',
                                    sianPath='',
                                    moviePath='',
                                    payment='0',
                                    renderType='0',
                                    modifyCount='0',
                                    state='0',
                                    created_at=now(),
                                    updated_at=now()
                                    ";

        $mysqli->query($sql);
        $result['state'] = "success";

    } else if ($data->nanuly_type=="AdminInsert") {

        //DB삭제
        $sql = "DELETE FROM uploads_admin WHERE no='{$data->no}'";
        $mysqli->query($sql);

        //DB입력

        $basicInfo = json_encode($data->basicInfo, JSON_UNESCAPED_UNICODE);
        $basicInfo = substr(substr($basicInfo, 0, -1), 1);

        $sceneInfo = json_encode($data->sceneInfo, JSON_UNESCAPED_UNICODE);
        $sceneInfo = substr(substr($sceneInfo, 0, -1), 1);
        $sceneInfo = addslashes($sceneInfo);

        $sql = "INSERT INTO uploads_admin SET
                                    no='{$data->no}',
                                    type='{$data->type}',
                                    goodsType='{$data->goodsType}',
                                    goodsNm='{$data->goodsNm}',
                                    goodsCd='{$data->goodsCd}',
                                    useYn='{$data->useYn}',
                                    sampleVideoUrl='{$data->sampleVideoUrl}',
                                    templateId='{$data->templateId}',
                                    templateName='{$data->templateName}',
                                    templatePath='".addslashes($data->templatePath)."',
                                    playTime='{$data->playTime}',
                                    bgmLayerName='{$data->bgmLayerName}',
                                    bgmName='{$data->bgmName}',
                                    bgmChange='{$data->bgmChange}',
                                    basicInfo='{$basicInfo}',
                                    sceneInfo='{$sceneInfo}'
                                    ";

        $mysqli->query($sql);
        $result['state'] = "success";

    } else if ($data->nanuly_type=="AdminDelete") {

        foreach ($data->chk->chk as $key => $value) {
            $sql = "DELETE FROM uploads_admin WHERE no = " . $value;
            $mysqli->query($sql);
        }

        $result['state'] = "success";

    } else if ($data->nanuly_type=="deleteData") {

        $path = '/home/nanu/apps/statamic/storage/autorender/uploads/'.$data->data->createNm;

        if (is_dir($path)) {
            rmdir_ok($path);
        }

        $sql = "DELETE FROM uploads_scene WHERE createNm = " . $data->data->createNm;
        $mysqli->query($sql);

        $sql = "DELETE FROM uploads WHERE createNm = " . $data->data->createNm;
        $mysqli->query($sql);

        $result['state'] = "success";

    }



}

function rmdir_ok($dir) {
    $dirs = dir($dir);
    while(false !== ($entry = $dirs->read())) {
    if(($entry != '.') && ($entry != '..')) {
        if(is_dir($dir.'/'.$entry)) {
           rmdir_ok($dir.'/'.$entry);
        } else {
           @unlink($dir.'/'.$entry);
        }
    }
    }
    $dirs->close();
    @rmdir($dir);
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);
exit;


