<?php

$auth_key = "!44f11~a3sE6ML5F8A@%8P#B$dds@*"; //인증키
$my_time = time(); //서버시간

$auth = hash('sha256', $auth_key.$_POST['auth_time']);

if ($_POST['auth_time']>$my_time && $_POST['auth']===$auth) {
	$file = $_POST['path'];
	$file_path = '/home/nanu/apps/statamic'.$file;

	if (strpos($file, 'storage/autorender') !== false) {
		if (file_exists($file_path)) {
			$filename = explode("/", $file);
			$original = $filename[count($filename)-1];
			header('Content-Type: video/mp4');
			header('Content-Length: ' . filesize($file_path));
			header("content-disposition: attachment; filename=\"$original\"");
			readfile($file_path); 			
		} else {
			echo '<SCRIPT LANGUAGE="javascript">';
			echo 'alert("파일이 존재하지 않습니다.");';
			echo 'history.back();'; 
			echo '</SCRIPT>'; 		
		}
	} else {
		echo '<SCRIPT LANGUAGE="javascript">';
		echo 'alert("다운로드 권한이 없습니다.");';
		echo 'history.back();'; 
		echo '</SCRIPT>'; 		
	}

} else {
	echo '<SCRIPT LANGUAGE="javascript">';
	echo 'alert("다운로드 권한이 없습니다.");';
	echo 'history.back();'; 
	echo '</SCRIPT>'; 
}
