<?php

set_time_limit(300);

$errorno = $_GET["errorno"];
$errormsg = $_GET["errormsg"];
$id = $_GET["id"];
$folder = $_GET["folder"];

if(!empty($errorno)) {
    if($errorno == "200") {
	    // do nothing
    } else {
        echo "Error";
        echo "(".$errorno."): ";
        echo $errormsg." : Order Id:".$id;
    }
    exit;
}

if(empty($id)) {
    echo "Error: Empty ORDER ID.";
    exit;
}

if(empty($folder)) {
    echo "Error: Empty folder of order id ". $id . ".";
    exit;
}

// ex: https://movie.studioium.com/ium-result/draft/1584415282776.mp4
$path =  $folder."/".$id.".mp4";
$url = "https://movie.aileenstory.com/result/".$path;
$mp4 = file_get_contents($url);

if(empty($mp4)) {
    echo "Error: File not found: Order Id: ". $id;
    exit;
}

if(strlen($mp4) < 1) {
    echo "Error: Empty file: Order Id: ". $id;
    exit;
}

$output_path = "/home/nanu/apps/statamic/storage/autorender/";

mkdir($output_path.$folder, 0775, true);

$file = file_put_contents($output_path.$path, $mp4);

if(!empty($file)) {
    echo "Success: " . $url;
} else {
    echo "Failed: OrderId ".$id;
}
