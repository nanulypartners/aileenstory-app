<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: x-csrf-token");
require '../statamic/vendor/autoload.php';
require './vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoUploadException;

function sendGodo($arrData) {

    $url = "http://aileenstory.com/api/card.php";
    $json_data = json_encode($arrData, JSON_UNESCAPED_UNICODE);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($json_data)));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output, true);

    $result = 'ok';
    if ($output['state']!="success") {
        $result = 'error';
    }

    return $result;
}

if ($_POST['type']=="vimeo_upload" && $_POST['pw']=="nanuly2020!") {

    $client = new Vimeo("f5ba5aba0f3ef2d33972255fb000b2aa652eee15", "rrknJD95csAsZcJP9LJk1wYOtLgt4PPguEx9mAM8kpK7yt5/bBdNUwRJBMkWTltLX+uT/ZARYAJu8JMUumkEaas7ufLpQ7OXntIkZgcGcR1/BBYMPDj3IEqTnV6JqhNl", "864943b8e4cad0eb683f0a7d70cc0e8f");

    $file_name = $_FILES['file']['tmp_name'];
    $uri = $client->upload($file_name, array(
        "name" => "초대장영상",
        "description" => "모바일 초대장 영상 입니다."
    ));

    $client->request($uri, array(
        'privacy' => array(
            'view' => 'disable'
        )
    ), 'PATCH');

    $client->request('/users/15644276/projects/2287827'.$uri, '', 'PUT');

    $response = $client->request($uri . '?fields=link');

    $result['uri'] = $uri;
    $result['link'] = $response['body']['link'];

    $arrData = array();
    $arrData['nanuly_type'] = 'vimeoUpload';
    $arrData['nanuly'] = "sksnfl2020@";
    $arrData['orderNo'] = $_POST['orderNo'];
    $arrData['odName'] = $_POST['odName'];
    $arrData['odMemNo'] = $_POST['odMemNo'];
    $arrData['category'] = $_POST['category'];
    $arrData['skin'] = $_POST['skin'];
    $arrData['vimeoUri'] = $uri;
    sendGodo($arrData);

    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;

}

if ($_POST['type']=="vimeo_delete" && $_POST['pw']=="nanuly2020!") {

    $client = new Vimeo("f5ba5aba0f3ef2d33972255fb000b2aa652eee15", "rrknJD95csAsZcJP9LJk1wYOtLgt4PPguEx9mAM8kpK7yt5/bBdNUwRJBMkWTltLX+uT/ZARYAJu8JMUumkEaas7ufLpQ7OXntIkZgcGcR1/BBYMPDj3IEqTnV6JqhNl", "864943b8e4cad0eb683f0a7d70cc0e8f");

    if ($_POST['uri']!="") {
        $uri = $_POST['uri'];
        $client->request($uri, array(), 'DELETE');

        $arrData = array();
        $arrData['nanuly_type'] = 'vimeoDelete';
        $arrData['nanuly'] = "sksnfl2020@";
        $arrData['orderNo'] = $_POST['orderNo'];
        $arrData['vimeoUri'] = $uri;
        sendGodo($arrData);
    }

    echo "ok";
    exit;

}

$data = file_get_contents('php://input');

if($data){
    $_POST = json_decode($data, true);
}

$path = "/home/nanu/apps/statamic/site/content/pages/";
$myfolder = $path.$_POST['card']['card_title']."/";
$mychat = $myfolder."chat/";
$myimg = "/home/nanu/apps/statamic/assets/pages/".$_POST['card']['card_title']."/";
$myyaml = $myfolder."/data.yaml";
$myindex = $myfolder."/index.md";
$type = $_POST['type'];
$old_folder = $_POST['old_folder'];
$id = $_POST['orderNo'].'-'.$_POST['orderNo'];
$title = $_POST['card']['card_title'];
$template = $_POST['category'].'/'.$_POST['skin'].'/index';

$data = $_POST;

// 디렉토리 삭제
function rmdir_ok($dir) {
    $dirs = dir($dir);
    while(false !== ($entry = $dirs->read())) {
        if(($entry != '.') && ($entry != '..')) {
            if(is_dir($dir.'/'.$entry)) {
                rmdir_ok($dir.'/'.$entry);
            } else {
                @unlink($dir.'/'.$entry);
            }
        }
    }
    $dirs->close();
    @rmdir($dir);
}

if ($type=="chat") {

    @mkdir($mychat, 0775, true);

    $data['card']['content']['password'] = password_hash($data['card']['content']['password'], PASSWORD_BCRYPT);

    $k = 0;

    if(file_exists($mychat."chat.yaml")){
        $yaml_read = file_get_contents($mychat."chat.yaml");
        $parse = Yaml::parse($yaml_read);
        foreach($parse as $k=>$v){
            $data['msg'][$k] = $v;
            $data['msg'][$k]['num'] = $k;
        }
    }

    $k = $k+1;

    $data['msg'][$k] = $data['card']['content'];
    $data['msg'][$k]['num'] = $k;

    $arr2 = array("title"=>"chat", "id"=>$id."-chat", "fieldset"=>"card", "yaml_data"=>$data['msg']);
    $info_yaml = Yaml::dump($arr2);
    $info_data = Yaml::dump($data['msg']);
    file_put_contents($mychat."chat.yaml", $info_data);
    file_put_contents($mychat."index.md", $info_yaml);

    foreach($data['msg'] as $key=>$val){
        unset($data['msg'][$key]['password']);
    }

    stache_update();
    echo json_encode($data['msg'], JSON_UNESCAPED_UNICODE);

} else if ($type=="edit_chat") {
    $num = $data['num'] / 1;
    $password = $data['password'];

    $yaml_read = file_get_contents($mychat."chat.yaml");
    $parse = Yaml::parse($yaml_read);
    $info = $parse[$num];

    /* 패스워드 검증 */
    if ( !password_verify($password , $info['password']) ) {
        echo "NoPass";
        exit;
    }else{
        //데이터 업데이트
        $parse[$num]['name'] = $data['name'];
        $parse[$num]['comment'] = $data['comment'];

        $info_data = Yaml::dump($parse);
        file_put_contents($mychat."chat.yaml", $info_data);
        $return['state'] = "Y";
        $return['num'] = $data['num'];
        $return['name'] = $data['name'];
        $return['comment'] = $data['comment'];
        stache_update();
        echo json_encode($return, JSON_UNESCAPED_UNICODE);
        exit;
    }

} else if ($type=="del_chat") {
    $num = $data['num'] / 1;
    $password = $data['password'];

    $yaml_read = file_get_contents($mychat."chat.yaml");
    $parse = Yaml::parse($yaml_read);
    $info = $parse[$num];

    /* 패스워드 검증 */
    if ( !password_verify($password , $info['password']) ) {
        echo "NoPass";
        exit;
    }else{
        //데이터 삭제처리
        unset($parse[$num]);

        $info_data = Yaml::dump($parse);
        file_put_contents($mychat."chat.yaml", $info_data);
        $return['state'] = "Y";
        $return['num'] = $data['num'];
        stache_update();
        echo json_encode($return, JSON_UNESCAPED_UNICODE);
        exit;
    }

} else if ($type=="get_chat") {

    $yaml_read = file_get_contents($mychat."chat.yaml");
    $parse = Yaml::parse($yaml_read);

    foreach($parse as $key=>$val){
        unset($parse[$key]['password']);
    }

    $result = json_encode($parse, JSON_UNESCAPED_UNICODE);
    echo $result;

} else if ($type=="check_card") {

    $url = "http://aileenstory.com/goods/goods_card_check.php";
    $json_data = json_encode($data);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($json_data)));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    echo $output;

} else if ($type=="card") {

    @mkdir($myfolder, 0775, true);
    @mkdir($myimg, 0775, true);

    if($old_folder!=""){
        //폴더변경시 이전폴명 변경
        rename($path.$old_folder."/", $myfolder);
        //이전 메인이미지 폴더 변경
        rename("/home/nanu/apps/statamic/assets/pages/".$old_folder."/", $myimg);
        $data['card']['main_img']['main_url'] = "https://app.aileenstory.com/assets/pages/".$title."/main";
    }

    if($data['card']['main_img']['main_url'] && !(strpos($data['card']['main_img']['main_url'], 'https://app.aileenstory.com/assets/pages/') !== false)){
        $main_img = $data['card']['main_img']['main_url'];

        $filemain = fopen($myimg."main", "wb");
        $main_data = explode(',', $main_img);
        fwrite($filemain, base64_decode($main_data[1]));
        fclose($filemain);

        $data['card']['main_img']['main_url'] = "https://app.aileenstory.com/assets/pages/".$title."/main";
    }

    /*
        foreach($data['card']['photo'] as $k=>$v){
            $filemain = fopen($myimg.$k, "wb");
            $main_data = explode(',', $v);
            fwrite($filemain, base64_decode($main_data[1]));
            fclose($filemain);
            $data['card']['photo'][$k] = "https://card.studioium.com/assets/pages/".$title."/".$k;
        }
    */

    $arr2 = array("title"=>$title, "id"=>$id, "fieldset"=>"card", "template"=>$template, "yaml_data"=>$data);
    $info_yaml = Yaml::dump($arr2);
    $info_data = Yaml::dump($data);
    file_put_contents($myyaml, $info_data);
    file_put_contents($myindex, $info_yaml);

    stache_update();

    echo "Y";

} else if ($type=="get_card") {

    $yaml_read = file_get_contents($myindex);
    $parse = Yaml::parse($yaml_read);
    $result = json_encode($parse, JSON_UNESCAPED_UNICODE);
    echo $result;

} else if ($type=="del_card") {

    if($title!=""){
        if(is_dir($myfolder)){
            rmdir_ok($myfolder);
        }
        if(is_dir($myimg)){
            rmdir_ok($myimg);
        }
    }

    stache_update();
    echo "Y";

}


function stache_update(){

    $url = 'https://app.aileenstory.com/!/stache-update/stache-update';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $data = curl_exec($ch);
    curl_close($ch);

}