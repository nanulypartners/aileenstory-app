<?php
require '/home/nanu/apps/statamic/api/vendor/autoload.php';

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoUploadException;

// 디렉토리 삭제
function rmdir_ok($dir) {
    $dirs = dir($dir);
    while(false !== ($entry = $dirs->read())) {
        if(($entry != '.') && ($entry != '..')) {
            if(is_dir($dir.'/'.$entry)) {
                rmdir_ok($dir.'/'.$entry);
            } else {
                @unlink($dir.'/'.$entry);
            }
        }
    }
    $dirs->close();
    @rmdir($dir);
}

$url = "http://www.aileenstory.com/goods/goods_card_check_del.php?type=nanuly";

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json'));
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_POST, 1);
$output = json_decode(curl_exec($ch), true);
curl_close($ch);

foreach ($output as $data) {

    if (!empty($data['url'])) {
        //초대장삭제
        $path = "/home/nanu/apps/statamic/site/content/pages/";
        $myfolder = $path.$data['url']."/";
        $myimg = "/home/nanu/apps/statamic/assets/pages/".$data['url']."/";

        if ( $data['url']!="" ) {
            if ( is_dir($myfolder) ) {
                rmdir_ok($myfolder);
                echo ' * del : '.$data['url'];
            }
            if ( is_dir($myimg) ) {
                rmdir_ok($myimg);
            }
        }

    }

    if (!empty($data['vimeoUri'])) {
        //비메오삭제
        $client = new Vimeo("f5ba5aba0f3ef2d33972255fb000b2aa652eee15", "rrknJD95csAsZcJP9LJk1wYOtLgt4PPguEx9mAM8kpK7yt5/bBdNUwRJBMkWTltLX+uT/ZARYAJu8JMUumkEaas7ufLpQ7OXntIkZgcGcR1/BBYMPDj3IEqTnV6JqhNl", "864943b8e4cad0eb683f0a7d70cc0e8f");
        if ($data['vimeoUri']!="") {
            $client->request($data['vimeoUri'], array(), 'DELETE');
            echo ' * del : '.$data['vimeoUri'];
        }

    }

}