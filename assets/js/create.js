
const initSize = {
    winW: "",
    winH: "",
    winY: ""
};

const sizeEvent = () => {
    initSize.winW = window.innerWidth;
    initSize.winH = window.innerHeight;
    initSize.winY = window.scrollY;
    window.addEventListener("resize", () => {
        initSize.winW = window.innerWidth;
        initSize.winH = window.innerHeight;
        initSize.winY = window.scrollY;
    });
    window.addEventListener("scroll", () => {
        initSize.winW = window.innerWidth;
        initSize.winH = window.innerHeight;
        initSize.winY = window.scrollY;
    });
};
sizeEvent();


const common = {
    pageNumber: 1,
    sceneNumber: 1,
    slideNumber: 0,
}

const page = {
    section1: document.querySelector('#intro_content'),
    section2: document.querySelector('#scene_content'),
}

const header = {
    prevBtn: document.querySelector('#header .prev_btn'),
    nextBtn: document.querySelector('#header .next_btn'),
    nextClick() {
        this.nextBtn.addEventListener('click', () => {
            modal.requestModal.style.display = 'flex';
            modal.requestModalOk.onclick = async () => {
                try {
                    const res = await RequestUpload()
                    res.json().then(v => {
                        // console.log(v)
                        if (v.state === 'ok') {
                            window.close()
                        } else if (v.state === 'error') {
                            modal.createModal.style.display = 'flex';
                            modal.createModal.querySelector('.txt').textContent = v.msg
                        }
                    })
                } catch (e) {
                    // console.log(e)
                    modal.createModal.style.display = 'flex';
                    modal.createModal.querySelector('.txt').textContent = '저장중에 오류가 발생했습니다.'
                } finally {
                    modal.requestModal.style.display = 'none';
                }
            }
        })
    }
}

header.nextClick();
// header.prevClick();


window.addEventListener('load', async () => {
    // const res = await loadImageList();
    // res.json().then(v => {
    //     console.log(v)
    //     loadCreateLi(v);
    //     uploadBox.minusNumberChange();
    // })
    uploadBox.minusNumberChange()
})



const modal = {
    fileLoadingModal: document.querySelector('.file_loading_modal'),
    alertModal: document.querySelector('.alert_modal'),
    alertModalClose: document.querySelector('.alert_modal .close_btn'),
    alertModalBack: document.querySelector('.alert_modal .back'),
    createModal: document.querySelector('.create_modal'),
    createModalClose: document.querySelector('.create_modal .close_btn'),
    createModalBack: document.querySelector('.create_modal .back'),
    requestModal: document.querySelector('.request_modal'),
    requestModalOk: document.querySelector('.request_modal .close_btn'),
    requestModalBack: document.querySelector('.request_modal .back')
}

modal.requestModalBack.addEventListener('click', () => {
    modal.requestModal.style.display = 'none';
})


class ModalClose {
    constructor(target, btn, back) {
        this.target = target;
        this.btn = btn;
        this.back = back;
    }
    off() {
        this.btn.addEventListener('click', () => {
            this.target.style.display = 'none';
        })
        this.back.addEventListener('click', () => {
            this.target.style.display = 'none';
        })
    }
}

const alertModal = new ModalClose(
    modal.alertModal,
    modal.alertModalClose,
    modal.alertModalBack
)

const createModal = new ModalClose(
    modal.createModal,
    modal.createModalClose,
    modal.createModalBack
)

alertModal.off();
createModal.off();

// musicModal.off();


const tabBox = {
    list: document.querySelectorAll('#content .right .tab_box li')
}
tabBox.list[0].querySelector('button').classList.add('active');

const uploadBox = {
    list: document.querySelectorAll('#content .right .upload_list_box'),
    content: document.querySelectorAll('#content .right .upload_list_box .upload_list_content'),
    uploadBtn: document.querySelectorAll('#content .right .upload_list_box .upload_btn'),
    uploadInputs: document.querySelectorAll('#content .right .upload_list_box .upload_input'),
    fileCount: document.querySelectorAll('#content .upload_list_header .count'),
    removeBtn: document.querySelectorAll('#content .upload_list_box .remove_btn'),
    infoSaveBtn: document.querySelector('#content .upload_list_box .info_save'),
    textSaveBtn: document.querySelectorAll('#content .upload_list_box .text_save'),
    btnClick() {
        this.uploadBtn.forEach((target,index) => {
            target.addEventListener('click', function (e) {
                uploadBox.uploadInputs[index].click();
            })
        })
    },
    inputChange() {
        this.uploadInputs.forEach((target, index) => {
            target.addEventListener('change', function (e) {
                const files = target.files; // Array of all files
                const type = uploadBox.content[index].getAttribute('data-type');
                const scene = target.getAttribute('data-scene');
                // console.log(type)
                // console.log(tabName)
                fileUpload(files, type, scene, index);
                target.value = ''
            })
        })
    },
    minusNumberChange() {
        this.fileCount.forEach((target, index) => {
            const total = document.querySelectorAll('.total')[index].value;
            const imgLength = uploadBox.content[index].querySelectorAll('.img_container').length;
            const resultCount = total - imgLength;
            target.textContent = String(resultCount);
        })
    },
    remove() {
        this.removeBtn.forEach((target, index) => {
            target.addEventListener('click', async () => {
                console.log(target)
                const scene = target.getAttribute('data-scene');
                const sceneSub = target.getAttribute('data-sceneSub');
                await fileDelete(scene, sceneSub)
                const list = document.querySelectorAll('.img_container')
                list[index].remove()

            })
        })
    },
    infoSave() {
        this.infoSaveBtn.addEventListener('click', async () => {
            modal.fileLoadingModal.style.display = 'flex';
            const infoBox = document.querySelector('.info_box')
            await InfoSave(infoBox)
            modal.fileLoadingModal.style.display = 'none';
        })
    },
    textSave() {
        this.textSaveBtn.forEach((t, i) => {
            t.addEventListener('click', async () => {
                modal.fileLoadingModal.style.display = 'flex';
                const target = document.querySelectorAll('.text_input_box')[i]
                const scene = t.getAttribute('data-scene');
                await TextSave(target, 'text', scene)
                modal.fileLoadingModal.style.display = 'none';
            })
        })
    }
}
uploadBox.list[0].classList.add('active');
uploadBox.btnClick();
uploadBox.inputChange();
uploadBox.remove();
uploadBox.infoSave();
uploadBox.textSave();

class TargetActive {
    constructor(targets) {
        this.targets = targets
    }

    on() {
        this.targets.forEach((e, index) => {
            e.addEventListener('click', (v) => {
                // console.log(e)
                for (let i = 0; i < this.targets.length; i += 1) {
                    this.targets[i].querySelector('button').classList.remove('active');
                    uploadBox.list[i].classList.remove('active');
                }
                v.currentTarget.querySelector('button').classList.add('active');
                uploadBox.list[index].classList.add('active');
            })
        })
    }
}

const tabBoxActive = new TargetActive(
    tabBox.list
)

tabBoxActive.on();






const textBox = {

}









