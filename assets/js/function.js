
function fileUpload(files, type, tabName, index) {
    const formData = new FormData();
    formData.append('_token', document.querySelector('#_token').value);
    formData.append('goodsNo', document.querySelector('#goodsNo').value);
    formData.append('createNo', document.querySelector('#createNo').value);
    formData.append('type', type);
    formData.append('tab_name', tabName);

    let fileTypeCheck = true;

    [].forEach.call(files, (f) => {
        // console.log(f)
        if (type === 'photo') {
            const imgType = f.type.split('/')
            if (imgType[0] !== 'image') {
                fileTypeCheck = false;
            }
        } else if (type === 'video') {
            const videoType = f.type.split('/')
            if (videoType[0] !== 'video') {
                fileTypeCheck = false;
            }
        }
        formData.append('file[]', f)
    });

    if (!fileTypeCheck) {
        return
    }

    modal.fileLoadingModal.style.display = 'flex';
    modal.fileLoadingModal.querySelector('.txt').textContent = '사진을 배치하는 중입니다 잠시만 기다려주세요.'


    fetch('https://app.aileenstory.com/!/uploads/uploads', {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        res.json().then(v => {
            // console.log(v)
            const Ok = v.files.filter(h => h.subState === 'success');
            const Error = v.files.filter(t => t.subState === 'error');
            const Time = v.files.filter(f => f.subState === 'error_time')
            if (Ok.length >= 1) {
                for (let i = 0; i < Ok.length; i += 1) {
                    const li = createLi(Ok[i].synthesis_image, Ok[i].sceneNo)
                    uploadBox.content[index].prepend(li);
                    // upload.dropZone[index].querySelector('.upload_img_box').classList.add('active');
                    // upload.dropZone[index].querySelector('.upload_drop_box').classList.remove('active');
                    // upload.resultNumberChange();
                }
            }
            uploadBox.minusNumberChange()
            if (Error.length >= 1) {
                const total = uploadBox.list[index].querySelector('.total').value;
                modal.alertModal.querySelector('.count').textContent = total
                modal.alertModal.style.display = 'flex';
            }

        })

    }).catch(function (error) {
        console.log(error);
    }).finally(function () {
        modal.fileLoadingModal.style.display = 'none';
    })
}


function createLi(src, number) {
    const li = document.createElement('li');
    const imgBox = document.createElement('div');
    const img = document.createElement('img');
    const delBox = document.createElement('div');
    const line1 = document.createElement('span');
    const line2 = document.createElement('span');

    li.classList.add('img_container');

    imgBox.classList.add('img_box');
    img.src = src;
    delBox.classList.add('remove_btn');
    delBox.setAttribute('data-sceneno', number);
    delBox.addEventListener('click', () => fileDelete(number, li))
    delBox.append(line1)
    delBox.append(line2)
    imgBox.append(img)
    li.append(imgBox)
    li.append(delBox)
    return li
}


function fileDelete(sceneNo, li) {
    const formData = new FormData();
    formData.append('_token', document.querySelector('#_token').value);
    formData.append('createNo', document.querySelector('#createNo').value);
    formData.append('sceneNo', sceneNo);

    fetch('https://app.aileenstory.com/!/uploads/scene-del', {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        res.json().then(v => {
            // console.log(v)
            // const uploadBody = document.querySelectorAll('.upload_body');
            // const uploadImgBox = document.querySelectorAll('.upload_img_box');
            // const uploaddropBox = document.querySelectorAll('.upload_drop_box');
            // uploadBody.forEach((f, i) => {
            //     const li = f.querySelectorAll('.upload_img_box li');
            //     // console.log(li)
            //     if (li.length === 0) {
            //         uploadImgBox[i].classList.remove('active')
            //         uploaddropBox[i].classList.add('active');
            //     }
            // })
        })
        li.remove()
    }).catch(function (error) {
        console.log(error);
    }).finally(function () {
        uploadBox.minusNumberChange()
    })
}


function controllerData() {
    const goodsNo = document.querySelector('#goodsNo').value;
    const createNo = document.querySelector('#createNo').value;
    const subDomain = new Array('app', 'app', 'app', 'app', 'app', 'app');
    const sub = subDomain[Math.floor(Math.random() * subDomain.length)];

    const $url = `https://${sub}.aileenstory.com/!/autorender/scene/${goodsNo}/${createNo}/${common.sceneNumber}`;
    return fetch($url, {
        method: 'GET',
    }).then(res => {
        // console.log(res)
        return res;

    }).catch(function (error) {
        console.log(error);
    }).finally(function () {

    })
}

function slideFilesLoad() {
    const goodsNo = document.querySelector('#goodsNo').value;
    const createNo = document.querySelector('#createNo').value;

    fetch(`https://app.aileenstory.com/!/autorender/second/${goodsNo}/${createNo}`, {
        method: 'GET',
    }).then(res => {
        // console.log(res)
        res.json().then(v => {
            // console.log(v)

            const resList = Object.entries(v)
            // console.log(resList)

            resList.forEach((t, i) => {
                // console.log(i)
                // console.log(v[i])
                slideList.viewImg[i].src = t[1].image_thumb;
                if (t[1].complete === 1) {
                    slideList.imgBox[i].classList.remove('not');
                } else {
                    slideList.imgBox[i].classList.add('not');
                }
                if (t[1].text) {
                    const TEXT = t[1].text[0].length > 15 ? `${t[1].text[0].slice(0,15)}...` : t[1].text[0]
                    // console.log(TEXT)
                    slideList.imgBox[i].querySelector('.center p').textContent = TEXT;
                }
            });


            // // console.log(slideList.itemBoxs[0])
            slideList.imgBox[0].click();
            // // galleryThumbs.slideTo(2)
        })

    }).catch(function (error) {
        console.log(error);
    }).finally(function () {

    })
}


function slideFileUpload(file, sceneNo, type) {
    const formdata = new FormData();
    formdata.append('_token', document.querySelector('#_token').value);
    formdata.append('goodsNo', document.querySelector('#goodsNo').value);
    formdata.append('createNo', document.querySelector('#createNo').value);
    formdata.append('sceneNo', sceneNo)
    formdata.append('type', type);
    formdata.append('file', file);
    // console.log(file)
    // return

    // console.log(imageFormData)

    return fetch('https://app.aileenstory.com/!/uploads/basic', {
        method: 'POST',
        body: formdata
    }).then(res => {
        // console.log(res)
        return res

    }).catch(function (error) {
        console.log(error);

    }).finally(function () {

    })
}




function base64StringToBlob(base64) {
    const type = base64.match(/data:([^;]+)/)[1];
    base64 = base64.replace(/^[^,]+,/g, '');
    const options = {};
    if (type) {
        options.type = type;
    }
    const binaryArrayBuffer = [binaryStringToArrayBuffer(window.atob(base64))];
    return new Blob(binaryArrayBuffer, options);
}

function binaryStringToArrayBuffer(binary) {
    const length = binary.length;
    const buf = new ArrayBuffer(length);
    const arr = new Uint8Array(buf);
    for (let i = 0; i < length; i++) {
        arr[i] = binary.charCodeAt(i);
    }
    return buf;
}


function getOrientation(file, callback) {
    const reader = new FileReader();
    reader.onload = event => {
        const view = new DataView(event.target.result);

        if (view.getUint16(0, false) != 0xffd8) return callback(-2);

        const length = view.byteLength;
        let offset = 2;

        while (offset < length) {
            const marker = view.getUint16(offset, false);
            offset += 2;

            if (marker == 0xffe1) {
                if (view.getUint32((offset += 2), false) != 0x45786966) {
                    return callback(-1);
                }
                const little = view.getUint16((offset += 6), false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                const tags = view.getUint16(offset, little);
                offset += 2;

                for (let i = 0; i < tags; i++) if (view.getUint16(offset + i * 12, little) == 0x0112) return callback(view.getUint16(offset + i * 12 + 8, little));
            } else if ((marker & 0xff00) != 0xff00) break;
            else offset += view.getUint16(offset, false);
        }
        return callback(-1);
    };
    reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
}

function resetOrientation(srcBase64, srcOrientation, callback) {
    const _img = new Image();

    _img.onload = () => {
        const width = _img.width;
        const height = _img.height;
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        // set proper canvas dimensions before transform & export
        // console.log('aa' , srcOrientation)
        if (4 < srcOrientation && srcOrientation < 9) {
            canvas.width = height;
            canvas.height = width;
        } else {
            canvas.width = width;
            canvas.height = height;
        }
        // transform context before drawing image
        switch (srcOrientation) {
            case 2:
                ctx.transform(-1, 0, 0, 1, width, 0);
                break;
            case 3:
                ctx.transform(-1, 0, 0, -1, width, height);
                break;
            case 4:
                ctx.transform(1, 0, 0, -1, 0, height);
                break;
            case 5:
                ctx.transform(0, 1, 1, 0, 0, 0);
                break;
            case 6:
                ctx.transform(0, 1, -1, 0, height, 0);
                break;
            case 7:
                ctx.transform(0, -1, -1, 0, height, width);
                break;
            case 8:
                ctx.transform(0, -1, 1, 0, 0, width);
                break;
            default:
                break;
        }

        // draw image
        ctx.drawImage(_img, 0, 0);

        // export base64
        callback(canvas.toDataURL('image/jpeg'));
    };

    _img.src = srcBase64;
}



function previewAPI(number) {

    const goodsNo = document.querySelector('#goodsNo').value;
    const createNo = document.querySelector('#createNo').value
    return fetch(`https://app.aileenstory.com/!/autorender/preview/${goodsNo}/${createNo}/${number}`, {
        method: 'GET',
    }).then(res => {
        // console.log(res)
        return res

    }).catch(function (error) {
        console.log(error);

    }).finally(function () {

    })
}


function screenChange(formData) {

    return fetch(`https://app.aileenstory.com/!/uploads/scene_change`, {
        method: 'POST',
        body: formData
    }).then(res => {
        // console.log(res)
        return res

    }).catch(function (error) {
        console.log(error);

    }).finally(function () {

    })
}


function moveUp(target, duration = 500) {
    let display = window.getComputedStyle(target).display;
    if (display === "none") display = "block";
    target.style.display = display;
    target.style.bottom = '2%';
    target.style.opacity = 0;
    target.style.transitionProperty = "bottom, opacity";
    target.style.transitionDuration = duration + "ms";
    window.setTimeout(() => {
        target.style.bottom = '5%';
        target.style.opacity = 1;
    }, 100)
    window.setTimeout(() => {
        target.style.removeProperty("bottom");
        target.style.removeProperty("opacity");
        target.style.removeProperty("transition-duration");
        target.style.removeProperty("transition-property");
    }, duration)
}

function moveDown(target, duration = 500) {
    target.style.transitionProperty = "bottom, opacity";
    target.style.transitionDuration = duration + "ms";
    window.setTimeout(() => {
        target.style.bottom = '2%';
        target.style.opacity = 0;
    }, 100)
    window.setTimeout(() => {
        target.style.removeProperty("bottom");
        target.style.removeProperty("opacity");
        target.style.removeProperty("transition-duration");
        target.style.removeProperty("transition-property");
        target.style.display = 'none';
    }, duration)
}


window.mobilecheck = () => {
    const UserAgent = navigator.userAgent;
    if (UserAgent.match(/iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || UserAgent.match(/LG|SAMSUNG|Samsung/) != null) {
        return true;
    } else {
        return false;
    }
}

function musicLoadApi(goodsNo, createNo) {
    return fetch(`https://app.aileenstory.com/!/autorender/mp3/${goodsNo}/${createNo}`, {
        method: 'GET',
    }).then(res => {
        console.log(res)
        return res
    }).catch(function (error) {
        console.log(error);

    }).finally(function () {

    })
}


function RequestUpload() {
    const formdata = new FormData();
    formdata.append('_token', document.querySelector('#_token').value);
    formdata.append('goodsNo', document.querySelector('#goodsNo').value);
    formdata.append('createNo', document.querySelector('#createNo').value);

    return fetch(`https://app.aileenstory.com/!/uploads/movie`, {
        method: 'POST',
        body: formdata
    }).then(res => {
        console.log(res)
        return res
    }).catch(function (error) {
        console.log(error);

    }).finally(function () {

    })
}

