const text = () => {

    const textControllor = {
        save: document.querySelector('#scene_content .btn_box .save')
    };

    textControllor.save.onclick = async () => {


        modal.fileLoadingModal.style.display = 'flex';
        modal.fileLoadingModal.querySelector('.txt').textContent = '텍스트를 저장하는 중입니다 잠시만 기다려주세요.';

        const form = $('#FormData')[0];
        const formData = new FormData(form);
        formData.append('_token', document.getElementById('_token').value);

        const subDomain = new Array('app', 'app', 'app', 'app', 'app', 'app');
        const sub = subDomain[Math.floor(Math.random() * subDomain.length)];

        const res = await saveAPI(formData)
        res.json().then(v => {
            modal.fileLoadingModal.style.display = 'none';
            const number = document.querySelector('#canvas_sceneNo').value;
            if (number === v.sceneNo) {
                controllerData()
                    .then(res2 => {
                        if (res2) {
                            // galleryThumbs.update();
                            const controller = document.querySelector('.user_controller_box')
                            res2.text().then(v => {
                                controller.innerHTML = v;
                                if (document.querySelector('.photo_content')) {
                                    photo()
                                } else if (document.querySelector('.video_content')) {
                                    video()
                                } else if (document.querySelector('.text_content')) {
                                    text()
                                }
                            })
                        }
                        slideList.imgBox[number - 1].classList.remove('not');
                        if (initSize.winW <= 1100) {
                            document.querySelector('#scene_content > .right').style.display = 'none';
                        }
                    })
            }
        })

        // $.ajax({
        //     url: $url,
        //     data: formData,
        //     type: 'POST',
        //     enctype: 'multipart/form-data',
        //     processData: false,
        //     contentType: false,
        //     cache: false,
        //     success: function (data) {
        //         const $data = JSON.parse(data);
        //
        //         if ($data['sceneNo'] == document.querySelector('#FormData input[name=sceneNo]').value) {
        //             controllerData()
        //                 .then(res2 => {
        //                     if (res2) {
        //                         const controller = document.querySelector('.user_controller_box')
        //                         res2.text().then(v => {
        //                             controller.innerHTML = v;
        //                             if (document.querySelector('.photo_content')) {
        //                                 photo()
        //                             } else if (document.querySelector('.video_content')) {
        //                                 video()
        //                             } else if (document.querySelector('.text_content')) {
        //                                 text()
        //                             }
        //                         })
        //                     }
        //                     modal.fileLoadingModal.style.display = 'none';
        //                     slideList.fileBox[$data['sceneNo']].classList.remove('not');
        //                 })
        //         }
        //         // videoInit.galleryTopCurtain.style.display = "none";
        //         // videoInit.galleryThumbsCurtain[$data["sceneNo"] - 1].style.display = "none";
        //         // videoInit.curtain2.style.display = "none";
        //     }
        // });
    };

    function saveAPI(formData) {
        const subDomain = new Array('app', 'app', 'app', 'app', 'app', 'app');
        const sub = subDomain[Math.floor(Math.random() * subDomain.length)];

        return fetch(`https://${sub}.aileenstory.com/!/uploads/scene`, {
            method: 'POST',
            body: formData
        }).then(res => {
            // console.log(res)
            return res

        }).catch(function (error) {
            console.log(error);

        }).finally(function () {

        })
    }

};
