function fileUpload(files, type, scene, index) {
    const formData = new FormData();

    const pathname = window.location.pathname.split('/');


    formData.append('_token', document.querySelector('#_token').value);
    formData.append('goodsCd', `${pathname[5]}`);
    formData.append('goodsType', `${pathname[4]}`);
    formData.append('createNo', document.querySelector('#createNo').value);
    formData.append('type', type);
    // formData.append('sceneSub', sceneSub);
    formData.append('scene', scene);

    let fileTypeCheck = true;

    [].forEach.call(files, (f, i) => {
        // console.log(f)
        if (type === 'photo') {
            const imgType = f.type.split('/')
            if (imgType[0] !== 'image') {
                fileTypeCheck = false;
            }
        } else if (type === 'video') {
            const videoType = f.type.split('/')
            if (videoType[0] !== 'video') {
                fileTypeCheck = false;
            }
        }
        formData.append('file[]', f)
        formData.append('sceneSub[]', i + 1);
    });


    if (!fileTypeCheck) {
        return
    }

    modal.fileLoadingModal.style.display = 'flex';
    modal.fileLoadingModal.querySelector('.txt').textContent = '사진을 배치하는 중입니다 잠시만 기다려주세요.'


    fetch('https://app.aileenstory.com/!/create/scene', {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        res.json().then(v => {
            console.log(v)
            const Ok = v.files.filter(h => h.subState === 'success');
            const Error = v.files.filter(t => t.subState === 'error');
            const Time = v.files.filter(f => f.subState === 'error_time')
            if (Ok.length >= 1) {
                for (let i = 0; i < Ok.length; i += 1) {
                    const li = createLi(Ok[i].fileThumb, Ok[i].sceneNo, Ok[i].sceneSub)
                    // console.log(document.querySelector(`.box_${Ok[i].sceneNo}`))
                    document.querySelector(`.box_${Ok[i].sceneNo}`).prepend(li);
                    // uploadBox.content[index].prepend(li);
                    // upload.dropZone[index].querySelector('.upload_img_box').classList.add('active');
                    // upload.dropZone[index].querySelector('.upload_drop_box').classList.remove('active');
                    // upload.resultNumberChange();
                }
            }
            uploadBox.minusNumberChange()
            if (Error.length >= 1) {
                const total = document.querySelectorAll('.total')[index].value;
                modal.alertModal.querySelector('.count').textContent = total
                modal.alertModal.style.display = 'flex';
            }

        })

    }).catch(function (error) {
        console.log(error);
    }).finally(function () {
        modal.fileLoadingModal.style.display = 'none';
    })
}


function createLi(src, sceneNo, sceneSub) {
    const li = document.createElement('li');
    const imgBox = document.createElement('div');
    const img = document.createElement('img');
    const delBox = document.createElement('div');
    const line1 = document.createElement('span');
    const line2 = document.createElement('span');

    li.classList.add('img_container');

    imgBox.classList.add('img_box');
    img.src = src;
    delBox.classList.add('remove_btn');
    delBox.setAttribute('data-scene', sceneNo);
    delBox.setAttribute('data-scenesub', sceneSub);
    delBox.addEventListener('click', async () => {
        await fileDelete(sceneNo, sceneSub)
        li.remove()
    })
    delBox.append(line1)
    delBox.append(line2)
    imgBox.append(img)
    li.append(imgBox)
    li.append(delBox)
    return li
}


function fileDelete(scene, sceneSub) {
    const formData = new FormData();
    formData.append('_token', document.querySelector('#_token').value);
    formData.append('createNo', document.querySelector('#createNo').value);
    formData.append('scene', scene);
    formData.append('sceneSub', sceneSub);

    fetch('https://app.aileenstory.com/!/create/scene-del', {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        return res;
    }).catch(function (error) {
        console.log(error);
    }).finally(function () {
        // uploadBox.minusNumberChange()
    })
}

function InfoSave(infoBox) {
    const formData = new FormData();
    formData.append('createNo', document.querySelector('#createNo').value);
    formData.append('_token', document.querySelector('#_token').value);
    const inputs = infoBox.querySelectorAll('input');
    [].forEach.call(inputs, (f, i) => {
        formData.append('introInputValue[]', f.value)
    });

    fetch('https://app.aileenstory.com/!/create/basic-info', {
        method: 'POST',
        body: formData
    }).then(res => {
        return res;
    }).catch(function (error) {
        console.log(error);
    }).finally(function () {
        // uploadBox.minusNumberChange()
    })
}


function TextSave(target, type, scene) {
    const formData = new FormData();
    const pathname = window.location.pathname.split('/');
    formData.append('_token', document.querySelector('#_token').value);
    formData.append('goodsCd', `${pathname[5]}`);
    formData.append('goodsType', `${pathname[4]}`);
    formData.append('createNo', document.querySelector('#createNo').value);
    formData.append('type', type);
    formData.append('scene', scene);

    const inputs = target.querySelectorAll('.input');


    [].forEach.call(inputs, (f, i) => {
        // console.log(f.value)
        formData.append('text[]', f.value)
        formData.append('sceneSub[]', i + 1);
    });


    fetch('https://app.aileenstory.com/!/create/scene', {
        method: 'POST',
        headers: {},
        body: formData
    }).then(res => {
        // console.log(res)
        return res;
    }).catch(function (error) {
        console.log(error);
    }).finally(function () {
        modal.fileLoadingModal.style.display = 'none';
    })
}


function RequestUpload() {
    const formData = new FormData();
    const pathname = window.location.pathname.split('/');
    formData.append('_token', document.querySelector('#_token').value);
    formData.append('goodsType', `${pathname[4]}`);
    formData.append('goodsCd', `${pathname[5]}`);
    formData.append('createNo', document.querySelector('#createNo').value);

    return fetch('https://app.aileenstory.com/!/create/movie', {
        method: 'POST',
        body: formData
    }).then(res => {
        return res

    }).catch(function (error) {
        console.log(error);
        return error
    })
}














