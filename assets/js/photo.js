function photo() {
    const photoInit = {
        photoContent: document.querySelector('.photo_content'),
        imageInfo: JSON.parse(document.querySelector('#imageInfo').value),
        basicImage: document.querySelector('#basicImage').value,
        cutInfo: document.querySelector('#cutInfo').value,
        pointLine: document.querySelector('.point_line'),
        saveTimer: true,
    }

    const photoController = {
        reset: document.querySelector('.photo_content .controller_box .reset'),
        sizeUp: document.querySelector('.photo_content .controller_box .size_up'),
        sizeDown: document.querySelector('.photo_content .controller_box .size_down'),
        bright: document.querySelector('.photo_content .controller_box  #bright'),
        change: document.querySelector('.photo_content .controller_box .change'),
        rotate: document.querySelector('.photo_content .controller_box .rotate'),
        align: document.querySelector('.photo_content .controller_box .align'),
        heightAlign: document.querySelector('.photo_content .controller_box .height_align'),
        widthAlign: document.querySelector('.photo_content .controller_box .width_align'),
        mirrorOpen: document.querySelector('.photo_content .controller_box .mirror_open'),
        mirrorBox: document.querySelector('.photo_content .controller_box .mirror_box'),
        mirrorWhite: document.querySelector('.photo_content .controller_box .mirror_white'),
        mirrorBlack: document.querySelector('.photo_content .controller_box .mirror_black'),
        mirrorColor: document.querySelector('.photo_content .controller_box .mirror_color'),
        defaultBtns: document.querySelectorAll('.photo_content .controller_box .default'),
        mirrorBtns: document.querySelectorAll('.photo_content .controller_box .mirror'),
        mirrorClose: document.querySelector('.photo_content .controller_box .mirror_close'),
        prevSlide: document.querySelector('.photo_content .controller_box .prev'),
        nextSlide: document.querySelector('.photo_content .controller_box .next'),
        control: document.querySelectorAll('.photo_content .control'),
        save: document.querySelector('#scene_content .btn_box .save'),
        brightUp: document.querySelector('.photo_content .controller_box .bright_up'),
        brightDown: document.querySelector('.photo_content .controller_box .bright_down'),
    };

    let canvasResizeWidth;
    let canvasStateWidth;

    const canvasBox = document.querySelector('.canvas_box');
    const canvas = document.querySelector('#canvas');
    canvas.width = canvasBox.offsetWidth;
    canvas.height = canvasBox.offsetHeight;
    const ctx = canvas.getContext('2d');
    let angleInDegrees = 0;
    let canvasOffset = canvas.getBoundingClientRect();
    let offsetX = canvasOffset.left + document.body.scrollLeft;
    let offsetY = canvasOffset.top + document.body.scrollTop;
    let isDragging = false;

    let pi2 = Math.PI * 2;
    let resizerRadius = 8;
    let rr = resizerRadius * resizerRadius;
    let draggingResizer = {x: 0, y: 0};
    let startX;
    let startY;
    let isDown = false;
    let scale;
    let imageX = 0;
    let imageY = 0;
    let mouseX;
    let mouseY;
    let imageWidth, imageHeight, imageRight, imageBottom;
    let draggingImage = false;
    let image = new Image();
    let canvasBack = 0;
    let blur = 3;
    let bright = 0;


    canvas.width = canvasBox.offsetWidth;
    canvas.height = canvasBox.offsetHeight;

    // console.log(photoInit.photoContent)

    const canvasResizeWidthEvent = () => {
        canvasResizeWidth = canvasBox.offsetWidth;
    };
    canvasResizeWidthEvent();
    window.addEventListener('resize', () => {
        canvasResizeWidthEvent();
    });

    const isMobile = () => {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    };

    function draw(withAnchors, withBorders) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.save();

        if (canvasBack === 0) {
            // photoController.mirrorBlack.classList.add('active');
            // photoController.mirrorWhite.classList.remove('active');
            // photoController.mirrorColor.classList.remove('active');
            ctx.fillStyle = 'rgb(000,000,000)';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
        } else if (canvasBack === 1) {
            ctx.fillStyle = 'rgb(255,255,255)';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
        } else if (canvasBack === 2) {
            const $image = image;
            const $canvas = document.createElement('canvas');
            const $ctx = $canvas.getContext('2d');
            const $maxW = canvas.width;
            const $maxH = canvas.height;
            const $iw = $image.width;
            const $ih = $image.height;
            const $scale = Math.min($maxW / $iw, $maxH / $ih) / 1.5;
            const $iwScaled = $iw * $scale;
            const $ihScaled = $ih * $scale;
            let $state;
            let $startW;
            let $startH;
            let $endW;
            let $endH;
            // console.log(angleInDegrees)

            $endW = $iwScaled;
            $endH = $ihScaled;
            $canvas.width = $endW;
            $canvas.height = $endH;
            $startW = 0;
            $startH = 0;

            $ctx.drawImage($image, $startW, $startH, $endW, $endH);

            let sum = 0;
            const delta = 5;
            const alpha_left = 1 / (2 * Math.PI * delta * delta);
            const step = blur < 3 ? 1 : 2;
            for (let y = -blur; y <= blur; y += step) {
                for (let x = -blur; x <= blur; x += step) {
                    const weight = alpha_left * Math.exp(-(x * x + y * y) / (2 * delta * delta));
                    sum += weight;
                }
            }
            let count = 0;
            for (let y = -blur; y <= blur; y += step) {
                for (let x = -blur; x <= blur; x += step) {
                    count++;
                    $ctx.globalAlpha = ((alpha_left * Math.exp(-(x * x + y * y) / (2 * delta * delta))) / sum) * blur;

                    $ctx.drawImage($canvas, x, y);
                    // console.log(x , y)
                }
            }

            blur = 3;
            $ctx.globalAlpha = 1;

            if (angleInDegrees === 0 || angleInDegrees === 180) {
                if ($iwScaled / 16 >= $ihScaled / 9) {
                    $state = $maxH / $ihScaled;
                } else {
                    $state = $maxW / $iwScaled;
                }
                $endW = $iwScaled * $state;
                $endH = $ihScaled * $state;
                $startW = $maxW / 2 + ($endW / 2) * -1;
                $startH = $maxH / 2 + ($endH / 2) * -1;
            } else if (angleInDegrees === 90 || angleInDegrees === 270) {
                if ($iwScaled / 9 >= $ihScaled / 16) {
                    $state = $maxW / $ihScaled;
                } else {
                    $state = $maxH / $iwScaled;
                }
                $endW = $iwScaled * $state;
                $endH = $ihScaled * $state;
                $startW = $maxH / 2 + ($endW / 2) * -1;
                $startH = $maxW / 2 + ($endH / 2) * -1;
            }

            const _canvas = document.createElement('canvas');
            const _ctx = _canvas.getContext('2d');
            const _maxW = canvas.width;
            const _maxH = canvas.height;
            _canvas.width = _maxW;
            _canvas.height = _maxH;

            // console.log(_canvas.width , _canvas.height , $startW , $startH , $endW , $endH ,)
            _ctx.translate(_canvas.width / 2, _canvas.height / 2);
            _ctx.rotate((angleInDegrees * Math.PI) / 180);
            if (angleInDegrees === 0 || angleInDegrees === 180) {
                _ctx.translate(-_canvas.width / 2, -_canvas.height / 2);
            } else if (angleInDegrees === 90 || angleInDegrees === 270) {
                _ctx.translate(-_canvas.height / 2, -_canvas.width / 2);
            }
            _ctx.drawImage($canvas, $startW, $startH, $endW, $endH);
            _ctx.restore();
            ctx.drawImage(_canvas, 0, 0, canvas.width, canvas.height);


        }


        ctx.translate(canvas.width / 2, canvas.height / 2);
        ctx.rotate((angleInDegrees * Math.PI) / 180);
        ctx.translate(-canvas.width / 2, -canvas.height / 2);

        ctx.drawImage(image, 0, 0, image.width, image.height, imageX, imageY, imageWidth, imageHeight);




        // Bright up / down
        if (bright) {
            const $gab = (canvas.width - canvas.height) / 2;
            let imageData;
            if (angleInDegrees === 90) {
                imageData = ctx.getImageData(-imageY + (canvas.height - imageHeight) + $gab, imageX - $gab, imageHeight, imageWidth);
            } else if (angleInDegrees === 180) {
                imageData = ctx.getImageData(-imageX + (canvas.width - imageWidth), -imageY + (canvas.height - imageHeight), imageWidth, imageHeight);
            } else if (angleInDegrees === 270) {
                imageData = ctx.getImageData(imageY + $gab, -imageX + (canvas.width - imageWidth) - $gab, imageHeight, imageWidth);
            } else {
                imageData = ctx.getImageData(imageX, imageY, imageWidth, imageHeight);
            }

            const px = imageData.data;
            const length = px.length;
            const adjust = Math.floor(255 * (bright / 100));
            for (let i = 0; i < length; i += 4) {
                px[i] += adjust;
                px[i + 1] += adjust;
                px[i + 2] += adjust;
            }

            if (angleInDegrees === 90) {
                ctx.putImageData(imageData, -imageY + (canvas.height - imageHeight) + $gab, imageX - $gab);
            } else if (angleInDegrees === 180) {
                ctx.putImageData(imageData, -imageX + (canvas.width - imageWidth), -imageY + (canvas.height - imageHeight));
            } else if (angleInDegrees === 270) {
                ctx.putImageData(imageData, imageY + $gab, -imageX + (canvas.width - imageWidth) - $gab);
            } else {
                ctx.putImageData(imageData, imageX, imageY);
            }

        }

        ctx.restore();


    }

    const standardImage = () => {
        imageX = canvas.width / 2 - (image.width / 2) * scale;
        imageY = canvas.height / 2 - (image.height / 2) * scale;
        imageWidth = image.width * scale;
        imageHeight = image.height * scale;
        imageRight = imageX + imageWidth;
        imageBottom = imageY + imageHeight;
    };

    const widthImage = () => {
        imageX = canvas.width / 2 - (image.width / 2) * scale;
        //imageY = (canvas.height / 2) - (image.height / 2) * scale;
        imageWidth = image.width * scale;
        //imageHeight=image.height * scale;
        imageRight = imageX + imageWidth;
        //imageBottom=imageY+imageHeight;
    };

    const heightImage = () => {
        //imageX = (canvas.width / 2) - (image.width / 2) * scale;
        imageY = canvas.height / 2 - (image.height / 2) * scale;
        //imageWidth=image.width * scale;
        imageHeight = image.height * scale;
        //imageRight=imageX+imageWidth;
        imageBottom = imageY + imageHeight;
    };

    const drawDragAnchor = (x, y) => {
        ctx.beginPath();
        ctx.arc(x, y, resizerRadius, 0, pi2, false);
        ctx.closePath();
        ctx.fill();
    };

    const anchorHitTest = (x, y) => {
        let dx, dy;
        // top-left
        dx = x - imageX;
        dy = y - imageY;
        if (dx * dx + dy * dy <= rr) {
            return 0;
        }
        // top-right
        dx = x - imageRight;
        dy = y - imageY;
        if (dx * dx + dy * dy <= rr) {
            return 1;
        }
        // bottom-right
        dx = x - imageRight;
        dy = y - imageBottom;
        if (dx * dx + dy * dy <= rr) {
            return 2;
        }
        // bottom-left
        dx = x - imageX;
        dy = y - imageBottom;
        if (dx * dx + dy * dy <= rr) {
            return 3;
        }
        return -1;
    };

    const hitImage = (x, y) => {
        return x > imageX && x < imageX + imageWidth && y > imageY && y < imageY + imageHeight;
    };

    const handleMouseDown = e => {
        if (angleInDegrees === 0 || angleInDegrees === 180) {
            startX = parseInt(e.clientX - offsetX);
            startY = parseInt(e.clientY - offsetY);
        } else if (angleInDegrees === 90 || angleInDegrees === 270) {
            startX = parseInt(e.clientY - offsetY);
            startY = parseInt(e.clientX - offsetX);
        }
        draggingResizer = anchorHitTest(startX, startY);
        //draggingImage = draggingResizer < 0 && hitImage(startX,startY);
        draggingImage = draggingResizer < 0;
    };

    const handleMouseUp = e => {
        draggingResizer = -1;
        draggingImage = false;
        draw(true, false);


    };

    const handleMouseOut = e => {
        handleMouseUp(e);
    };

    const handleMouseMove = e => {
        // console.log(draggingImage);
        if (draggingImage) {
            // imageClick=false;

            if (angleInDegrees === 0 || angleInDegrees === 180) {
                mouseX = parseInt(e.clientX - offsetX);
                mouseY = parseInt(e.clientY - offsetY);
            } else if (angleInDegrees === 90 || angleInDegrees === 270) {
                mouseX = parseInt(e.clientY - offsetY);
                mouseY = parseInt(e.clientX - offsetX);
            }

            // move the image by the amount of the latest drag

            var dx = mouseX - startX;
            var dy = mouseY - startY;

            if (angleInDegrees === 0) {
                imageX += dx;
                imageY += dy;
            } else if (angleInDegrees === 90) {
                imageX += dx;
                imageY -= dy;
            } else if (angleInDegrees === 180) {
                imageX -= dx;
                imageY -= dy;
            } else if (angleInDegrees === 270) {
                imageX -= dx;
                imageY += dy;
            }

            imageRight += dx;
            imageBottom += dy;
            // reset the startXY for next time
            startX = mouseX;
            startY = mouseY;
            // redraw the image with border
            draw(false, true);

        }
    };

    const handleTouchStart = e => {
        // console.log(e.touches[0].clientX)
        // console.log(e.touches[0].clientY)
        if (angleInDegrees === 0 || angleInDegrees === 180) {
            startX = parseInt(e.touches[0].clientX - offsetX);
            startY = parseInt(e.touches[0].clientY - offsetY);
        } else if (angleInDegrees === 90 || angleInDegrees === 270) {
            startX = parseInt(e.touches[0].clientY - offsetY);
            startY = parseInt(e.touches[0].clientX - offsetX);
        }
        draggingResizer = anchorHitTest(startX, startY);
        draggingImage = draggingResizer < 0;
    };

    const handleTouchMove = e => {
        e.preventDefault();
        if (draggingImage) {
            // imageClick=false;

            if (angleInDegrees === 0 || angleInDegrees === 180) {
                mouseX = parseInt(e.touches[0].clientX - offsetX);
                mouseY = parseInt(e.touches[0].clientY - offsetY);
            } else if (angleInDegrees === 90 || angleInDegrees === 270) {
                mouseX = parseInt(e.touches[0].clientY - offsetY);
                mouseY = parseInt(e.touches[0].clientX - offsetX);
            }
            // move the image by the amount of the latest drag

            var dx = mouseX - startX;
            var dy = mouseY - startY;

            if (angleInDegrees === 0) {
                imageX += dx;
                imageY += dy;
            } else if (angleInDegrees === 90) {
                imageX += dx;
                imageY -= dy;
            } else if (angleInDegrees === 180) {
                imageX -= dx;
                imageY -= dy;
            } else if (angleInDegrees === 270) {
                imageX -= dx;
                imageY += dy;
            }

            imageRight += dx;
            imageBottom += dy;
            // reset the startXY for next time
            startX = mouseX;
            startY = mouseY;
            // redraw the image with border
            draw(false, true);
            draw(false, true);

        }
    };

    const sizeUpEvent = () => {
        scale += 0.01;
        standardImage();
        draw(true, false);

    };

    const sizeDownEvent = () => {
        scale -= 0.01;
        standardImage();
        draw(true, false);
        // if (photoInit.saveTimer) {
        //     clearTimeout(photoInit.saveTimer)
        // }
        // photoInit.saveTimer = setTimeout(function(){
        //     saveEvent()
        // }, 500)
    };

    const rotateEvent = () => {
        // scale = Math.min(canvas.width / image.width, canvas.height / image.height)
        standardImage();
        draw(true, false);
        ctx.restore();
        // saveEvent()
    };

    const brightUpEvent = () => {
        bright += 5;
        draw(true, false);
    };

    const brightDownEvent = () => {
        bright -= 5;
        draw(true, false);
    };

    const resetEvent = () => {
        canvasBack = 0;
        scale = Math.min(canvas.width / image.width, canvas.height / image.height);
        //standardImage();
        angleInDegrees = 0;
        bright = 0;
        rotateEvent();
    };

    const initEvent = () => {
        canvasBack = 0;
        scale = Math.min(canvas.width / image.width, canvas.height / image.height);
        //standardImage();
        angleInDegrees = 0;
        bright = 0;
        standardImage();
        draw(true, false);
        ctx.restore();
    }



    const mirrorWhiteEvnet = () => {
        canvasBack = 1;
        draw(true, false);
    };

    const mirrorBlackEvent = () => {
        canvasBack = 0;
        draw(true, false);
    };

    const mirrorColorEvent = () => {
        canvasBack = 2;
        draw(true, false);
    };

    function pohtoEventListener() {
        if (!isMobile()) {
            canvasBox.addEventListener('mousedown', e => {
                handleMouseDown(e);
            });
            canvasBox.addEventListener('mousemove', e => {
                handleMouseMove(e);

            });
            canvasBox.addEventListener('mouseup', e => {
                handleMouseUp(e);

            });
            canvasBox.addEventListener('mouseout', e => {
                handleMouseOut(e);
            });
        } else {
            canvasBox.addEventListener('touchstart', e => {
                handleTouchStart(e);
            });
            canvasBox.addEventListener('touchmove', e => {
                handleTouchMove(e);
            });
        }

        photoController.reset.addEventListener('click', e => {
            resetEvent();
        });
        photoController.sizeUp.addEventListener('click', sizeUpEvent);

        photoController.sizeDown.addEventListener('click', e => {
            sizeDownEvent();
        });

        // photoController.bright.addEventListener('input', e => {
        //     bright = e.target.value;
        //     draw(true, false);
        // })


        photoController.rotate.addEventListener('click', e => {
            angleInDegrees += 90;
            if (angleInDegrees === 360) {
                angleInDegrees = 0;
            }
            // imageUserInfoJson.rotate = angleInDegrees;
            rotateEvent();
        });
        photoController.align.addEventListener('click', () => {
            widthImage();
            heightImage();
            draw(true, false);
        })

        photoController.mirrorOpen.addEventListener('click', () => {
            photoController.mirrorBox.style.display = 'flex';
            document.querySelector('.controller_box .controll_ok').style.display = 'none';

        });
        photoController.mirrorWhite.addEventListener('click', () => {
            mirrorWhiteEvnet();
            photoController.mirrorBox.style.display = 'none';
            document.querySelector('.controller_box .controll_ok').style.display = 'flex';
        });
        photoController.mirrorBlack.addEventListener('click', () => {
            mirrorBlackEvent();
            photoController.mirrorBox.style.display = 'none';
            document.querySelector('.controller_box .controll_ok').style.display = 'flex';
        });
        photoController.mirrorColor.addEventListener('click', () => {
            mirrorColorEvent();
            photoController.mirrorBox.style.display = 'none';
            document.querySelector('.controller_box .controll_ok').style.display = 'flex';
        });
        photoController.brightUp.addEventListener('click', e => {
            brightUpEvent();
        });

        photoController.brightDown.addEventListener('click', e => {
            brightDownEvent();
        });

        // photoController.imgFile.addEventListener('change', (e) => {
        //     // console.log(e.target)
        //     // const number = document.querySelector('#canvas_sceneNo').value;
        //     controlInputChange()
        // })
        // photoController.mirrorClose.addEventListener('click', () => {
        //     Array.from(photoController.defaultBtns).forEach(v => {
        //         v.style.display = 'flex';
        //     });
        //     Array.from(photoController.mirrorBtns).forEach(v => {
        //         v.style.display = 'none';
        //     })
        // });
        // photoController.prevSlide.addEventListener('click', () => {
        //     if (common.slideNumber !== 0) {
        //         slideList.itemBoxs[common.slideNumber -1].click()
        //     }
        //
        // });
        // photoController.nextSlide.addEventListener('click', () => {
        //     if (common.slideNumber !== slideList.itemBoxs.length -1) {
        //         slideList.itemBoxs[common.slideNumber +1].click();
        //     }
        // });
        photoController.change.addEventListener('click', (e) => {
            const number = document.querySelector('#canvas_sceneNo').value;
            // console.log(slideList.fileBtns)
            Array.from(slideList.fileBtns).forEach(function(v,i){
                const targetNumber = v.querySelector('.sceneNo').value;
                if (targetNumber === number) {
                    slideList.fileBtns[i].click();
                }
            })
        });
    };

    photoController.save.onclick = () => {
        saveEvent();
    };


    function loadImg() {
        if ( photoInit.basicImage !== '' && photoInit.cutInfo === '') {
            image.src = photoInit.basicImage;
            image.onload = () => {
                canvasStateWidth = canvasBox.offsetWidth;
                // draw()
                // resetEvent();
                initEvent();
                pohtoEventListener();
            }
        } else if( photoInit.basicImage !== '' && photoInit.cutInfo !== '') {
            image.src = photoInit.basicImage;
            photoInit.cutInfo = JSON.parse(photoInit.cutInfo);
            image.onload = () => {
                imageX = photoInit.cutInfo.imageX;
                imageY = photoInit.cutInfo.imageY;
                imageWidth = photoInit.cutInfo.imageWidth;
                imageHeight = photoInit.cutInfo.imageHeight;
                scale = photoInit.cutInfo.scale;
                angleInDegrees = photoInit.cutInfo.angleInDegrees;
                canvasBack = photoInit.cutInfo.canvasBack;
                canvasStateWidth = photoInit.cutInfo.canvasStateWidth;
                bright = photoInit.cutInfo.bright;
                // document.querySelector('#bright').value = photoInit.cutInfo.bright;
                const canvasResizeScale = canvasResizeWidth / canvasStateWidth;

                imageX = imageX * canvasResizeScale;
                imageY = imageY * canvasResizeScale;

                imageWidth = imageWidth * canvasResizeScale;
                imageHeight = imageHeight * canvasResizeScale;

                draw();
            }


            pohtoEventListener();
        } else {
            pohtoEventListener();
        }
    }
    loadImg()


    function pointLine() {

        const sizeW = (canvas.width / 1920) * photoInit.imageInfo.width;
        const sizeH = (canvas.height / 1080) * photoInit.imageInfo.height;

        const W = sizeW * canvas.height;
        const H = sizeH * canvas.width;

        let sizeState;

        if (W > H) {
            sizeState = canvas.width / sizeW;
        } else {
            sizeState = canvas.height / sizeH;
        }

        const pointW = sizeW * sizeState;
        const pointH = sizeH * sizeState;

        photoInit.pointLine.style.width = `${pointW}px`;
        photoInit.pointLine.style.height = `${pointH}px`;


    }
    pointLine();


    function saveEvent() {

        const basicImage = document.querySelector('#basicImage').value;
        if (!basicImage) {
            return false
        }

        // slideList.freeviewLoading.style.display = 'flex';
        //
        // upload.loadingModal.querySelector('.title').innerHTML = ` <img src="/assets/image/file-upload.png" alt="">
        //     자료를<span>&nbsp;업로드&nbsp;</span> 중입니다!`;
        // upload.loadingModal.querySelector('.txt').textContent = '업로드가 완료되면 자동으로 창이 닫힙니다.';
        //
        // upload.loadingModal.style.display = 'flex';
        // setTimeout(function () {
        //     upload.loadingBar.classList.add('loading_20');
        //     upload.loadingBar.addEventListener('transitionend', () => {
        //         upload.loadingBar.classList.add('loading_80');
        //     })
        // }, 100)

        modal.fileLoadingModal.style.display = 'flex';
        modal.fileLoadingModal.querySelector('.txt').textContent = '사진을 저장하는 중입니다 잠시만 기다려주세요.';


        const number = document.querySelector('#canvas_sceneNo').value;
        // Array.from(slideList.fileBtns).forEach(function(t,i){
        //     const target = t.querySelector('.sceneNo').value;
        //     if (target === number) {
        //         slideList.imageLoadings[i].style.display = 'flex';
        //     }
        // })
        // document.querySelector('.canvas_loading_box').style.display = 'flex';

        photoController.control.forEach(v => {
            v.style.pointerEvents = 'none';
        });




        canvasStateWidth = canvasBox.offsetWidth;
        const cutInfo = {
            width: image.width,
            height: image.height,
            imageX,
            imageY,
            imageWidth,
            imageHeight,
            scale,
            angleInDegrees,
            canvasBack,
            canvasStateWidth,
            bright: bright
        };
        canvas.width = 1920;
        canvas.height = 1080;

        imageX *= 1920 / canvasBox.offsetWidth;
        imageY *= 1920 / canvasBox.offsetWidth;
        imageWidth *= 1920 / canvasBox.offsetWidth;
        imageHeight *= 1920 / canvasBox.offsetWidth;
        blur = 10;
        draw();

        const sizeW = (canvasBox.offsetWidth / 1920) * photoInit.imageInfo.width;
        const sizeH = (canvasBox.offsetHeight / 1080) * photoInit.imageInfo.height;

        const W = sizeW * canvasBox.offsetHeight;
        const H = sizeH * canvasBox.offsetWidth;

        let sizeState;

        if (W > H) {
            sizeState = canvasBox.offsetWidth / sizeW;
        } else {
            sizeState = canvasBox.offsetHeight / sizeH;
        }

        const _cutEndX = photoInit.imageInfo.width * sizeState;
        const _cutEndY = photoInit.imageInfo.height * sizeState;

        const _cutStartX = (canvas.width - _cutEndX) / 2;
        const _cutStartY = (canvas.height - _cutEndY) / 2;

        const _cutCanvas = document.createElement('canvas');
        _cutCanvas.width = _cutEndX;
        _cutCanvas.height = _cutEndY;
        const _cutCtx = _cutCanvas.getContext('2d');

        const _image = new Image();
        _image.src = canvas.toDataURL('image/jpeg', 1);

        _image.onload = async () => {
            _cutCtx.drawImage(_image, _cutStartX, _cutStartY, _cutEndX, _cutEndY, 0, 0, _cutEndX, _cutEndY);

            const src = base64StringToBlob(_cutCanvas.toDataURL('image/jpeg', 1));
            const form = document.querySelector('#FormData');
            const formData = new FormData(form);
            formData.append('_token', document.getElementById('_token').value);
            formData.append('basic_image', photoInit.basic_image);
            formData.append('cut_image', src);
            for (let key in cutInfo) {
                formData.append('cut_info[' + key + ']', cutInfo[key]);
            }
            const res = await saveAPI(formData)

            res.json().then(v => {
                // document.querySelector('.canvas_loading_box').style.display = 'none';

                Array.from(slideList.fileBtns).forEach(async function(t,i){
                    const target = t.querySelector('.sceneNo').value;
                    if (target === v.sceneNo) {
                        slideList.fileBox[i].querySelector('.view_img img').src = v.synthesis_thumb;
                        // slideList.imageLoadings[i].style.display = 'none';
                    }
                })
                const number = document.querySelector('#canvas_sceneNo').value;
                if (number === v.sceneNo) {
                    controllerData()
                        .then(res2 => {
                            if (res2) {
                                const controller = document.querySelector('.user_controller_box')
                                res2.text().then(v => {
                                    controller.innerHTML = v;
                                    if (document.querySelector('.photo_content')) {
                                        photo()
                                    } else if (document.querySelector('.video_content')) {
                                        video()
                                    } else if (document.querySelector('.text_content')) {
                                        text()
                                    }
                                })
                            }
                            modal.fileLoadingModal.style.display = 'none';
                        })


                    previewAPI(document.querySelector('#canvas_sceneNo').value)
                        .then(res3 => {
                            if (res3) {
                                res3.json().then(v => {
                                    freeView.imgBox.src = v.image
                                })
                            }
                        })

                    slideList.imgBox[number - 1].classList.remove('not');
                    if (initSize.winW <= 1100) {
                        document.querySelector('#scene_content > .right').style.display = 'none';
                    }
                }

                // upload.loadingBar.classList.add('loading_100');
                // setTimeout(function () {
                //     upload.loadingModal.style.display = 'none';
                //     upload.loadingBar.classList.remove('loading_20', 'loading_80', 'loading_100');
                // }, 500)
            })



        }
    }



    function saveAPI(formData) {

        const subDomain = new Array('app', 'app', 'app', 'app', 'app', 'app');
        const sub = subDomain[Math.floor(Math.random() * subDomain.length)];

        return fetch(`https://${sub}.aileenstory.com/!/uploads/scene`, {
            method: 'POST',
            body: formData
        }).then(res => {
            // console.log(res)
            return res

        }).catch(function (error) {
            console.log(error);

        }).finally(function () {

        })
    }



}
