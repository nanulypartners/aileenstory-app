class guideEvent {
  constructor(modal, closeBtn, closeBtn2) {
    this.modal = modal;
    this.closeBtn = closeBtn;
    this.closeBtn2 = closeBtn2;
  }
  modalEvent() {
    this.modal.classList.add('active');

    this.closeBtn.addEventListener('click', () => {
      this.modal.classList.remove('active');
    });

    this.closeBtn2.addEventListener('click', () => {
      this.modal.classList.remove('active');
    });
  }
}

class newGuideEvent extends guideEvent {
  constructor(modal, closeBtn, closeBtn2, guideLine) {
    super(modal, closeBtn, closeBtn2);
    this.guideLine = guideLine;
    this.inputBox = document.querySelector('#upload_section .input_content_box').offsetTop;
  }
  initSize() {
    this.inputBox = document.querySelector('#upload_section .input_content_box').offsetTop + 36;
    this.guideLine.style.top = `${this.inputBox}px`;
  }
  sizeEvent() {
    this.initSize();
    window.addEventListener('resize', () => this.initSize());
  }
}

const introGuidePage = new newGuideEvent(
  document.querySelector('.guide_page1'),
  document.querySelector('.guide_page1 .guide_close'),
  document.querySelector('.guide_page1 .guide_close2'),
  document.querySelector('.guide_page1 .guide_line')
);

const createGudiePage = new guideEvent(document.querySelector('.guide_page2'), document.querySelector('.guide_page2 .guide_close'), document.querySelector('.guide_page2 .guide_close2'));

const page1Guide = () => {
  introGuidePage.modalEvent();
  introGuidePage.sizeEvent();
};

const page2Guide = () => {
  createGudiePage.modalEvent();
};

const pageGuide = () => {
  const openBtn = document.querySelector('#header .popup_open');
  openBtn.removeEventListener('click', page1Guide);
  openBtn.removeEventListener('click', page2Guide);
  switch (common.pageNum) {
    case 1:
      openBtn.addEventListener('click', page1Guide);
      break;
    case 2:
      page2Guide();
      openBtn.addEventListener('click', page2Guide);
      document.querySelector('.guide_page2').classList.add('active');
    default:
      break;
  }
};
pageGuide();
