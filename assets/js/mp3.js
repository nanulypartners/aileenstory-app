const audioFile = () => {
  const audioInput = document.querySelector('#audio_section #audio');
  const audioBefore = document.querySelector('#audio_section .before');
  const audioAfter = document.querySelector('#audio_section .after');
  const fileName = document.querySelector('#audio_section .after .file_name');
  const fileClose = document.querySelector('#audio_section .after .file_close');
  const audioBollBox = document.querySelector('.audio_content .audio_boll_box');

  const first = common.location[4];
  const second = common.location[5];
  const $url = `/!/autorender/mp3/${first}/${second}`;

  $.ajax({
    url: $url,
    type: 'GET',
    processData: false,
    contentType: false,
    cache: false,
    success: function(data) {
      const $data = JSON.parse(data);
      if ($data['state'] == 'ok') {
        if ($data['data']['text']) {
          fileName.textContent = $data['data']['text'];
          audioBefore.style.display = 'none';
          audioAfter.style.display = 'block';
        }
      } else if ($data['state'] == 'error') {
        alert($data['msg']);
      } else {
        alert('알수 없는 오류가 발생했습니다. 다시 시도해 주세요.');
      }
    }
  });

  audioInput.addEventListener('change', e => {
    const form = $('#FormData')[0];
    const formData = new FormData(form);
    const $url = '/!/uploads/mp3';
    formData.append('_token', document.getElementById('_token').value);

    const files = e.target.files; // FileList object
    const file = files[0];

    if (file.type.match('mp3.*') || file.type.match('audio.*')) {
      audioBollBox.style.display = 'flex';
      formData.append('mp3', file);

      $.ajax({
        url: $url,
        data: formData,
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
          const $data = JSON.parse(data);
          if ($data['state'] == 'ok') {
            const name = e.target.files[0].name;
            fileName.textContent = name;
            audioBefore.style.display = 'none';
            audioAfter.style.display = 'block';
            audioBollBox.style.display = 'none';
          } else if ($data['state'] == 'error') {
            alert($data['msg']);
          } else {
            alert('알수 없는 오류가 발생했습니다. 다시 시도해 주세요.');
          }
        }
      });
    } else {
      alert('not an mp3');
    }
  });

  fileClose.addEventListener('click', () => {
    const form = $('#FormData')[0];
    const formData = new FormData(form);
    const $url = '/!/uploads/mp3';
    formData.append('_token', document.getElementById('_token').value);
    formData.append('type', 'del');

    $.ajax({
      url: $url,
      data: formData,
      type: 'POST',
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false,
      success: function(data) {
        const $data = JSON.parse(data);
        if ($data['state'] == 'ok') {
          audioInput.value = '';
          audioBefore.style.display = 'block';
          audioAfter.style.display = 'none';
        } else if ($data['state'] == 'error') {
          alert($data['msg']);
        } else {
          alert('알수 없는 오류가 발생했습니다. 다시 시도해 주세요.');
        }
      }
    });
  });
};

audioFile();
