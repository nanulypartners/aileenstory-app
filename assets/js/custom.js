let common = {
  location: location.pathname.split('/'),
  pageNum: 1,
  controllState: 1,
  theme: 'lihgt',
  pageNext: document.querySelector('#header .btn_box button.next'),
  pageNext2: document.querySelector('#footer button.next')
};

const page = {
  section1: document.querySelector('#upload_section'),
  section2: document.querySelector('#create_section'),
  section3: document.querySelector('#audio_section'),
  section4: document.querySelector('#making_section')
};

const createSectionSize = () => {
  const controllSizeEvent = () => {
    const winW = window.innerWidth;
    const galleryTop = document.querySelector('#create_section .gallery-top').clientHeight;
    const sampleVideoBoxTop = document.querySelector('#upload_section .sample_video_box').clientHeight;
    const galleryThumbs = document.querySelector('#create_section .gallery-thumbs');
    const guidePage2BtmBorder = document.querySelector('.guide_page2 .left .btm');
    const userFirstDataInputContent = document.querySelector('.user_first_data .input_content_box');
    const userFirstGuideLine = document.querySelector('.guide_page1 .guide_line');
    if (winW <= 767) {
      galleryThumbs.style.maxHeight = `calc(${window.innerHeight}px - 45px - ${galleryTop}px - 36px - 67px)`;
      guidePage2BtmBorder.style.height = `calc(${window.innerHeight}px - 45px - ${galleryTop}px - 52px - 67px)`;
      userFirstDataInputContent.style.maxHeight = `calc(${window.innerHeight}px - 45px - ${sampleVideoBoxTop}px - 19px - 67px)`;
      // console.log(userFirstDataInputContent.offsetHeight);
      userFirstGuideLine.style.height = `calc(${userFirstDataInputContent.offsetHeight}px - 36px)`;
    } else {
      galleryThumbs.style.maxHeight = '372px';
      guidePage2BtmBorder.style.height = '363px';
      userFirstDataInputContent.style.maxHeight = '330px';
    }
  };
  controllSizeEvent();
  window.addEventListener('resize', () => {
    controllSizeEvent();
  });
};

var loading = 1;

const controllerData = () => {
  const first = common.location[4];
  const second = common.location[5];

  const subDomain = new Array('app', 'app', 'app', 'app', 'app', 'app');
  const sub = subDomain[Math.floor(Math.random() * subDomain.length)];
  const $url = `https://${sub}.aileenstory.com/!/autorender/scene/${first}/${second}/${common.controllState}`;
  $.ajax({
    url: $url,
    type: 'GET',
    processData: false,
    contentType: false,
    cache: false,
    success: function(data) {
      loading = 1;
      // console.log($('.gallery-top .curtain')[0])
      $('.user_controller_box').html(data);

      if (document.querySelector('.photo_content')) {
        $(function() {
          photoEvent();
        });
      } else if (document.querySelector('.video_content')) {
        $(function() {
          videoEvent();
        });
      } else if (document.querySelector('.text_content')) {
        $(function() {
          textEvent();
        });
      }
    }
  });
};

const pageElement = () => {
  const headerTitle = document.querySelector('#header .header_title');
  const headerNext = document.querySelector('#header .btn_box button.next span');
  const footer = document.querySelector('#footer');
  const pagePrevBtn = document.querySelector('#header .btn_box .prev');
  const openBtn = document.querySelector('#header .popup_open');

  switch (common.pageNum) {
    case 1:
      page.section1.style.display = 'block';
      page.section2.style.display = 'none';
      page.section3.style.display = 'none';
      page.section4.style.display = 'none';

      headerTitle.textContent = '기본정보 입력';
      headerNext.textContent = '장면편집';
      common.pageNext2.textContent = '장면편집으로 넘어가기';
      openBtn.style.display = 'block';

      footer.classList.remove('active');
      pagePrevBtn.classList.remove('active');
      common.pageNext.classList.remove('active');
      common.pageNext2.classList.remove('active');

      createSectionSize();
      break;
    case 2:
      page.section1.style.display = 'none';
      page.section2.style.display = 'block';
      page.section3.style.display = 'none';
      page.section4.style.display = 'none';

      headerTitle.textContent = '장면편집';
      headerNext.textContent = '음악변경';
      common.pageNext2.textContent = '음악변경으로 넘어가기';
      openBtn.style.display = 'block';

      if (document.querySelector("#bgm").value=="") {
        headerNext.textContent = '제작안내';
        common.pageNext2.textContent = '제작안내로 넘어가기';
      }

      footer.classList.add('active');
      pagePrevBtn.classList.add('active');

      if ($('.incomplete').length == 0) {
        common.pageNext.classList.add('active');
        common.pageNext2.classList.add('active');
      } else {
        common.pageNext.classList.remove('active');
        common.pageNext2.classList.remove('active');
      }

      // common.controllState = 1;
      controllerData();
      createSectionSize();
      break;
    case 3:
      page.section1.style.display = 'none';
      page.section2.style.display = 'none';
      page.section3.style.display = 'block';
      page.section4.style.display = 'none';

      headerTitle.textContent = '음악변경';
      headerNext.textContent = '제작안내';
      common.pageNext2.textContent = '제작안내로 넘어가기';
      openBtn.style.display = 'none';

      footer.classList.remove('active');
      pagePrevBtn.classList.add('active');
      common.pageNext.classList.add('active');
      common.pageNext2.classList.add('active');

      break;
    case 4:
      page.section1.style.display = 'none';
      page.section2.style.display = 'none';
      page.section3.style.display = 'none';
      page.section4.style.display = 'block';

      headerTitle.textContent = '제작안내';
      headerNext.textContent = '영상제작';
      common.pageNext2.textContent = '영상제작';
      openBtn.style.display = 'none';

      footer.classList.remove('active');
      pagePrevBtn.classList.add('active');
      common.pageNext.classList.add('active');
      common.pageNext2.classList.add('active');

      break;
    default:
      break;
  }
};

pageElement();

const userState = () => {
  const introInput = document.querySelectorAll('#upload_section .user_first_data .input_content_box input');

  // console.log(introInput.value)
  const validation = currentValue => currentValue !== '';
  let introInputValue = [];
  const valueCheck = () => {
    let check = introInputValue.every(validation);
    if (check && common.pageNum == 1) {
      common.pageNext.classList.add('active');
      common.pageNext2.classList.add('active');
    } else {
      common.pageNext.classList.remove('active');
      common.pageNext2.classList.remove('active');
    }
    // console.log(introInputValue.every(validation))
  };
  Array.prototype.forEach.call(introInput, (e, i) => {
    introInputValue[i] = e.value;
    valueCheck();
    e.addEventListener('keyup', () => {
      introInputValue[i] = e.value;
      valueCheck();
    });
  });
};

userState();

var galleryThumbs = new Swiper('.gallery-thumbs', {
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true
});

var galleryTop = new Swiper('.gallery-top', {
  // spaceBetween: 10,
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  simulateTouch: false,
  thumbs: {
    swiper: galleryThumbs
  },
  navigation: {
    nextEl: '.user_controller .btn_box .next',
    prevEl: '.user_controller .btn_box .prev'
  },
  on: {
    transitionEnd: function() {
      // console.log(this.activeIndex + 1);
      const galleryThumbsCurtain = document.querySelectorAll('.gallery-thumbs .curtain');
      // console.log(galleryThumbsCurtain[this.activeIndex].style.display);
      if (galleryThumbsCurtain[this.activeIndex].style.display === 'flex') {
        $('.gallery-top .curtain')[0].style.display = 'flex';
        $('.gallery-top .curtain_text_box')[0].style.display = 'flex';
        $('.curtain2')[0].style.display = 'block';
        $('.curtain2 .text_box')[0].style.display = 'flex';
      } else {
        $('.gallery-top .curtain')[0].style.display = 'none';
        $('.gallery-top .curtain_text_box')[0].style.display = 'none';
        $('.curtain2')[0].style.display = 'none';
        $('.curtain2 .text_box')[0].style.display = 'none';
      }
      common.controllState = this.activeIndex + 1;
      controllerData();
    }
  }
});

const nextPageEvent = () => {
  const introInput = document.querySelectorAll('#upload_section .user_first_data .input_content_box input');
  const makingPopup = document.querySelector('#making_section .making_popup');
  common.pageNext.addEventListener('click', () => {
    axiosEvent();
  });
  common.pageNext2.addEventListener('click', () => {
    axiosEvent();
  });

  const axiosEvent = () => {
    let introInputValue = [];
    Array.prototype.forEach.call(introInput, (e, i) => {
      introInputValue[i] = e.value;
    });

    // console.log(introInputValue)
    if (common.pageNum === 1) {
      const _token = document.getElementById('_token').value;
      const first = common.location[4];
      const second = common.location[5];
      const $url = `/!/autorender/basic-info/${first}/${second}`;

      $.ajax({
        url: $url,
        type: 'post',
        data: {
          // 보낼 데이터
          introInputValue,
          _token
        },
        dataType: 'json',
        success: function(data) {
          // console.log(data)
          if (data.state == 'success') {
            common.pageNum = 2;
            // page.section1.style.display = 'none';
            // page.section2.style.display = 'block';
            pageElement();
            galleryThumbs.update();
            galleryTop.update();
            pageGuide();
          } else {
            alert(data.msg);
          }
        }
      });
    } else if (common.pageNum === 2) {
      
      if (document.querySelector("#bgm").value=="") {
        common.pageNum = 4;
      } else {
        common.pageNum = 3;
      }

      pageElement();
      pageGuide();
    } else if (common.pageNum === 3) {
      common.pageNum = 4;
      pageElement();
      pageGuide();
    } else if (common.pageNum === 4) {
      const form = $('#FormData')[0];
      const formData = new FormData(form);
      formData.append('_token', document.getElementById('_token').value);
      const $url = '/!/uploads/movie';
      $.ajax({
        url: $url,
        data: formData,
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
          const $data = JSON.parse(data);
          if ($data['state'] == 'ok') {
            makingPopup.style.display = 'flex';
            setTimeout(() => {
              makingPopup.style.opacity = '1';
            }, 100);
          } else if ($data['state'] == 'error') {
            alert($data['msg']);
          } else {
            alert('알수 없는 오류가 발생했습니다. 다시 시도해 주세요.');
          }
        }
      });
    }
  };
};

nextPageEvent();

class pageBack {
  constructor(btn) {
    this.btn = btn;
    this.userController = document.querySelector('#create_section .user_controller');
    this.header = document.querySelector('#header');
    this.guidePage2 = document.querySelector('.guide_page2');
  }
  backEvent() {

    this.btn.addEventListener('click', () => {
      if (common.pageNum === 2) {
        if (common.theme === 'lihgt') {
          common.pageNum = 1;
          pageElement();
          pageGuide();
          userState();
        } else {
          this.userController.classList.remove('active');
          this.header.classList.remove('active');
          common.theme = 'lihgt';
          this.guidePage2.classList.remove('dark');
        }
      } else if (common.pageNum === 3) {
        common.pageNum = 2;
        pageElement();
        galleryThumbs.update();
        galleryTop.update();
        pageGuide();
      } else if (common.pageNum === 4) {

        if (document.querySelector("#bgm").value=="") {
          common.pageNum = 2;
          pageElement();
          galleryThumbs.update();
          galleryTop.update();
          pageGuide();
        } else {
          common.pageNum = 3;
          pageElement();
          pageGuide();          
        }

      }
    });
  }
}

const prevPageEvent = () => {
  const pagePrevBtn = new pageBack(document.querySelector('#header .btn_box .prev'));
  pagePrevBtn.backEvent();
  const pagePrevBtn2 = new pageBack(document.querySelector('#header .title_box .prev_link'));
  pagePrevBtn2.backEvent();
};

prevPageEvent();

const editPopup = () => {
  const swiperSlideItem = document.querySelectorAll('#create_section .gallery-top .swiper-slide');
  const userController = document.querySelector('#create_section .user_controller');
  const header = document.querySelector('#header');
  const guidePage2 = document.querySelector('.guide_page2');
  // console.log(swiperSlideItem)
  Array.prototype.forEach.call(swiperSlideItem, e => {
    e.addEventListener('click', () => {
      header.classList.add('active');
      userController.classList.add('active');
      common.theme = 'dark';
      guidePage2.classList.add('dark');
    });
  });
};

editPopup();

const windowCloseBtn = document.querySelector('#making_section .making_popup .btn_box button');

windowCloseBtn.addEventListener('click', () => {
  window.close();
});

// const audioFile = () => {
//   const audioInput = document.querySelector('#audio_section #audio');
//   const audioBefore = document.querySelector('#audio_section .before');
//   const audioAfter = document.querySelector('#audio_section .after');
//   const fileName = document.querySelector('#audio_section .after .file_name');
//   const fileClose = document.querySelector('#audio_section .after .file_close');
//   // console.log(fileName)
//   audioInput.addEventListener('change', e => {
//     // console.log(e);
//     const name = e.target.files[0].name;
//     // console.log(name)
//     fileName.textContent = name;
//     audioBefore.style.display = 'none';
//     audioAfter.style.display = 'block';
//   });

//   fileClose.addEventListener('click', () => {
//     audioInput.value = '';
//     audioBefore.style.display = 'block';
//     audioAfter.style.display = 'none';
//   });
// };

// audioFile();
