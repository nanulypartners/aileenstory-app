function video() {
    const videoInit = {
        videoContent: document.querySelector('#create_section .video_content'),
        videoInfo: JSON.parse(document.querySelector('#videoInfo').value),
        cutInfo: document.querySelector('#cutInfo'),
        pointLine: document.querySelector('.user_controller .canvas_box .point_line'),
        myVideo: document.querySelector('#my_video'),
        numberCenter: document.querySelector('.controller_box .slider_bar .control_box .number.center'),
        timelineBar: document.querySelector('.timeline_box .input_bar_box .bar'),
        timelineBarDiv: document.querySelector('.timeline_box .input_bar_box .bar_div'),
        nowList : '',
        nowFirstList : '',
        videoOk: document.querySelector('.video_content .controller_box .video_ok'),
    }

    const videoController = {
        timeline: document.querySelector('.video_content #timeline'),
        play: document.querySelector('.video_content .controller_box .play'),
        change: document.querySelector('.video_content .controller_box .change'),
        rotate: document.querySelector('.video_content .controller_box .rotate'),
        rotateValue: document.querySelector('#video_rotate'),
        save: document.querySelector('#scene_content .btn_box .save'),
        control: document.querySelectorAll('.video_content .control'),
        cut: document.querySelector('.video_content .controller_box .cut'),
        cutBox: document.querySelector('.video_content .controller_box .cut_box'),
        last_time: document.querySelector('.video_content .cut_box #last_time'),
        timeResultBtn: document.querySelector('.video_content .cut_box .result_btn'),
    }

    function videoEventListener() {
        videoController.play.addEventListener('click', () => {
            // console.log(videoInit.myVideo.currentTime)
            // console.log(videoInit.nowList)
            if (parseInt(videoInit.myVideo.currentTime) >= parseInt(videoInit.nowList)) {
                videoInit.myVideo.currentTime = videoInit.nowFirstList;
            }

            if (videoInit.myVideo.paused) {
                videoInit.myVideo.play();
                videoController.play.querySelector('.text').textContent = `정지`;
            } else {
                videoInit.myVideo.pause();
                videoController.play.querySelector('.text').textContent = `재생`;
            }

        });

        videoController.change.addEventListener('click', () => {
            const number = document.querySelector('#canvas_sceneNo').value;
            Array.from(slideList.fileBtns).forEach(function(v,i){
                const targetNumber = v.querySelector('.sceneNo').value;
                if (targetNumber === number) {
                    slideList.fileBtns[i].click();
                }
            })
        });

        videoController.cut.addEventListener('click', () => {
            videoInit.myVideo.currentTime = $(videoController.timeline).val() / 100;
            const last = `${Number(videoInit.myVideo.currentTime) + Number(videoInit.videoInfo.video_time)}`;
            videoInit.numberCenter.textContent = `${videoInit.myVideo.currentTime.toFixed(2)}-${Number(last).toFixed(2)}`;
            videoInit.videoOk.style.display = 'none';
            videoController.cutBox.style.display = 'block';

            const barCutTime = Math.round((videoInit.videoInfo.video_time / videoController.last_time.textContent) * 10) * 10;
            videoController.timeline.classList.add(`point${barCutTime}`);

        });

        videoController.timeResultBtn.addEventListener('click', () => {
            videoInit.videoOk.style.display = 'flex';
            videoController.cutBox.style.display = 'none';
        });


        videoInit.nowList = `${Number(videoInit.myVideo.currentTime) + Number(videoInit.videoInfo.video_time)}`;


        videoInit.myVideo.addEventListener('timeupdate', function(){
            const $end_time = parseFloat($(videoController.timeline).val() / 100) + parseFloat($base_time);
            const $now = this.currentTime.toFixed(2);
            if ($now >= $end_time) {
                videoInit.myVideo.pause();
                videoController.play.querySelector('.text').textContent = `재생`;
            }
        });

        videoController.rotate.addEventListener('click', () => {
            rotate_check();
        });


    }

    videoEventListener()

    videoController.save.onclick = async () => {
        if (document.querySelector('#basicImage').value === '') {
            return
        }

        const number = document.querySelector('#canvas_sceneNo').value;
        // Array.from(slideList.fileBtns).forEach(function(t,i){
        //     const target = t.querySelector('.sceneNo').value;
        //     if (target === number) {
        //         slideList.imageLoadings[i].style.display = 'flex';
        //     }
        // });
        // document.querySelector('.canvas_loading_box').style.display = 'flex';

        // videoController.control.forEach(v => {
        //     v.style.pointerEvents = 'none';
        // })

        modal.fileLoadingModal.style.display = 'flex';
        modal.fileLoadingModal.querySelector('.txt').textContent = '비디오를 저장하는 중입니다 잠시만 기다려주세요.';

        const cutInfo = {
            timeline: videoController.timeline.value,
            // volume: videoController.volumeControll.value,
            rotate: videoController.rotateValue.value,
            // background: $(videoControllor.backgroundControll).val()
        };
        const form = document.querySelector('#FormData');
        const formData = new FormData(form);
        formData.append('_token', document.getElementById('_token').value);
        for (const key in cutInfo) {
            formData.append('cut_info[' + key + ']', cutInfo[key]);
        }
        const res = await saveAPI(formData)


        res.json().then(v => {
            // document.querySelector('.canvas_loading_box').style.display = 'none';
            Array.from(slideList.fileBtns).forEach( async function(t,i){
                const target = t.querySelector('.sceneNo').value;
                if (target === v.sceneNo) {
                    slideList.viewImg[i].src = v.synthesis_thumb;
                    // slideList.images[i].src = v.synthesis_thumb;
                    // slideList.imageLoadings[i].style.display = 'none';
                    // slideList.incomplete[i].classList.remove('active');
                }
            });
            modal.fileLoadingModal.style.display = 'none';

            // upload.loadingBar.classList.add('loading_100');
            // setTimeout(function () {
            //     upload.loadingModal.style.display = 'none';
            //     upload.loadingBar.classList.remove('loading_20', 'loading_80', 'loading_100');
            // }, 500)


            const number = document.querySelector('#canvas_sceneNo').value;
            if (number === v.sceneNo) {
                controllerData()
                    .then(res2 => {
                        if (res2) {
                            // galleryThumbs.update();
                            const controller = document.querySelector('.user_controller_box')
                            res2.text().then(v => {
                                controller.innerHTML = v;
                                if (document.querySelector('.photo_content')) {
                                    photo()
                                } else if (document.querySelector('.video_content')) {
                                    video()
                                } else if (document.querySelector('.text_content')) {
                                    text()
                                }
                            })
                        }
                    })


                previewAPI(document.querySelector('#canvas_sceneNo').value)
                    .then(res3 => {
                        if (res3) {
                            res3.json().then(v => {
                                freeView.imgBox.src = v.image
                            })
                        }
                    })

                slideList.imgBox[number - 1].classList.remove('not');
                if (initSize.winW <= 1100) {
                    document.querySelector('#scene_content > .right').style.display = 'none';
                }

            }

        })

    };
    const $base_time = videoInit.videoInfo.video_time;
    videoInit.myVideo.onloadedmetadata = () => {
        videoInit.myVideo.pause();

        const width_ = videoInit.myVideo.videoWidth;
        const height_ = videoInit.myVideo.videoHeight;

        let $cut_info = '';
        if (videoInit.cutInfo.value) {
            const cutInfo = JSON.parse(videoInit.cutInfo.value);
            $cut_info = cutInfo.timeline;
            videoController.rotateValue.value = cutInfo.rotate;
        }

        const $total = videoInit.myVideo.duration.toFixed(2);
        const $last_time = ($total - $base_time).toFixed(2) * 100;

        videoController.timeline.max = $last_time;
        videoController.last_time.innerText = $total;

        $(videoController.timeline).on('input', function () {
            videoInit.myVideo.currentTime = $(videoController.timeline).val() / 100;
            const last = `${Number(videoInit.myVideo.currentTime) + Number(videoInit.videoInfo.video_time)}`;
            videoInit.numberCenter.textContent = `${videoInit.myVideo.currentTime.toFixed(2)}-${Number(last).toFixed(2)}`;
            videoInit.nowList = `${Number(videoInit.myVideo.currentTime) + Number(videoInit.videoInfo.video_time)}`;
        });

        if ($cut_info !== '') {
            $(videoController.timeline).val($cut_info);
            videoInit.myVideo.currentTime = $(videoController.timeline).val() / 100;
        } else {
            $(videoController.timeline).val('0');
            videoInit.myVideo.currentTime = 0;
        }

        rotate_check('now');
    }


    function saveAPI(formData) {
        const subDomain = new Array('app', 'app', 'app', 'app', 'app', 'app');
        const sub = subDomain[Math.floor(Math.random() * subDomain.length)];

        return fetch(`https://${sub}.aileenstory.com/!/uploads/scene`, {
            method: 'POST',
            body: formData
        }).then(res => {
            // console.log(res)
            return res

        }).catch(function (error) {
            console.log(error);

        }).finally(function () {

        })
    }



    function rotate_check($now) {
        let rotate = Number(videoController.rotateValue.value);


        if ($now === 'now') {
            if (rotate === 0) {
                rotate = 270;
            } else {
                rotate -= 90;
            }
        }

        const width_ = videoInit.myVideo.videoWidth;
        const height_ = videoInit.myVideo.videoHeight;
        const userVideo = document.querySelector('#user_video');

        if (rotate === 0) {
            videoController.rotateValue.value = 90;

            if (width_ > height_) {
                userVideo.style.left = '21.75%';
                userVideo.style.top = '0';
                userVideo.style.width = '56.25%';
                userVideo.style.height = '100%';
                userVideo.style.transform = 'rotate(90deg)';
            } else {
                userVideo.style.left = '0';
                userVideo.style.top = '-38.5%';
                userVideo.style.width = '100%';
                userVideo.style.height = '177.5%';
                userVideo.style.transform = 'rotate(90deg)';
            }
        } else if (rotate === 90) {
            videoController.rotateValue.value = 180;
            if (width_ > height_) {
                userVideo.style.left = '0';
                userVideo.style.top = '0';
                userVideo.style.width = '100%';
                userVideo.style.height = '100%';
                userVideo.style.transform = 'rotate(180deg)';
            } else {
                userVideo.style.left = '21.75%';
                userVideo.style.top = '0';
                userVideo.style.width = '56.5%';
                userVideo.style.height = '100%';
                userVideo.style.transform = 'rotate(180deg)';
            }
        } else if (rotate === 180) {
            videoController.rotateValue.value = 270;
            if (width_ > height_) {
                userVideo.style.left = '21.75%';
                userVideo.style.top = '0';
                userVideo.style.width = '56.5%';
                userVideo.style.height = '100%';
                userVideo.style.transform = 'rotate(270deg)';
            } else {
                userVideo.style.left = '0';
                userVideo.style.top = '-38.5%';
                userVideo.style.width = '100%';
                userVideo.style.height = '177.5%';
                userVideo.style.transform = 'rotate(270deg)';
            }
        } else {
            videoController.rotateValue.value = 0;
            if (width_ > height_) {
                userVideo.style.left = '0';
                userVideo.style.top = '0';
                userVideo.style.width = '100%';
                userVideo.style.height = '100%';
                userVideo.style.transform = 'rotate(0deg)';
            } else {
                userVideo.style.left = '21.75%';
                userVideo.style.top = '0';
                userVideo.style.width = '56.5%';
                userVideo.style.height = '100%';
                userVideo.style.transform = 'rotate(0deg)';
            }
        }
    }

    function minSec(time) {
        if (time >= 60) {
            const min = parseInt((time%3600)/60);
            const sec = time%60;
            return time = `${min}분 ${sec}초`;
        } else {
            const sec = time%60;
            return time = `${sec}초`;
        }
    }


}

