
const initSize = {
    winW: "",
    winH: "",
    winY: ""
};

const sizeEvent = () => {
    initSize.winW = window.innerWidth;
    initSize.winH = window.innerHeight;
    initSize.winY = window.scrollY;
    window.addEventListener("resize", () => {
        initSize.winW = window.innerWidth;
        initSize.winH = window.innerHeight;
        initSize.winY = window.scrollY;
    });
    window.addEventListener("scroll", () => {
        initSize.winW = window.innerWidth;
        initSize.winH = window.innerHeight;
        initSize.winY = window.scrollY;
    });
};
sizeEvent();


const common = {
    pageNumber: 1,
    sceneNumber: 1,
    slideNumber: 0,
}

const page = {
    section1: document.querySelector('#intro_content'),
    section2: document.querySelector('#scene_content'),
}

const header = {
    prevBtn: document.querySelector('#header .prev_btn'),
    nextBtn: document.querySelector('#header .next_btn'),
    nextClick() {
        this.nextBtn.addEventListener('click', () => {
            if (common.pageNumber === 1) {
                common.pageNumber = 2;
                page.section1.style.display = 'none';
                page.section2.style.display = 'flex';
                header.prevBtn.classList.add('active');
                slideFilesLoad();
                header.nextBtn.querySelector('span').textContent = '영상제작'
            } else if (common.pageNumber === 2) {
                const alertTxt = []

                Array.from(slideList.imgBox).forEach(function (e, i) {
                    if (e.classList.contains('not')) {
                        const sceneNumber = slideList.imgBox[i].querySelector('.bottom span').textContent
                        alertTxt.push(sceneNumber)
                    }
                })
                if (alertTxt.length > 0) {
                    modal.createModal.style.display = 'flex';
                } else {
                    modal.requestModal.style.display = 'flex';
                    modal.requestModalOk.onclick = async () => {
                        const res = await RequestUpload()
                        res.json().then(v => {
                            // console.log(v)
                            if (v.state === 'ok') {
                                window.close()
                            }
                        })
                    }
                }

            }
        })
    },
    prevClick() {
        this.prevBtn.addEventListener('click', () => {
            if (common.pageNumber === 2) {
                common.pageNumber = 1;
                page.section1.style.display = 'block';
                page.section2.style.display = 'none';
                header.prevBtn.classList.remove('active');
                header.nextBtn.querySelector('span').textContent = '장면편집'
            }
        })
    }
}

header.nextClick();
header.prevClick();


window.addEventListener('load', async () => {
    const res = await loadImageList();
    res.json().then(v => {
        // console.log(v)
        loadCreateLi(v);
        uploadBox.minusNumberChange();
    })
})

function loadImageList() {
    const goodsNo = document.querySelector('#goodsNo').value;
    const createNo = document.querySelector('#createNo').value
    return fetch(`https://app.aileenstory.com/!/autorender/first/${goodsNo}/${createNo}`, {
        method: 'GET',
    }).then(res => {
        // console.log(res)
        return res
    }).catch(function (error) {
        console.log(error);
    }).finally(function () {

    })
}

function loadCreateLi(data) {
    uploadBox.content.forEach((target, index) => {
        const type = target.getAttribute('data-type');
        const tab = target.getAttribute('data-tab_name');
        // console.log(data)
        if (data[type]) {
            if (data[type][tab]) {
                const timestampSecond = Math.floor(+new Date() / 1000);
                data[type][tab].forEach((f) => {
                    const li = createLi(`${f.user_upload_image_thumb}?${timestampSecond}`, f.sceneNo)
                    target.prepend(li)
                })
            }
        }
    })
}

const modal = {
    fileLoadingModal: document.querySelector('.file_loading_modal'),
    alertModal: document.querySelector('.alert_modal'),
    alertModalClose: document.querySelector('.alert_modal .close_btn'),
    alertModalBack: document.querySelector('.alert_modal .back'),
    musicModal: document.querySelector('.music_modal'),
    musicModalClose: document.querySelector('.music_modal .close_btn'),
    musicModalBack: document.querySelector('.music_modal .back'),
    createModal: document.querySelector('.create_modal'),
    createModalClose: document.querySelector('.create_modal .close_btn'),
    createModalBack: document.querySelector('.create_modal .back'),
    requestModal: document.querySelector('.request_modal'),
    requestModalOk: document.querySelector('.request_modal .close_btn'),
    requestModalBack: document.querySelector('.request_modal .back')
}

modal.requestModalBack.addEventListener('click', () => {
    modal.requestModal.style.display = 'none';
})


class ModalClose {
    constructor(target, btn, back) {
        this.target = target;
        this.btn = btn;
        this.back = back;
    }
    off() {
        this.btn.addEventListener('click', () => {
            this.target.style.display = 'none';
        });
        this.back.addEventListener('click', () => {
            this.target.style.display = 'none';
        })
    }
}

const alertModal = new ModalClose(
    modal.alertModal,
    modal.alertModalClose,
    modal.alertModalBack
)

const musicModal = new ModalClose(
    modal.musicModal,
    modal.musicModalClose,
    modal.musicModalBack
)

const createModal = new ModalClose(
    modal.createModal,
    modal.createModalClose,
    modal.createModalBack
)

alertModal.off();
musicModal.off();
createModal.off();


const tabBox = {
    list: document.querySelectorAll('#content .right .tab_box li')
}
tabBox.list[0].querySelector('button').classList.add('active');

const uploadBox = {
    list: document.querySelectorAll('#content .right .upload_list_box'),
    content: document.querySelectorAll('#content .right .upload_list_box .upload_list_content'),
    uploadBtn: document.querySelectorAll('#content .right .upload_list_box .upload_btn'),
    uploadInputs: document.querySelectorAll('#content .right .upload_list_box .upload_input'),
    fileCount: document.querySelectorAll('#content .upload_list_header .count'),
    btnClick() {
        this.uploadBtn.forEach((target,index) => {
            target.addEventListener('click', function (e) {
                uploadBox.uploadInputs[index].click();
            })
        })
    },
    inputChange() {
        this.uploadInputs.forEach((target, index) => {
            target.addEventListener('change', function (e) {
                const files = target.files; // Array of all files
                const type = uploadBox.content[index].getAttribute('data-type');
                const tabName = uploadBox.content[index].getAttribute('data-tab_name');
                fileUpload(files, type, tabName, index);
                target.value = ''
            })
        })
    },
    minusNumberChange() {
        this.fileCount.forEach((target, index) => {
            const total = uploadBox.list[index].querySelector('.total').value;
            const imgLength = uploadBox.content[index].querySelectorAll('.img_container').length;
            const resultCount = total - imgLength;
            target.textContent = String(resultCount);
        })
    }
}
uploadBox.list[0].classList.add('active');
uploadBox.btnClick();
uploadBox.inputChange();

class TargetActive {
    constructor(targets) {
        this.targets = targets
    }

    on() {
        this.targets.forEach((e, index) => {
            e.addEventListener('click', (v) => {
                for (let i = 0; i < this.targets.length; i += 1) {
                    this.targets[i].querySelector('button').classList.remove('active');
                    uploadBox.list[i].classList.remove('active');
                }
                v.currentTarget.querySelector('button').classList.add('active');
                uploadBox.list[index].classList.add('active');
            })
        })
    }
}

const tabBoxActive = new TargetActive(
    tabBox.list
)

tabBoxActive.on();


const penInput = document.querySelectorAll('.text_input_box .text_input');

function inputLength() {
    Array.prototype.forEach.call(penInput, function (e) {
        e.nextSibling.nextSibling.firstChild.textContent = e.value.length;
        e.addEventListener('keyup', function () {
            e.nextSibling.nextSibling.firstChild.textContent = e.value.length;
        });
    });
}

inputLength();

const slideList = {
    imgBox: document.querySelectorAll('#scene_content .left .img_box'),
    fileBox: document.querySelectorAll('#scene_content .left .img_box.file_box'),
    viewImg: document.querySelectorAll('#scene_content .left .img_box .view_img img'),
    fileBtns: document.querySelectorAll('#scene_content .left .img_box.file_box .file_input_btn'),
    prevBtn: document.querySelector('#scene_content .right .btn_box .prev'),
    nextBtn: document.querySelector('#scene_content .right .btn_box .next'),
    changeBtn: document.querySelectorAll('#scene_content .left .bottom button'),
    changeStart: false,
    changeFirst: {
        first: '',
        firstText: '',
        index: ''
    },
    changeSecond: {
        second: '',
        secondText: '',
        index: ''
    },
    changeModal: document.querySelector('.move_modal'),
    musicBox: document.querySelector('.music_box'),
    musicTitle: document.querySelector('.music_title'),
    musicAddBtn: document.querySelector('#music'),
    imgBoxClick() {
        this.imgBox.forEach((target, index) => {
            target.addEventListener('click', async () => {
                for (let i = 0; i < slideList.imgBox.length; i += 1) {
                    slideList.imgBox[i].classList.remove('active');
                }
                target.classList.add('active')
                const number = slideList.imgBox[index].querySelector('.sceneNo');

                // if (document.querySelector('#canvas_sceneNo') && index !== 1) {
                //     const canvasNumber = document.querySelector('#canvas_sceneNo').value;
                //     console.log(number.value)
                //     console.log(canvasNumber)
                //     if (number.value === canvasNumber) {
                //         return
                //     }
                // }

                if (number) {
                    common.sceneNumber = number.value;
                    common.slideNumber = index;
                    this.check();
                    const res = await controllerData();
                    if (res) {
                        page.section1.style.display = 'none';
                        page.section2.style.display = 'flex';
                        const controller = document.querySelector('.user_controller_box')
                        res.text().then(v => {
                            controller.innerHTML = v;
                            if (document.querySelector('.photo_content')) {
                                photo()
                            } else if (document.querySelector('.video_content')) {
                                video()
                            } else if (document.querySelector('.text_content')) {
                                text()
                            }
                            textBox.textEvent();

                        })
                    }
                    const res2 = await previewAPI(number.value)
                    res2.json().then(v => {
                        // console.log(v)
                        slideList.viewImg[index].src =  v.image
                        freeView.imgBox.src = v.image
                    })

                }

            })
        })
    },
    prevBtnClick() {
        this.prevBtn.addEventListener('click', () => {
            slideList.imgBox[common.slideNumber - 1].click();
        });
    },
    nextBtnClick() {
        this.nextBtn.addEventListener('click', () => {
            slideList.imgBox[common.slideNumber + 1].click();
        });
    },
    check() {
        if (Number(common.slideNumber) === 0) {
            slideList.prevBtn.classList.add('active');
        } else {
            slideList.prevBtn.classList.remove('active');
        }

        if (Number(common.slideNumber) + 1 === slideList.imgBox.length) {
            slideList.nextBtn.classList.add('active');
        } else {
            slideList.nextBtn.classList.remove('active');
        }
    },
    inputChange() {
        Array.from(this.fileBtns).forEach(function (target, index) {
            target.addEventListener('click', () => target.querySelector('.file_input').click());
            target.querySelector('.file_input').addEventListener('change', async function () {
                const files = target.querySelector('.file_input').files; // FileList object
                const file = files[0];
                const file2 = files[1];
                const type = target.querySelector('.type').value;
                const sceneNo = target.querySelector('.sceneNo').value;

                modal.fileLoadingModal.style.display = 'flex';


                if (file.type.match('image.*')) { // 이미지 업로드
                    modal.fileLoadingModal.querySelector('.txt').textContent = '사진을 저장하는 중입니다 잠시만 기다려주세요.'
                    if (file2) {
                        const reader = new Image();
                        const reader2 = new Image();
                        reader.onload = function () {
                            reader2.onload = function () {
                                const maxW = 1920;
                                const maxH = 1080;

                                getOrientation(file, function (orientation) {
                                    const canvas2 = document.createElement('canvas');
                                    const ctx2 = canvas2.getContext('2d');
                                    const iw = reader.width;
                                    const ih = reader.height;
                                    const scale = Math.min(maxW / iw, maxH / ih);
                                    const iwScaled = iw * scale;
                                    const ihScaled = ih * scale;
                                    canvas2.width = iwScaled;
                                    canvas2.height = ihScaled;
                                    ctx2.drawImage(reader, 0, 0, iwScaled, ihScaled);
                                    resetOrientation(canvas2.toDataURL('image/jpeg', 1), orientation, function (resetImage) {
                                        resetOrientation2(resetImage);
                                    });
                                }); // getOrientation end


                                function resetOrientation2(resetImage) {
                                    getOrientation(file2, function (orientation) {
                                        const canvas3 = document.createElement('canvas');
                                        const ctx3 = canvas3.getContext('2d');
                                        const iw = reader2.width;
                                        const ih = reader2.height;
                                        const scale = Math.min(maxW / iw, maxH / ih);
                                        const iwScaled = iw * scale;
                                        const ihScaled = ih * scale;
                                        canvas3.width = iwScaled;
                                        canvas3.height = ihScaled;
                                        ctx3.drawImage(reader2, 0, 0, iwScaled, ihScaled);
                                        // output.text = canvas.toDataURL("image/jpeg",0.5);
                                        resetOrientation(canvas3.toDataURL('image/jpeg', 1), orientation, function (resetImage2) {
                                            const resetImage_1 = new Image();
                                            resetImage_1.src = window.URL.createObjectURL(base64StringToBlob(resetImage));

                                            resetImage_1.onload = async function () {
                                                const resetImage_2 = new Image();
                                                resetImage_2.src = window.URL.createObjectURL(base64StringToBlob(resetImage2));
                                                resetImage_2.onload = async function () {
                                                    const canvasNew = document.createElement('canvas');
                                                    const ctx3 = canvasNew.getContext('2d');
                                                    const $width = (1080 / resetImage_1.height) * resetImage_1.width;
                                                    const $width2 = (1080 / resetImage_2.height) * resetImage_2.width;

                                                    canvasNew.width = parseInt($width) + parseInt($width2);
                                                    canvasNew.height = 1080;
                                                    ctx3.drawImage(resetImage_1, 0, 0, $width, 1080);
                                                    ctx3.drawImage(resetImage_2, $width, 0, $width2, 1080);

                                                    const canvasNew2 = document.createElement('canvas');
                                                    const iw = canvasNew.width;
                                                    const ih = canvasNew.height;
                                                    const scale = Math.min(maxW / iw, maxH / ih);
                                                    const iwScaled = iw * scale;
                                                    const ihScaled = ih * scale;
                                                    canvasNew2.width = iwScaled;
                                                    canvasNew2.height = ihScaled;
                                                    const ctx4 = canvasNew2.getContext('2d');
                                                    ctx4.drawImage(canvasNew, 0, 0, canvasNew2.width, canvasNew2.height);

                                                    const $basic_image_blob = canvasNew2.toDataURL('image/jpeg');

                                                    await slideFileUpload(base64StringToBlob($basic_image_blob), sceneNo, type);
                                                    const res2 = await controllerData();
                                                    if (res2) {
                                                        // page.section1.style.display = 'none';
                                                        // page.section2.style.display = 'flex';
                                                        // galleryThumbs.update();
                                                        const controller = document.querySelector('.user_controller_box')
                                                        res2.text().then(v => {
                                                            controller.innerHTML = v;
                                                            if (document.querySelector('.photo_content')) {
                                                                photo()
                                                            } else if (document.querySelector('.video_content')) {
                                                                video()
                                                            } else if (document.querySelector('.text_content')) {
                                                                text()
                                                            }
                                                        })
                                                    }
                                                    const res = await previewAPI(sceneNo)
                                                    res.json().then(v => {
                                                        slideList.fileBox[index].classList.remove('not');
                                                        slideList.fileBox[index].querySelector('.view_img img').src = v.image;
                                                    })
                                                    file.value = ''
                                                    file2.value = ''
                                                    modal.fileLoadingModal.style.display = 'none';

                                                };
                                            };
                                        });
                                    }); //
                                }
                            }
                            reader2.src = URL.createObjectURL(file2);
                        }
                        reader.src = URL.createObjectURL(file);
                    } else if (file) {
                        await slideFileUpload(file, sceneNo, type);
                        const res2 = await controllerData();
                        if (res2) {
                            // page.section1.style.display = 'none';
                            // page.section2.style.display = 'flex';
                            // galleryThumbs.update();
                            const controller = document.querySelector('.user_controller_box')
                            res2.text().then(v => {
                                controller.innerHTML = v;
                                if (document.querySelector('.photo_content')) {
                                    photo()
                                } else if (document.querySelector('.video_content')) {
                                    video()
                                } else if (document.querySelector('.text_content')) {
                                    text()
                                }
                            })
                        }
                        const res = await previewAPI(sceneNo)
                        res.json().then(v => {
                            slideList.fileBox[index].classList.remove('not');
                            slideList.fileBox[index].querySelector('.view_img img').src = v.image;
                        })
                        file.value = ''
                        modal.fileLoadingModal.style.display = 'none';
                    }

                } else if (file.type.match('video.*')) { // 비디오 업로드
                    modal.fileLoadingModal.querySelector('.txt').textContent = '비디오를 저장하는 중입니다 잠시만 기다려주세요.'
                    const res3 = await slideFileUpload(file, sceneNo, type);
                    if (res3) {
                        res3.json().then(v => {
                            // console.log(v)
                            // if (v.files.subState === 'error_time') {
                            //     upload.uploadAlertModal2.style.display = 'flex';
                            //     upload.uploadAlertModal2.querySelector('.txt').textContent = v.files.msg;
                            // }
                        })
                    }
                    const res2 = await controllerData();
                    if (res2) {
                        // page.section1.style.display = 'none';
                        // page.section2.style.display = 'flex';
                        // galleryThumbs.update();
                        const controller = document.querySelector('.user_controller_box')
                        res2.text().then(v => {
                            controller.innerHTML = v;
                            if (document.querySelector('.photo_content')) {
                                photo()
                            } else if (document.querySelector('.video_content')) {
                                video()
                            } else if (document.querySelector('.text_content')) {
                                text()
                            }
                        })
                    }
                    const res = await previewAPI(sceneNo)
                    res.json().then(v => {
                        // slideList.fileBox[index].classList.remove('not');
                        slideList.fileBox[index].querySelector('.view_img img').src = v.image;
                    })
                    file.value = ''
                    modal.fileLoadingModal.style.display = 'none';
                }
            })
        })
    },
    changeBtnClick() {
        this.changeBtn.forEach((target, index) => {
            target.addEventListener('click', async (e) => {
                e.stopPropagation();
                const number = slideList.fileBox[index].querySelector('.sceneNo').value;

                if (!slideList.changeStart) {
                    slideList.changeFirst.first = number;
                    slideList.changeFirst.firstText = '';
                    slideList.changeFirst.index = index;
                    slideList.changeStart = true;
                    moveUp(slideList.changeModal)
                } else if (slideList.changeStart) {
                    slideList.changeSecond.second = number;
                    slideList.changeSecond.secondText = '';
                    slideList.changeSecond.index = index;
                    moveDown(slideList.changeModal);
                    if (slideList.changeFirst.first === slideList.changeSecond.second) {
                        slideList.changeStart = false;
                        return
                    }
                    const formData = new FormData();
                    formData.append('_token', document.querySelector('#_token').value);
                    formData.append('goodsNo', document.querySelector('#goodsNo').value);
                    formData.append('createNo', document.querySelector('#createNo').value);
                    formData.append('first', slideList.changeFirst.first);
                    formData.append('firstText', slideList.changeFirst.firstText);
                    formData.append('second', slideList.changeSecond.second);
                    formData.append('secondText', slideList.changeSecond.secondText);
                    const res = await screenChange(formData)
                    // console.log(res)
                    slideList.changeStart = false;
                    slideList.fileBox[slideList.changeFirst.index].click();
                    const res2 = await previewAPI(slideList.changeSecond.second)
                    res2.json().then(v => {
                        // console.log(v)
                        slideList.fileBox[slideList.changeSecond.index].querySelector('.view_img img').src =  v.image
                    })
                }
            })
        })
    },
    musicBtnClick() {
        this.musicAddBtn.addEventListener('change', (e) => {

            const target = e.target;
            const file = target.files[0];
            const formData = new FormData();
            formData.append('_token', document.querySelector('#_token').value);
            formData.append('goodsNo', document.querySelector('#goodsNo').value);
            formData.append('createNo', document.querySelector('#createNo').value);
            formData.append('mp3', file);
            fetch('https://app.aileenstory.com/!/uploads/mp3', {
                method: 'POST',
                body: formData
            }).then(res => {
                res.json().then(v => {
                    // console.log(v)
                    slideList.musicTitle.textContent = file.name;
                    slideList.musicAddBtn.style.display = 'none';
                    modal.musicModal.style.display = 'flex';
                    // slideList.musicResultBox.style.display = 'flex';
                })


            }).catch(function (error) {
                console.log(error);
            })
        })
    },
    musicLoad()  {
        const goodsNo = document.querySelector('#goodsNo').value;
        const createNo = document.querySelector('#createNo').value;
        if (this.musicBox) {
            fetch(`https://app.aileenstory.com/!/autorender/mp3/${goodsNo}/${createNo}`, {
                method: 'GET',
            }).then(res => {
                res.json().then(v => {
                    // console.log(v)
                    if (v.data === 'none') {
                        this.musicTitle.textContent = '기본 음원';
                    } else {
                        this.musicTitle.textContent = v.data.text;
                    }

                })

            }).catch(function (error) {
                console.log(error);

            })
        }
    },
}
slideList.imgBoxClick();
slideList.inputChange();
slideList.prevBtnClick();
slideList.nextBtnClick();
slideList.check();
slideList.changeBtnClick();
slideList.musicBtnClick();
slideList.musicLoad();






const textBox = {
    textInputLength() {
        const textInputs = document.querySelectorAll('.text_input_box .text_input');
        const inputLength = document.querySelectorAll('.text_input_box .input_length');
        textInputs.forEach((t, i) => {
            inputLength[i].textContent = t.value.length;
            t.addEventListener('keyup', function () {
                inputLength[i].textContent = t.value.length;
            });
        })
    },
    textBoxBig() {
        const bigBtn = document.querySelectorAll('.text_input_box .btn_box2 button');
        const textInputs = document.querySelectorAll('.text_input_box .text_input');
        bigBtn.forEach((t , i) => {
            t.addEventListener('click', () => {
                if (t.classList.contains('active')) {
                    t.classList.remove('active')
                    textInputs[i].style.height = '100px';
                } else {
                    t.classList.add('active')
                    textInputs[i].style.height = '400px';
                }

            })
        })
    },
    textReset() {
        const resetBtn = document.querySelectorAll('.text_input_box .btn_box1');
        const textInputs = document.querySelectorAll('.text_input_box .text_input');
        resetBtn.forEach((t, i) => {
            t.addEventListener('click', () => {
                textInputs[i].value = textInputs[i].placeholder
            })
        })
    },
    textEvent() {
        this.textInputLength();
        this.textBoxBig();
        this.textReset();
    }
}


const freeView = {
    imgBox: document.querySelector('.free_view .free_view_box img'),
    controllerOpen() {
        this.imgBox.addEventListener('click', async () => {
            const View = document.querySelector('#scene_content > .right');
            View.style.display = 'block';

            const res = await controllerData();
            if (res) {
                const controller = document.querySelector('.user_controller_box')
                res.text().then(v => {
                    controller.innerHTML = v;
                    if (document.querySelector('.photo_content')) {
                        photo()
                    } else if (document.querySelector('.video_content')) {
                        video()
                    } else if (document.querySelector('.text_content')) {
                        text()
                    }
                })
            }
        })
    }
}

freeView.controllerOpen();





